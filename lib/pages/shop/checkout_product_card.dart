import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/pages/product/product_model.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import '../../utils/base_utils.dart';

class CheckoutProductCard extends StatelessWidget {
  final ProductModel product;
  final int count;
  final bool showImage;
  const CheckoutProductCard(
      {Key key, this.product, this.count, this.showImage = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (showImage) ...[
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: ProductImage(
                      url: (product.thumbnails.isNotEmpty)
                          ? product.thumbnails[0].thumbnailUrl
                          : null)),
            ),
          ),
          SizedBox(width: 20),
        ],
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                product.name,
                style: TextStyle(color: Colors.black, fontSize: 16),
                maxLines: 2,
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text('$count x ', style: TextStyle(fontSize: 14)),
                      Text(product.price.toCurrency(),
                          style: TextStyle(fontSize: 14, color: primaryColor)),
                    ],
                  ),
                  Text(
                    (count * product.price).toCurrency(),
                    style: TextStyle(color: primaryColor, fontSize: 16),
                    maxLines: 2,
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
