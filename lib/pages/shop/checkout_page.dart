import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/user_model.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/product/product_detail_component/top_rounded_container.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_rounded.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/card/base_outline_card.dart';
import 'package:mobile_customer/widget/listtile_custome.dart';
import 'package:mobile_customer/widget/pages/success_page.dart';

import 'checkout_controller.dart';
import 'checkout_page_hardware_service.dart';
import 'checkout_product_card.dart';
import '../../utils/base_utils.dart';

class CheckoutPage extends StatefulWidget {
  CheckoutPage({Key key}) : super(key: key);

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  final checkoutController = Get.put(CheckoutController());

  Future<void> save() async {
    User user = await WebAuth().getUser();

    var data = {
      "name": checkoutController.namapic,
      "address": checkoutController.addrres,
      "phone": checkoutController.nohp,
      "latitude": checkoutController.lat,
      "longitude": checkoutController.long,
      "note": checkoutController.note,
      "service_id": checkoutController.service.value.id,
      "customer_id": user.id,
      "email": user.email,
      'grand_total':
          checkoutController.service.value.price + checkoutController.totalSum
    };

    Alertx.loading();
    try {
      var res =
          await Webservice().post('joborder/', data: data, showDialog: false);
      // print(res);
      String jobOrderId = res['id'].toString();
      for (var item in checkoutController.items) {
        var data = {
          "qty": item.count,
          "price": item.product.price,
          "job_order_id": jobOrderId,
          "product_id": item.product.id,
        };
        await Webservice()
            .post('joborderproduct/', data: data, showDialog: false);
      }
      if (checkoutController.date1 != null) {
        String dateStr = checkoutController.date1.toDate2() +
            ' ' +
            checkoutController.time1.toTime();
        var data = {
          'start_at': dateStr,
          'end_at': dateStr,
          'job_order_id': jobOrderId
        };
        await Webservice().post('jobschedule/', data: data, showDialog: false);
      }
      if (checkoutController.date2 != null) {
        String dateStr = checkoutController.date2.toDate2() +
            ' ' +
            checkoutController.time2.toTime();
        var data = {
          'start_at': dateStr,
          'end_at': dateStr,
          'job_order_id': jobOrderId
        };
        await Webservice().post('jobschedule/', data: data, showDialog: false);
      }
      if (checkoutController.date3 != null) {
        String dateStr = checkoutController.date3.toDate2() +
            ' ' +
            checkoutController.time3.toTime();
        var data = {
          'start_at': dateStr,
          'end_at': dateStr,
          'job_order_id': jobOrderId
        };
        await Webservice().post('jobschedule/', data: data, showDialog: false);
      }
      await Webservice().get('joborder/checkout/$jobOrderId', data: data);
      checkoutController.reset();

      if (Get.isDialogOpen) Get.back();
      Alertx().success('Checkout Berhasil', HomePage());
    } on Error catch (e) {
      if (Get.isDialogOpen) Get.back();
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 1,
      appBar: BaseAppBar(
        title: 'Checkout',
        bgColor: Colors.grey[300],
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                color: Colors.grey[300],
                padding: const EdgeInsets.only(top: 15),
                child: TopRoundedContainer(
                  color: Colors.white,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: ListView(
                    children: [
                      Text(
                        checkoutController.namapic.value,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 28, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'PIC Name',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                      // SizedBox(
                      //   height: 15,
                      // ),
                      // Text(
                      //   (checkoutController.service.value.price +
                      //           checkoutController.totalSum)
                      //       .toCurrency(),
                      //   textAlign: TextAlign.center,
                      //   style: TextStyle(
                      //       fontSize: 28,
                      //       fontWeight: FontWeight.bold,
                      //       color: primaryColor),
                      // ),
                      SizedBox(
                        height: 35,
                      ),
                      ListTileDetailRow(
                        icon: Icons.location_on_outlined,
                        title: 'Address',
                        subtitle: checkoutController.addrres.value,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.phone_outlined,
                        title: 'Phone',
                        subtitle: '+62' + checkoutController.nohp.value,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.date_range_rounded,
                        title: 'Date',
                        subtitle: checkoutController.date1.toDate() +
                            ', ' +
                            checkoutController.time1.toTime(),
                      ),
                      if (checkoutController.date2 != null)
                        ListTileDetailRow(
                          title: '',
                          subtitle: checkoutController.date2.toDate() +
                              ', ' +
                              checkoutController.time2.toTime(),
                        ),
                      SizedBox(
                        height: 10,
                      ),
                      if (checkoutController.date3 != null)
                        ListTileDetailRow(
                          title: '',
                          subtitle: checkoutController.date3.toDate() +
                              ', ' +
                              checkoutController.time3.toTime(),
                        ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Row(
                              children: [
                                Icon(Icons.sticky_note_2_outlined),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Note",
                                  style: TextStyle(),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                              flex: 3,
                              child: Container(
                                margin: EdgeInsets.only(top: 5),
                                child: Divider(
                                  color: Colors.black,
                                ),
                              )),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Text((checkoutController.note.toString() != '')
                            ? checkoutController.note.toString()
                            : 'Tidak ada catatan'),
                      ),
                      Divider(
                        color: Colors.black,
                      ),
                      SizedBox(height: 10),
                      CheckoutHardwareService(),
                      SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 120,
                            height: 40,
                            child: ButtonSave(
                              label: 'Checkout',
                              onPressed: save,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BaseCard(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Icon(
                    Icons.shopping_cart_outlined,
                    color: primaryColor,
                    size: 40,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget cardInfo(String info, String price) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                info,
                style: h2,
              ),
            ],
          ),
          Text(
            price,
            style: styleListPrice,
          )
        ],
      ),
    );
  }
}
