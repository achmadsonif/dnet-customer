import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/utils/constants.dart';
import '../../utils/base_utils.dart';

class CheckoutHardwareService extends StatelessWidget {
  const CheckoutHardwareService({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final checkoutController = Get.put(CheckoutController());

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffc4c4c4)),
        borderRadius: BorderRadius.all(
            Radius.circular(5.0) //                 <--- border radius here
            ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Service List',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Text(' - 1 service'),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Row(
                        children: [
                          // Container(
                          //   width: 40,
                          //   height: 40,
                          //   decoration: BoxDecoration(
                          //     color: Color(0xFFF5F6F9),
                          //     borderRadius: BorderRadius.circular(15),
                          //   ),
                          //   child: ProductImage(
                          //     url: checkoutController.service.value.iconUrl,
                          //     fit: BoxFit.fill,
                          //   ),
                          // ),
                          Icon(Icons.miscellaneous_services),
                          SizedBox(width: 10),
                          Flexible(
                            child: Text(checkoutController.service.value.name,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold)),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        checkoutController.service.value.price.toCurrency(),
                        style: TextStyle(
                          color: primaryColor,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  children: [
                    Text(
                      'Hardware List',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Text(' - ${checkoutController.totalItem} items'),
                  ],
                ),
                Column(
                  children: [
                    for (var item in checkoutController.items)
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                color: Color(0xFFF5F6F9),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: ProductImage(
                                url: (item.product.thumbnails.isNotEmpty)
                                    ? item.product.thumbnails[0].thumbnailUrl
                                    : null,
                                fit: BoxFit.fill,
                              ),
                            ),
                            SizedBox(width: 10),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    item.product.name,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        '${item.product.price.toCurrency()} (${item.count} buah) ',
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal),
                                      ),
                                      Text(
                                        (item.count * item.product.price)
                                            .toCurrency(),
                                        style: TextStyle(
                                            color: primaryColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                  ],
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1,
            color: Color(0xffc4c4c4),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Grand Total', style: TextStyle(color: Colors.black)),
                Text(
                  (checkoutController.service.value.price +
                          checkoutController.totalSum)
                      .toCurrency(),
                  style: TextStyle(
                      color: primaryColor, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
        ],
      ),
    );
  }
}
