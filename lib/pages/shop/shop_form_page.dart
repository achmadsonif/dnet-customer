import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/map/map_get_location.dart';
import 'package:mobile_customer/pages/map/map_google_get_location.dart';
import 'package:mobile_customer/pages/shop/checkout_page.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/gps_utils.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_date.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/button/button_time.dart';
import '../../widget/base/base_app_bar.dart';
import '../../widget/form/form_date.dart';

import '../../widget/form/form_input.dart';
import 'package:flutter/material.dart';
import '../../utils/base_utils.dart';
import 'checkout_controller.dart';
import 'package:dart_date/dart_date.dart';

class ShopFormPage extends StatefulWidget {
  ShopFormPage({Key key}) : super(key: key);

  @override
  _ShopFormPageState createState() => _ShopFormPageState();
}

class _ShopFormPageState extends State<ShopFormPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  final checkoutController = Get.put(CheckoutController());

  TextEditingController _fullnameController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _addrresController = new TextEditingController();
  TextEditingController _noteController = new TextEditingController();
  // TextEditingController _date1Controller = new TextEditingController();
  // TextEditingController _date2Controller = new TextEditingController();
  // TextEditingController _date3Controller = new TextEditingController();
  DateTime _date1;
  DateTime _date2;
  DateTime _date3;
  DateTime _time1;
  DateTime _time2;
  DateTime _time3;

  DateTime minimiumDate = DateTime.now();
  String validateDate1 = '';
  String validateDate2 = '';
  String validateDate3 = '';

  bool errorDateRequired = false;

  @override
  void initState() {
    super.initState();
    _fullnameController.text = checkoutController.namapic.value;
    _phoneController.text = checkoutController.nohp.value;
    _addrresController.text = checkoutController.addrres.value;
    _noteController.text = checkoutController.note.value;

    minimiumDate.addHours(4);
    minimiumDate.addHours(4);
    // _date1Controller.text = checkoutController.date1.value;
    // _date2Controller.text = checkoutController.date2.value;
    // _date3Controller.text = checkoutController.date3.value;
  }

  int opsiCountDate = 1;

  Future<void> save() async {
    bool checker = true;
    if (_date1 == null || _time1 == null) {
      setState(() {
        errorDateRequired = true;
      });
    } else {
      setState(() {
        errorDateRequired = false;
      });
    }

    if (_key.currentState.validate()) {
      if (_date1 == null || _time1 == null) {
        return;
      }

      checkoutController.namapic.value = _fullnameController.text.trim();
      checkoutController.nohp.value = _phoneController.text.trim();
      checkoutController.note.value = _noteController.text.trim();

      checkoutController.date1 = _date1;
      checkoutController.time1 = _time1;
      if (_date2 == null || _time2 == null) {
        checkoutController.date2 = null;
        checkoutController.time2 = null;
      } else {
        checkoutController.date2 = _date2;
        checkoutController.time2 = _time2;
      }
      if (_date3 == null || _time3 == null) {
        checkoutController.date3 = null;
        checkoutController.time3 = null;
      } else {
        checkoutController.date3 = _date3;
        checkoutController.time3 = _time3;
      }
      // checkoutController.date1.value = _date1Controller.text.trim();
      // checkoutController.date2.value = _date2Controller.text.trim();
      // checkoutController.date3.value = _date3Controller.text.trim();
      validateDate1 = '';
      try {
        var data = {
          'start_at': _date1.toDate2() + ' ' + _time1.toTime(),
          'end_at': _date1.toDate2() + ' ' + _time1.toTime(),
        };
        await Webservice().post('jobschedule/check', data: data);
      } catch (e) {
        print('error');
        checker = false;
        validateDate1 = e.toString();
      }
      validateDate2 = '';
      if (_date2 != null && _time2 != null) {
        try {
          var data = {
            'start_at': _date2.toDate2() + ' ' + _time2.toTime(),
            'end_at': _date2.toDate2() + ' ' + _time2.toTime(),
          };
          await Webservice().post('jobschedule/check', data: data);
        } catch (e) {
          checker = false;
          validateDate2 = e.toString();
        }
      }
      validateDate3 = '';
      if (_date3 != null && _time3 != null) {
        try {
          var data = {
            'start_at': _date3.toDate2() + ' ' + _time3.toTime(),
            'end_at': _date3.toDate2() + ' ' + _time3.toTime(),
          };
          await Webservice().post('jobschedule/check', data: data);
        } catch (e) {
          checker = false;
          validateDate3 = e.toString();
        }
      }
      if (checker) {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => CheckoutPage()),
        );
      } else {
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 2,
      appBar: BaseAppBar(
        title: 'Form Pesan Teknisi',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Text(
                'Detail Information',
                textAlign: TextAlign.center,
                style: h1,
              ),
              Text(
                'Please fill form below to continue',
                textAlign: TextAlign.center,
                style: h3,
              ),
              SizedBox(
                height: 40,
              ),
              FormInput(
                title: 'Name PIC',
                controller: _fullnameController,
                validatorRequired: true,
              ),
              FormInput(
                title: 'Nomor HP',
                controller: _phoneController,
                validatorRequired: true,
                validatioType: [ValidationType.hp],
                keyboardType: TextInputType.phone,
                prefix: Container(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    '+62',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              FormInput(
                title: 'Alamat',
                maxLine: 5,
                controller: _addrresController,
                readOnly: true,
                validatorRequired: true,
                onTap: () async {
                  GpsLocation mypoint = await Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => MapGoogleGetLocation()),
                  );
                  setState(() {
                    _addrresController.text = mypoint.name;
                    checkoutController.addrres.value = mypoint.name;
                    checkoutController.lat.value = mypoint.latitude.toString();
                    checkoutController.long.value =
                        mypoint.longitude.toString();
                  });
                },
              ),
              FormInput(
                title: 'Note',
                maxLine: 5,
                controller: _noteController,
                validatorRequired: false,
              ),
              SizedBox(
                height: 10,
              ),
              widgetOptionDate(),
              SizedBox(height: 20.0),
              Container(
                  width: double.infinity,
                  child: ButtonSave(
                    label: 'Continue',
                    onPressed: save,
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget widgetOptionDate() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Tanggal Pengerjaan',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              InkWell(
                  onTap: () {
                    if (opsiCountDate < 3)
                      setState(() {
                        opsiCountDate++;
                      });
                  },
                  child: (opsiCountDate >= 3)
                      ? SizedBox.shrink()
                      : CircleAvatar(
                          maxRadius: 15,
                          backgroundColor: Colors.purple,
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 15,
                          ),
                        )),
            ],
          ),
        ),
        titleDate('Opsi ke-1'),
        Row(
          children: [
            Flexible(
              flex: 3,
              child: BaseButtonDate(
                minTime: DateTime.now(),
                value: _date1,
                onConfirm: (value) {
                  checkoutController.date1 = value;
                  setState(() {
                    _date1 = value;
                  });
                },
              ),
            ),
            Flexible(
              flex: 2,
              child: BaseButtonTime(
                value: _time1,
                onConfirm: (value) {
                  checkoutController.time1 = value;
                  setState(() {
                    _time1 = value;
                  });
                },
              ),
            ),
          ],
        ),
        if (errorDateRequired)
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Tanggal dan jam opsi 1 harus diisi',
              style: TextStyle(color: Colors.red),
            ),
          ),
        if (opsiCountDate > 1)
          Column(
            children: [
              titleDate('Opsi ke-2'),
              Row(
                children: [
                  Flexible(
                    flex: 3,
                    child: BaseButtonDate(
                      minTime: DateTime.now(),
                      value: _date2,
                      onConfirm: (value) {
                        setState(() {
                          _date2 = value;
                        });
                      },
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: BaseButtonTime(
                            value: _time2,
                            onConfirm: (value) {
                              checkoutController.time2 = value;
                              setState(() {
                                _time2 = value;
                              });
                            },
                          ),
                        ),
                        if (opsiCountDate == 2)
                          InkWell(
                            child: CircleAvatar(
                              maxRadius: 15,
                              backgroundColor: Colors.red[900],
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 15,
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                opsiCountDate--;
                                checkoutController.date2 = null;
                                checkoutController.time2 = null;
                                _date2 = null;
                                _time2 = null;
                              });
                            },
                          ),
                      ],
                    ),
                  ),
                ],
              ),
              if (validateDate2 != '')
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    validateDate2,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
            ],
          ),
        if (opsiCountDate > 2)
          Column(
            children: [
              titleDate('Opsi ke-3'),
              Row(
                children: [
                  Flexible(
                    flex: 3,
                    child: BaseButtonDate(
                      minTime: DateTime.now(),
                      value: _date3,
                      onConfirm: (value) {
                        setState(() {
                          _date3 = value;
                        });
                      },
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: BaseButtonTime(
                            value: _time3,
                            onConfirm: (value) {
                              setState(() {
                                _time3 = value;
                              });
                            },
                          ),
                        ),
                        if (opsiCountDate == 3)
                          InkWell(
                            child: CircleAvatar(
                              maxRadius: 15,
                              backgroundColor: Colors.red[900],
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 15,
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                opsiCountDate--;
                                _date3 = null;
                                _time3 = null;
                              });
                            },
                          ),
                      ],
                    ),
                  ),
                ],
              ),
              if (validateDate3 != '')
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    validateDate3,
                    style: TextStyle(color: Colors.red),
                  ),
                )
            ],
          ),
      ],
    );
  }

  Row titleDate(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(text),
        ),
      ],
    );
  }
}
