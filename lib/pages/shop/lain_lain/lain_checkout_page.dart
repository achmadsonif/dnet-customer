import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/listtile_custome.dart';
import 'package:mobile_customer/widget/pages/success_page.dart';

class LainCheckoutPage extends StatefulWidget {
  LainCheckoutPage({Key key}) : super(key: key);

  @override
  _LainCheckoutPageState createState() => _LainCheckoutPageState();
}

class _LainCheckoutPageState extends State<LainCheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(title: 'Checkout',),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: [
          SizedBox(
            height: 10,
          ),
          Text(
            'Confirmation',
            textAlign: TextAlign.center,
            style: h1,
          ),
          Text(
            'Check infomation below before continue',
            textAlign: TextAlign.center,
            style: h3,
          ),
          SizedBox(
            height: 40,
          ),
          ListTileDetailColumn(title: 'Name PIC',subtitle: 'Budi Aji',),
          ListTileDetailColumn(title: 'Address',subtitle: 'Jl. Wonokromo 123',),
          ListTileDetailColumn(title: 'Nomor HP',subtitle: '085712345678',),
          ListTileDetailColumn(title: 'Email',subtitle: 'abc@gmail.com',),
          ListTileDetailColumn(title: 'Start Date',subtitle: '21 March 2021',),
          ListTileDetailColumn(title: 'End Date',subtitle: '21 June 2021',),
          ListTileDetailColumn(title: 'Jumlah Teknisi',subtitle: '10',),
          ListTileDetailColumn(title: 'Alat yang dibutuhkan',subtitle: 'Elit et mollit excepteur laborum ullamco.',),
          ListTileDetailColumn(title: 'Keterangan',subtitle: 'Consectetur pariatur veniam exercitation et id est aliqua sunt.',),
          SizedBox(height: 20),
          ButtonSave(label: 'Checkout',onPressed: ()=>Navigator.of(context).push(MaterialPageRoute(builder:
            (context) => SuccessPage(text: 'Checkout Success',)),
          ),),
          SizedBox(height: 20),

        ],
      ),
    );
  }
  Widget cardInfo(String info,String price) {
    return 
      Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  info,
                  style: h2,
                ),
              ],
            ),
            Text(
              price,
              style: styleListPrice,
            )
          ],
        ),
      );
  }
}
