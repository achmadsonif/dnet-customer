import 'package:mobile_customer/pages/auth/user_model.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/map/map_google_get_location.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/gps_utils.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_date.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import '../../../widget/base/base_app_bar.dart';
import '../../../utils/webservice.dart';

import '../../../widget/form/form_input.dart';
import 'package:flutter/material.dart';
import '../../../utils/base_utils.dart';

class LainLainFormPage extends StatefulWidget {
  LainLainFormPage({Key key}) : super(key: key);

  @override
  _LainLainFormPageState createState() => _LainLainFormPageState();
}

class _LainLainFormPageState extends State<LainLainFormPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  GpsLocation mypoint;

  TextEditingController _fullnameController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _projectNameController = new TextEditingController();
  TextEditingController _teknisiController = new TextEditingController();
  TextEditingController _alatController = new TextEditingController();
  TextEditingController _noteController = new TextEditingController();

  DateTime startDate;
  DateTime endDate;
  DateTime minDate = DateTime.now();
  DateTime maxDate = DateTime(2101);
  bool errorDateRequired = false;

  Future<void> save() async {
    User user = await WebAuth().getUser();
    if (startDate == null || endDate == null) {
      setState(() {
        errorDateRequired = true;
      });
    } else {
      setState(() {
        errorDateRequired = false;
      });
    }
    if (_key.currentState.validate()) {
      if (startDate == null || endDate == null) {
        return;
      }
      var data = {
        "name": _fullnameController.text.trim(),
        "address": _addressController.text.trim(),
        "latitude": mypoint.latitude.toString(),
        "longitude": mypoint.longitude.toString(),
        "phone": '0' + int.parse(_phoneController.text.trim()).toString(),
        "email": _emailController.text.trim(),
        "project_name": _projectNameController.text.trim(),
        "project_start_at": startDate.toDate2(),
        "project_end_at": endDate.toDate2(),
        "technician_note": _teknisiController.text.trim(),
        "hardware_note": _alatController.text.trim(),
        "note": _noteController.text.trim(),
        "customer_id": user.id,
      };
      print(data);

      try {
        await Webservice().post('joborder/', data: data);
        Alertx().success('Checkout Berhasil', HomePage());
      } on Error catch (e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 2,
      appBar: BaseAppBar(
        title: 'Jasa Lain Lain',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(15.0),
        child: Form(
          key: _key,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Text(
                'Detail Information',
                textAlign: TextAlign.center,
                style: h1,
              ),
              Text(
                'Please fill form below to continue',
                textAlign: TextAlign.center,
                style: h3,
              ),
              SizedBox(
                height: 40,
              ),
              FormInput(
                title: 'Name PIC',
                controller: _fullnameController,
                validatorRequired: true,
              ),
              FormInput(
                title: 'Alamat',
                maxLine: 5,
                controller: _addressController,
                readOnly: true,
                validatorRequired: true,
                onTap: () async {
                  GpsLocation newpoint = await Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => MapGoogleGetLocation()),
                  );
                  if (newpoint != null) {
                    mypoint = newpoint;
                    setState(() {
                      _addressController.text = newpoint.name;
                    });
                  }
                },
              ),
              FormInput(
                title: 'Nomor HP',
                controller: _phoneController,
                keyboardType: TextInputType.phone,
                validatorRequired: true,
                validatioType: [ValidationType.hp],
                prefix: Container(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    '+62',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              FormInput(
                title: 'Email',
                controller: _emailController,
                validatorRequired: true,
              ),
              FormInput(
                title: 'Nama Project',
                controller: _projectNameController,
                validatorRequired: true,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  children: [
                    Expanded(
                      child: BaseButtonDate(
                        value: startDate,
                        minTime: DateTime.now(),
                        maxTime: maxDate,
                        placeholder: 'Start date',
                        onConfirm: (value) {
                          if (value != null)
                            setState(() {
                              minDate = value;
                              startDate = value;
                            });
                        },
                      ),
                    ),
                    Expanded(
                      child: BaseButtonDate(
                        value: endDate,
                        minTime: minDate,
                        placeholder: 'End date',
                        onConfirm: (value) {
                          if (value != null)
                            setState(() {
                              endDate = value;
                              maxDate = value;
                            });
                        },
                      ),
                    ),
                  ],
                ),
              ),
              if (errorDateRequired)
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 52.0, vertical: 5),
                  child: Text(
                    'Tanggal tidak boleh kosong',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              SizedBox(
                height: 5,
              ),
              // FormInput(
              //   title: 'Jumlah Teknisi',
              //   controller: _teknisiController,
              //   maxLine: 5,
              //   validatorRequired: true,
              // ),
              // FormInput(
              //   title: 'Alat yang dibutuhkan',
              //   maxLine: 5,
              //   controller: _alatController,
              //   validatorRequired: false,
              // ),
              FormInput(
                title: 'Keterangan',
                maxLine: 5,
                controller: _noteController,
                validatorRequired: false,
              ),
              SizedBox(height: 15.0),
              Container(
                  width: double.infinity,
                  child: ButtonSave(
                    label: 'Continue',
                    onPressed: save,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
