import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_card.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_rouded_square.dart';
import '../../widget/base/base_app_bar.dart';
import 'checkout_controller.dart';
import 'checkout_product_card.dart';
import '../../utils/base_utils.dart';

class ProductCartPage extends StatelessWidget {
  const ProductCartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final checkoutController = Get.put(CheckoutController());
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Keranjang',
      ),
      body: Obx(
        () => ListView(
          children: [
            for (var item in checkoutController.items)
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Row(
                  children: [
                    SizedBox(
                      width: 88,
                      child: AspectRatio(
                        aspectRatio: 0.88,
                        child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xFFF5F6F9),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: ProductImage(
                                url: (item.product.thumbnails.isNotEmpty)
                                    ? item.product.thumbnails[0].thumbnailUrl
                                    : null)),
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            item.product.name,
                            style: TextStyle(color: Colors.black, fontSize: 16),
                            maxLines: 2,
                          ),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Text('@ ' + item.product.price.toCurrency(),
                                      style: TextStyle(fontSize: 14)),
                                ],
                              ),
                              Text(
                                (item.count * item.product.price).toCurrency(),
                                style: TextStyle(
                                    color: primaryColor, fontSize: 16),
                                maxLines: 2,
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              ButtonRoundedSquare(
                                bgColor: Colors.white,
                                iconColor: Colors.red,
                                icon: Icons.remove,
                                onPressed: () =>
                                    checkoutController.remove(item.product),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                item.count.toString(),
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              ButtonRoundedSquare(
                                icon: Icons.add,
                                bgColor: Colors.white,
                                iconColor: Colors.green,
                                onPressed: () =>
                                    checkoutController.add(item.product),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
