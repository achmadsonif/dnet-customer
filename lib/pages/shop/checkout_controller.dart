import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_model.dart';
import 'package:mobile_customer/pages/service/service_model.dart';

class CheckoutController extends GetxController {
  var namapic = ''.obs;
  var nohp = ''.obs;
  var addrres = ''.obs;
  var lat = ''.obs;
  var long = ''.obs;
  var note = ''.obs;
  DateTime date1;
  DateTime date2;
  DateTime date3;
  DateTime time1;
  DateTime time2;
  DateTime time3;

  var service = ServiceModel().obs;
  var items = <CheckoutModel>[].obs;

  reset() {
    namapic.value = '';
    nohp.value = '';
    addrres.value = '';
    lat.value = '';
    long.value = '';
    note.value = '';

    items.clear();
    date1 = null;
    date2 = null;
    date3 = null;
    time1 = null;
    time2 = null;
    time3 = null;
  }

  addWithNumb(ProductModel p, int countProduct) {
    final item =
        items.firstWhere((e) => e.product.id == p.id, orElse: () => null);
    if (item == null) {
      items.add(CheckoutModel(product: p, count: countProduct));
    } else {
      item.count = countProduct;
    }
    items.refresh();
  }

  add(ProductModel p) {
    final item =
        items.firstWhere((e) => e.product.id == p.id, orElse: () => null);
    if (item == null) {
      items.add(CheckoutModel(product: p));
    } else {
      item.count++;
    }
    items.refresh();
  }

  remove(ProductModel p) {
    final item = items.firstWhere((e) => e.product.id == p.id);
    if (item.count == 1) {
      items.remove(item);
    } else {
      item.count--;
    }
    items.refresh();
  }

  int get totalItem => items.length;

  int get totalSum {
    int sum = 0;
    items.forEach((e) {
      sum += e.product.price * e.count;
    });
    return sum;
  }
}

class CheckoutModel {
  int count;
  ProductModel product;
  CheckoutModel({this.count = 1, this.product});
}
