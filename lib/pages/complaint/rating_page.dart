import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import '../../widget/base/base_app_bar.dart';
import '../../utils/webservice.dart';

import '../../widget/form/form_input.dart';
import '../../widget/pages/success_page.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class RatingPage extends StatefulWidget {
  final OrderModel item;
  RatingPage({Key key, this.item}) : super(key: key);

  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState extends State<RatingPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  TextEditingController _textController = new TextEditingController();
  double _rating = 3;

  Future<void> save() async {
    // if (_key.currentState.validate()) {
    var data = {
      "technician_rating": _rating.toString(),
      "technician_feedback": _textController.text,
      "status_note": "Konfirmasi selesai",
    };

    await Webservice().put('joborder/rate/${widget.item.id}', data: data);
    Alertx().success('Rating Berhasil', HomePage());

    // }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Rating',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Text(
                'Rating',
                textAlign: TextAlign.center,
                style: h1,
              ),
              Text(
                'Choose Rating from 1 to 5',
                textAlign: TextAlign.center,
                style: h3,
              ),
              SizedBox(
                height: 40,
              ),
              RatingBar.builder(
                initialRating: 3,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  print(rating);
                  _rating = rating;
                },
              ),
              FormInput(
                title: '',
                controller: _textController,
                maxLine: 5,
                // controller: _fullnameController,
                validatorRequired: false,
              ),
              SizedBox(height: 40.0),
              Container(
                  width: double.infinity,
                  child: ButtonSave(
                    label: 'Submit',
                    onPressed: save,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
