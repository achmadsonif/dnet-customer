import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import '../../widget/base/base_app_bar.dart';
import '../../utils/webservice.dart';

import '../../widget/form/form_input.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ComplaintPage extends StatefulWidget {
  final OrderModel item;
  ComplaintPage({Key key, this.item}) : super(key: key);

  @override
  _ComplaintPageState createState() => _ComplaintPageState();
}

class _ComplaintPageState extends State<ComplaintPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  ProgressDialog pr;

  TextEditingController _textController = new TextEditingController();

  Future<void> save() async {
    if (_key.currentState.validate()) {
      var data = {
        "status_note": _textController.text,
      };

      await Webservice()
          .put('joborder/complained/${widget.item.id}', data: data);
      Alertx().success('Complaint Berhasil', HomePage());
    }
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Complaint',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Text(
                'Complaint',
                textAlign: TextAlign.center,
                style: h1,
              ),
              Text(
                'Please fill form below to continue',
                textAlign: TextAlign.center,
                style: h3,
              ),
              SizedBox(
                height: 40,
              ),
              FormInput(
                title: 'Note',
                maxLine: 10,
                controller: _textController,
                validatorRequired: true,
              ),
              SizedBox(height: 15.0),
              Container(
                  width: double.infinity,
                  child: ButtonSave(
                    label: 'Continue',
                    onPressed: save,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
