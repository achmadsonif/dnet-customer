import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/loading_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentPage extends StatefulWidget {
  final String url;

  const PaymentPage({Key key, this.url}) : super(key: key);
  @override
  PaymentPageState createState() => PaymentPageState();
}

class PaymentPageState extends State<PaymentPage> {
  int progress = 0;
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: BaseAppBar(
      //   title: 'Pembayaran',
      // ),
      body: SafeArea(
        child: Column(
          children: [
            Row(
              children: [
                InkWell(
                    onTap: () => Get.offAll(HomePage()),
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Icon(
                        Icons.arrow_back,
                        color: primaryColor,
                      ),
                    )),
                SizedBox(width: 30),
                Text('Pembayaran',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Colors.black)),
              ],
            ),
            if (progress < 100) LinearProgressIndicator(),
            Expanded(
              child: WebView(
                javascriptMode: JavascriptMode.unrestricted,
                onProgress: (int currentprogress) {
                  setState(() {
                    progress = currentprogress;
                  });
                  print("loading (progress : $currentprogress%)");
                },
                initialUrl: widget.url,
                navigationDelegate: (NavigationRequest request) {
                  if (request.url.startsWith('gojek://') ||
                      request.url.startsWith('shopeeid://')) {
                    print('blocking navigation to $request}');
                    _launchURL(request.url);
                    return NavigationDecision.prevent;
                  }

                  print('allowing navigation to $request');
                  return NavigationDecision.navigate;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
