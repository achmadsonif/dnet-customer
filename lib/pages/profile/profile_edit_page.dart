import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/user_model.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/pages/profile/resend_mail_page.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/form/form_image_avatar.dart';
import 'package:mobile_customer/widget/form/form_input.dart';
import 'package:mobile_customer/widget/form/form_radio.dart';
import 'package:mobile_customer/widget/loading_widget.dart';
import 'package:dio/dio.dart' as d;
import 'package:path/path.dart';

class ProfileEditPage extends StatefulWidget {
  ProfileEditPage({Key key}) : super(key: key);

  @override
  _ProfileEditPageState createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  TextEditingController _fullnameController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _hpController = new TextEditingController();
  List<RadioModel> listItemRadio = [
    RadioModel('Laki-Laki', 'Laki-Laki'),
    RadioModel('Perempuan', 'Perempuan'),
  ];
  RadioModel _genderSelected;

  User user;

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<void> getData() async {
    try {
      user = await WebAuth().getUser();
      setState(() {
        _fullnameController.text = user.name;
        _addressController.text = user.address;
        _emailController.text = user.email;
        _hpController.text = int.parse(user.phone).toString();
        if (user.gender == 'Laki-Laki') {
          _genderSelected = listItemRadio[0];
        } else {
          _genderSelected = listItemRadio[1];
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> save() async {
    if (_key.currentState.validate()) {
      Map<String, dynamic> data = {
        "name": _fullnameController.text.trim(),
        "gender": _genderSelected.id,
        "email": _emailController.text.trim(),
        "address": _addressController.text.trim(),
        "phone": _hpController.text.trim(),
      };

      if (avatarImage != null) {
        data['photo'] = await d.MultipartFile.fromFile(avatarImage.path,
            filename: basename(avatarImage.path));
      }

      try {
        await Webservice().put('profile/${user.id}/', data: data, isForm: true);
        await WebAuth().getUserFromServer();

        Get.back();
      } on Error catch (e) {
        print(e);
      }
    }
  }

  File avatarImage;

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Edit Profile',
      ),
      body: (user == null)
          ? LoadingWidget()
          : Form(
              key: _key,
              child: ListView(
                padding: EdgeInsets.all(20),
                children: [
                  FormImageAvatar(
                    urlImage: (avatarImage == null)
                        ? user.photoUrl
                        : avatarImage.path,
                    size: 150,
                    onChanged: (value) {
                      setState(() {
                        avatarImage = value;
                      });
                    },
                  ),
                  FormInput(
                    title: 'Nama Lengkap',
                    controller: _fullnameController,
                    validatorRequired: true,
                  ),
                  FormInput(
                    title: 'Alamat',
                    controller: _addressController,
                    validatorRequired: true,
                    maxLine: 3,
                  ),
                  FormInput(
                    title: 'No Hp',
                    readOnly: true,
                    controller: _hpController,
                    validatorRequired: true,
                    keyboardType: TextInputType.number,
                    prefix: Container(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        '+62',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                  FormInput(
                    title: 'Email',
                    controller: _emailController,
                    validatorRequired: true,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        (user?.emailVerifiedAt == null)
                            ? InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => ResendMailPage(
                                              mail: user.email,
                                            )),
                                  );
                                },
                                child: Chip(
                                  padding: EdgeInsets.all(0),
                                  backgroundColor: Colors.grey,
                                  label: Text('Klik disini untuk verifikasi',
                                      style: TextStyle(color: Colors.white)),
                                ),
                              )
                            : Chip(
                                padding: EdgeInsets.all(0),
                                backgroundColor: Colors.green,
                                label: Text('Verified',
                                    style: TextStyle(color: Colors.white)),
                              ),
                      ],
                    ),
                  ),
                  FormRadio(
                    title: 'Jenis Kelamin',
                    selected: _genderSelected,
                    listItem: listItemRadio,
                    onChanged: (value) {
                      setState(() {
                        _genderSelected = value;
                      });
                    },
                  ),
                  SizedBox(height: 15.0),
                  Container(
                      // width: double.infinity,
                      child: ButtonSave(
                    label: 'Submit',
                    onPressed: save,
                  ))
                ],
              ),
            ),
    );
  }
}
