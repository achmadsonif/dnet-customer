import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/form/form_input.dart';
import 'package:mobile_customer/widget/form/form_radio.dart';
import '../../widget/base/base_app_bar.dart';
import '../../utils/webservice.dart';

import 'package:flutter/material.dart';
import '../../widget/button/button_save.dart';

class PasswordPage extends StatefulWidget {
  PasswordPage({Key key}) : super(key: key);

  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  final gstate = Get.put(GlobalController());
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  TextEditingController _oldPasswordController = new TextEditingController();
  TextEditingController _newPasswordController = new TextEditingController();
  TextEditingController _confirmPasswordController =
      new TextEditingController();

  Future<void> save() async {
    if (_key.currentState.validate()) {
      var data = {
        "password_old": _oldPasswordController.text.trim(),
        "password_new": _newPasswordController.text.trim(),
        "password_confirm": _confirmPasswordController.text.trim(),
      };

      await Webservice()
          .put('profile/setpassword/${gstate.user.value.id}', data: data)
          .then((value) => Alertx().success('Password Berhasil', HomePage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Change Password',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              FormInput(
                title: 'Password Lama',
                controller: _oldPasswordController,
                validatorRequired: true,
                obscureText: true,
              ),
              FormInput(
                title: 'Password Baru',
                controller: _newPasswordController,
                validatorRequired: true,
                obscureText: true,
              ),
              FormInput(
                title: 'Confirm Password Baru',
                controller: _confirmPasswordController,
                validatorRequired: true,
                obscureText: true,
              ),
              SizedBox(height: 15.0),
              Container(
                  width: double.infinity,
                  child: ButtonSave(
                    label: 'Submit',
                    onPressed: save,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
