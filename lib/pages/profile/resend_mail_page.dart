import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/widget/form/form_input.dart';
import 'package:mobile_customer/widget/form/form_radio.dart';
import '../../widget/base/base_app_bar.dart';
import '../../utils/webservice.dart';

import 'package:flutter/material.dart';
import '../../widget/button/button_save.dart';

class ResendMailPage extends StatefulWidget {
  final String mail;
  ResendMailPage({Key key, this.mail}) : super(key: key);

  @override
  _ResendMailPageState createState() => _ResendMailPageState();
}

class _ResendMailPageState extends State<ResendMailPage> {
  final gstate = Get.put(GlobalController());
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  TextEditingController _mailController = new TextEditingController();

  @override
  void initState() {
    _mailController.text = widget.mail;
    super.initState();
  }

  Future<void> save() async {
    if (_key.currentState.validate()) {
      var data = {
        "email": _mailController.text.trim(),
      };

      await Webservice()
          .post('auth/resend/email', data: data)
          .then((value) => Get.back());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Verifikasi Email',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              FormInput(
                readOnly: true,
                title: 'Masukkan Email',
                controller: _mailController,
                validatorRequired: true,
              ),
              SizedBox(height: 15.0),
              Container(
                  width: double.infinity,
                  child: ButtonSave(
                    label: 'Kirim verifikasi',
                    onPressed: save,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
