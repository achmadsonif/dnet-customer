import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/auth/login_page.dart';
import 'package:mobile_customer/pages/auth/user_model.dart';
import 'package:mobile_customer/pages/contact_us.dart';
import 'package:mobile_customer/pages/profile/profile_edit_page.dart';
import 'package:mobile_customer/pages/profile/profile_menu.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/form/form_image_avatar.dart';

import 'package:flutter/material.dart';
import '../../utils/webservice.dart';
import '../about_us.dart';
import '../auth/web_auth.dart';
import 'password_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final gstate = Get.put(GlobalController());

  @override
  void initState() {
    super.initState();
  }

  void logout() async {
    await WebAuth.logOut();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(showNotif: true, title: 'Profile'),
      backgroudType: 2,
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Obx(
                () => CircleAvatar(
                  radius: 100.0,
                  backgroundImage: NetworkImage(gstate.user.value.photoUrl),
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          ProfileMenu(
            text: "Edit Profile",
            icon: "assets/icons/User Icon.svg",
            press: () => {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ProfileEditPage()),
              )
            },
          ),
          ProfileMenu(
            text: "Change Password",
            icon: "assets/icons/Lock.svg",
            press: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => PasswordPage()));
            },
          ),
          ProfileMenu(
            text: "Contact Us",
            icon: "assets/icons/Question mark.svg",
            press: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ContactUsPage()));
            },
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                  primary: primaryColor,
                  backgroundColor: Colors.white,
                  padding: EdgeInsets.all(20),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  side: BorderSide(color: primaryColor)),
              onPressed: () {
                Get.to(AboutUsPage());
              },
              child: Row(
                children: [
                  Icon(
                    Icons.info_outline,
                    color: primaryColor,
                  ),
                  SizedBox(width: 20),
                  Expanded(child: Text('About Us')),
                  Icon(Icons.arrow_forward_ios),
                ],
              ),
            ),
          ),
          ProfileMenu(
            text: "Log Out",
            icon: "assets/icons/Log out.svg",
            press: () {
              logout();
            },
          ),
        ],
      ),
    );
  }
}
