import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:mobile_customer/utils/webservice.dart';

import '../widget/base/base_app_bar.dart';
import '../widget/base/base_scaffold.dart';

class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  AboutModel data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<void> getData() async {
    try {
      var res = await Webservice().get('about/read');
      print(res);
      AboutModel newData = AboutModel.fromJson(res['data'][0]);
      setState(() {
        data = newData;
      });
    } on Error catch (e, s) {
      print(e);
      print(s);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
        backgroudType: 1,
        appBar: BaseAppBar(
          title: 'About Us',
          // bgColor: lightPrimaryColor,
        ),
        body: Stack(children: <Widget>[
          // SquareContainer(height: 320, color: lightPrimaryColor, child: SizedBox.shrink()),
          Align(
              alignment: Alignment.bottomCenter,
              child: Image(
                  // width: double.infinity,
                  width: double.infinity,
                  // height: 300,
                  fit: BoxFit.fill,
                  image: AssetImage('assets/images/aboutus.png'))),

          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    padding: EdgeInsets.only(top: 30, left: 20, right: 20),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                              child: Image(
                                  width: 150,
                                  image: AssetImage(
                                      'assets/images/Logo Color.png'))),
                          SizedBox(
                            height: 15,
                          ),
                          Center(
                              child: Image(
                                  width: 50,
                                  image: AssetImage(
                                      'assets/images/Rectangle.png'))),
                          SizedBox(
                            height: 20,
                          ),

                          // Text(
                          //   data.content,
                          //   textAlign: TextAlign.left,
                          //   style: TextStyle(color: Colors.black),
                          // ),
                        ])),
                Expanded(
                  child: (data == null)
                      ? Center(child: CircularProgressIndicator())
                      : Markdown(data: data.content),
                )
              ])
        ]));
  }
}

class AboutModel {
  int id;
  String title;
  String content;
  String visi;
  String misi;
  bool isActive;

  AboutModel(
      {this.id, this.title, this.content, this.visi, this.misi, this.isActive});

  AboutModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    content = json['content'];
    visi = json['visi'];
    misi = json['misi'];
    isActive = json['is_active'];
  }
}
