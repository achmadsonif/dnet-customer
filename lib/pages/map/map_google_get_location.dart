import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../pages/shop/checkout_controller.dart';
import '../../utils/base_style.dart';
import '../../utils/constants.dart';
import '../../utils/gps_utils.dart';
import '../../widget/base/base_app_bar.dart';
import '../../widget/button/button_rounded.dart';
import '../../widget/button/button_save.dart';
import '../../widget/button/rounded_icon_btn.dart';
import '../../widget/card/base_outline_card.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_api_headers/google_api_headers.dart';

class MapGoogleGetLocation extends StatefulWidget {
  @override
  State<MapGoogleGetLocation> createState() => MapGoogleGetLocationState();
}

class MapGoogleGetLocationState extends State<MapGoogleGetLocation> {
  final checkoutController = Get.put(CheckoutController());
  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(-7.23, 112.74),
    zoom: 14.4746,
  );
  Set<Marker> markers = Set();
  MarkerId selectedMarker;

  GpsLocation mypoint;
  bool loading = false;

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
  }

  void getCurrentLocation() async {
    if (loading) return;

    setState(() {
      loading = true;
    });
    mypoint = await GpsUtils().getLocation();
    if (mypoint != null) {
      await setLocation(LatLng(mypoint.latitude, mypoint.longitude));
      moveCamera();
    }

    setState(() {
      loading = false;
    });
  }

  Future<void> setLocation(LatLng value) async {
    Marker resultMarker = Marker(
      markerId: MarkerId('mylocation'),
      position: value,
    );
    var a = await Geocoder.google(API_KEY_GOOGLE).findAddressesFromCoordinates(
        new Coordinates(value.latitude, value.longitude));
    mypoint = GpsLocation(latitude: value.latitude, longitude: value.longitude);

    setState(() {
      markers.add(resultMarker);
      mypoint = GpsLocation(
          latitude: value.latitude,
          longitude: value.longitude,
          name: a[0].addressLine);
    });
  }

  Future<void> handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: API_KEY_GOOGLE,
      logo: Text(""),
      // onError: onError,
      mode: Mode.overlay,
      types: [],
      strictbounds: false,
      language: "id",
      decoration: InputDecoration(
        hintText: 'Search',
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      components: [Component(Component.country, "id")],
    );
    displayPrediction(p, homeScaffoldKey.currentState);
  }

  Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
    if (p != null) {
      // get detail (lat/lng)
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: API_KEY_GOOGLE,
        apiHeaders: await GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;

      Marker resultMarker = Marker(
        markerId: MarkerId('mylocation'),
        position: LatLng(lat, lng),
      );

      setState(() {
        markers.add(resultMarker);
        mypoint = GpsLocation(
          latitude: lat,
          longitude: lng,
          name: p.description,
        );
      });
      moveCamera();
    }
  }

  Future moveCamera() async {
    final GoogleMapController controller = await _controller.future;
    final _position = CameraPosition(
      target: LatLng(mypoint.latitude, mypoint.longitude),
      zoom: 17,
    );
    controller.animateCamera(CameraUpdate.newCameraPosition(_position));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: homeScaffoldKey,
      appBar: BaseAppBar(
        title: 'Map',
      ),
      body: Stack(
        children: [
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: _kGooglePlex,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
            markers: markers,
            onTap: setLocation,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: ButtonRounded(
                        icon: Icons.search,
                        color: Colors.lightBlue,
                        label: 'Cari alamat',
                        onPressed: handlePressButton,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundedIconBtn(
                        color: Colors.grey[50],
                        icon: Icons.gps_fixed,
                        press: () => getCurrentLocation(),
                      ),
                    ),
                  ],
                ),
                BaseOutlineCard(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        // Text("${location.first.countryName},${location.first.locality}, ${location.first.addressLine},${location.first.featureName}",style: h3,),
                        Text(
                          "${mypoint?.name ?? 'Belum pilih lokasi'}",
                          style: h3,
                        ),
                        if (loading) LinearProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: ButtonSave(
                              label: 'Continue',
                              onPressed: () {
                                Navigator.of(context).pop(mypoint);
                              }),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
