import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:latlong/latlong.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/gps_utils.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/button/rounded_icon_btn.dart';

class MapGetLocation extends StatefulWidget {
  MapGetLocation({Key key}) : super(key: key);

  @override
  _MapGetLocationState createState() => _MapGetLocationState();
}

class _MapGetLocationState extends State<MapGetLocation> {
  final checkoutController = Get.put(CheckoutController());

  // double lat = -7.23;
  // double long = 112.74;
  MapController mapController = MapController();
  GpsLocation mypoint;
  LatLng point = LatLng(-7.23, 112.74);
  String address = '';
  bool loading = false;
  var location;

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
  }

  void getCurrentLocation() async {
    if (loading) return;

    setState(() {
      loading = true;
      address = '';
    });
    mypoint = await GpsUtils().getLocation();

    setState(() {
      loading = false;
      if (mypoint != null) {
        point = LatLng(mypoint.latitude, mypoint.longitude);
        address = mypoint.name;
        mapController.move(LatLng(mypoint.latitude, mypoint.longitude), 17);
      }
    });
    print(point);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Map',
      ),
      body: Stack(
        children: [
          FlutterMap(
            mapController: mapController,
            options: MapOptions(
              onTap: (p) async {
                String newAddress =
                    await GpsUtils().getLocationName(p.latitude, p.longitude);

                setState(() {
                  point = p;
                  address = newAddress;
                  print(p);
                });
              },
              center: LatLng(point.latitude, point.longitude),
              zoom: 17.0,
            ),
            layers: [
              TileLayerOptions(
                  urlTemplate:
                      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                  subdomains: ['a', 'b', 'c']),
              MarkerLayerOptions(
                markers: [
                  Marker(
                    width: 80.0,
                    height: 80.0,
                    point: point,
                    builder: (ctx) => Container(
                      child: Icon(
                        Icons.location_on,
                        color: Colors.red,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 34.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        child: RoundedIconBtn(
                          icon: Icons.gps_fixed,
                          press: () => getCurrentLocation(),
                        ),
                      ),
                    ),
                  ],
                ),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        // Text("${location.first.countryName},${location.first.locality}, ${location.first.addressLine},${location.first.featureName}",style: h3,),
                        Text(
                          "$address",
                          style: h3,
                        ),
                        if (loading) LinearProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: ButtonSave(
                              label: 'Continue',
                              onPressed: () {
                                Navigator.of(context).pop(mypoint);
                              }),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
