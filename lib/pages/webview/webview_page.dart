import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/loading_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  final String url;
  final String title;

  const WebViewPage({Key key, this.url, this.title = ''}) : super(key: key);
  @override
  WebViewPageState createState() => WebViewPageState();
}

class WebViewPageState extends State<WebViewPage> {
  int progress = 0;
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: widget.title,
      ),
      body: Column(
        children: [
          if (progress < 100) LinearProgressIndicator(),
          Expanded(
            child: WebView(
              javascriptMode: JavascriptMode.unrestricted,
              onProgress: (int currentprogress) {
                setState(() {
                  progress = currentprogress;
                });
                print("loading (progress : $currentprogress%)");
              },
              initialUrl: widget.url,
            ),
          ),
        ],
      ),
    );
  }
}
