import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/notif/notif_page.dart';
import 'package:mobile_customer/pages/profile/profile_edit_page.dart';
import 'package:mobile_customer/utils/base_style.dart';
// import 'package:shop_app/screens/cart/cart_screen.dart';

import '../../../utils/size_config.dart';
import 'icon_btn_with_counter.dart';
import 'search_field.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // SearchField(),
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ProfileEditPage()),
              );
            },
            child: Image(
              width: 20,
              image: AssetImage('assets/icons/Profile.png'),
            ),
          ),
          Text(
            'Home',
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          InkWell(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => NotifPage()),
            ),
            child: Image(
              width: 25,
              image: AssetImage('assets/icons/Notifikasi.png'),
            ),
          )
          // IconBtnWithCounter(
          //   svgSrc: "assets/icons/Bell.svg",
          //   numOfitem: 0,
          //   press: () {
          //     Navigator.of(context).push(
          //       MaterialPageRoute(builder: (context) => NotifPage()),
          //     );
          //   },
          // ),
        ],
      ),
    );
  }
}
