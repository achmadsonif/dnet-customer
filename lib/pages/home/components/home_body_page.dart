import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/list_viewx.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../utils/constants.dart';
import '../../../widget/base/base_scaffold.dart';
import '../../../widget/card/base_card.dart';
import '../../auth/global_controller.dart';
import '../../notif/notif_page.dart';
import '../../profile/profile_edit_page.dart';
import '../../service/service_list_page.dart';
import '../../shop/lain_lain/lain_form_page.dart';
import 'categories.dart';
import 'home_header.dart';
import 'home_menu.dart';
import 'profile_banner.dart';

class HomeBodyPage extends StatefulWidget {
  final ValueNotifier<bool> openCloseDial;
  const HomeBodyPage({Key key, this.openCloseDial}) : super(key: key);

  @override
  _HomeBodyPageState createState() => _HomeBodyPageState();
}

class _HomeBodyPageState extends State<HomeBodyPage> {
  final gstate = Get.put(GlobalController());

  GlobalKey<CategoriesState> _keyCategory = GlobalKey();
  GlobalKey<HomeMenuState> _keyHomeMenu = GlobalKey();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    checknotif();
  }

  void onRefresh() {
    WebAuth().getUserFromServer();
    _keyCategory.currentState.refresh();
    _keyHomeMenu.currentState.getData();
    _refreshController.refreshCompleted();
    checknotif();
  }

  void checknotif() {
    WebAuth().getNotif().then((value) {
      if (value) {
        gstate.setNotif(1);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final gstate = Get.put(GlobalController());
    return BaseScaffold(
      appBar: BaseAppBar(
        showNotif: true,
        title: 'Home',
        leading: InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => ProfileEditPage()),
            );
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 8.0),
            child: Image(
              width: 25,
              image: AssetImage('assets/icons/Profile.png'),
            ),
          ),
        ),
      ),
      backgroudType: 2,
      body: ListViewx(
        controller: _refreshController,
        enablePullUp: false,
        onRefresh: onRefresh,
        child: SingleChildScrollView(
          child: Column(
            children: [
              // SizedBox(height: 15),
              // HomeHeader(),
              SizedBox(height: 35),
              ProfileBanner(),
              SizedBox(height: 20),
              if (gstate.user.value.isActive &&
                  gstate.user.value.approvedAt != null) ...[
                Categories(key: _keyCategory),
                HomeMenu(key: _keyHomeMenu),
              ] else ...[
                if (gstate.user.value.approvedAt == null)
                  Padding(
                    padding: const EdgeInsets.only(top: 100),
                    child: Text(
                      'Menunggu proses approval',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                if (gstate.user.value.isActive == false)
                  Padding(
                    padding: const EdgeInsets.only(top: 100),
                    child: Text(
                      'User di Banned',
                      style: TextStyle(fontSize: 20),
                    ),
                  )
              ],
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
      floatingActionButton:
          (gstate.user.value.isActive && gstate.user.value.approvedAt != null)
              ? buildSpeedDial(context)
              : null,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  SpeedDial buildSpeedDial(BuildContext context) {
    return SpeedDial(
      icon: Icons.add,
      activeIcon: Icons.close,
      buttonSize: 50.0,
      childrenButtonSize: 45,
      visible: true,
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      tooltip: 'Speed Dial',
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: primaryColor,
      foregroundColor: Colors.white,
      switchLabelPosition: true,
      openCloseDial: widget.openCloseDial,
      elevation: 2,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
          child: Image.asset(
            'assets/icons/createnewjob.png',
            width: 20,
          ),
          backgroundColor: Colors.white,
          labelWidget: labelFab('Pesan Custom'),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => LainLainFormPage()),
            );
          },
        ),
        SpeedDialChild(
          child: Image.asset(
            'assets/icons/createcustomejob.png',
            width: 20,
          ),
          backgroundColor: Colors.white,
          labelWidget: labelFab('Pesan Teknisi'),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => ServiceListPage()),
            );
          },
        ),
      ],
    );
  }

  Widget labelFab(String text) {
    return BaseCard(
      radius: 25,
      color: primaryColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Text(
          text,
          style: TextStyle(fontSize: 14, color: Colors.white),
        ),
      ),
    );
  }
}
