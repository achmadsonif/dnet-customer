import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/pages/product/product_list_page.dart';
import 'package:mobile_customer/pages/service/service_list_page.dart';
import 'package:mobile_customer/pages/service/service_model.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/list_viewx.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../utils/base_utils.dart';

import 'section_title.dart';

class Categories extends StatefulWidget {
  Categories({Key key}) : super(key: key);

  @override
  CategoriesState createState() => CategoriesState();
}

class CategoriesState extends State<Categories> {
  final checkoutController = Get.put(CheckoutController());

  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  String _searchController = '';
  List<Object> _dataList = [];
  int page = 1;

  @override
  void initState() {
    super.initState();
    refresh();
  }

  Future<void> _loadMore() async {
    Map<String, dynamic> param = {
      'page': page,
      'limit': LENGTH_DATA,
      'sort': 'id',
      'order': 'DESC',
    };
    if (_searchController.isNotEmpty) {
      param['filter[name]'] = _searchController;
    }

    try {
      Map response = await Webservice().get('jobservice/popular', data: param);
      Iterable iter = response['data'];
      List<ServiceModel> newDataList =
          iter.map((i) => ServiceModel.fromJson(i)).toList();
      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> refresh() async {
    page = 1;
    _dataList.clear();
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Widget _buildItemsForListView(BuildContext context, int i) {
    ServiceModel item = _dataList[i];

    return InkWell(
      onTap: () {
        checkoutController.service.value = item;
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ProductListPage(
                    service: item,
                  )),
        );
      },
      child: BaseCard(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: 120,
          child: Column(
            children: [
              Container(
                height: 120,
                width: 100,
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: ProductImage(
                  url: item.iconUrl,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                item.name,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 14),
              ),
              Text(
                item.category.title,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(fontSize: 12),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                item.price.toCurrency(),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    color: Colors.black),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SectionTitle(
            title: "Popular Services",
            press: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => ServiceListPage()),
              );
            },
          ),
        ),
        Container(
          height: 250,
          padding: EdgeInsets.all(20),
          child: ListViewx(
            scrollController: Axis.horizontal,
            enablePullDown: false,
            controller: _refreshController,
            onRefresh: refresh,
            onLoading: _loadMore,
            child: ListView.builder(
                itemCount: _dataList.length,
                itemBuilder: _buildItemsForListView),
          ),
        ),
      ],
    );
  }
}
