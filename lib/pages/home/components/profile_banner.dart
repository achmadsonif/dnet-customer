import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_chip.dart';

import '../home_bottom_nav.dart';

class ProfileBanner extends StatefulWidget {
  const ProfileBanner({
    Key key,
  }) : super(key: key);

  @override
  _ProfileBannerState createState() => _ProfileBannerState();
}

class _ProfileBannerState extends State<ProfileBanner> {
  final gstate = Get.put(GlobalController());

  @override
  void initState() {
    super.initState();
    WebAuth().getUserFromServer();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // Alertx().success('Checkout Berhasil', HomePage());

        // Navigator.of(context).push(
        //   MaterialPageRoute(builder: (context) => ProfileEditPage()),
        // );
      },
      child: Obx(() => Container(
            height: 150,
            decoration: BoxDecoration(
              // color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
            ),
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 5,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Image.asset(
                      'assets/images/bgProfile.png',
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 5,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              gstate.user.value.name,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Flexible(
                                  child: Text(
                                    gstate.user.value.email,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        fontSize: 14),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                (gstate.user.value.emailVerifiedAt != null)
                                    ? CircleAvatar(
                                        radius: 8,
                                        child: Icon(
                                          Icons.check,
                                          color: Colors.white,
                                          size: 10,
                                        ),
                                        backgroundColor: Colors.green,
                                      )
                                    : CircleAvatar(
                                        radius: 8,
                                        child: Icon(
                                          Icons.close,
                                          color: Colors.white,
                                          size: 10,
                                        ),
                                        backgroundColor: Colors.grey,
                                      )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                RatingBar.builder(
                                  ignoreGestures: true,
                                  itemSize: 18,
                                  initialRating:
                                      double.parse(gstate.user.value.rating),
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  onRatingUpdate: (rating) {
                                    print(rating);
                                  },
                                ),
                                Text(
                                  "  ${double.parse(gstate.user.value.rating).toStringAsFixed(1)}/5.0",
                                  style: TextStyle(fontSize: 14),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            radius: 30.0,
                            backgroundImage:
                                NetworkImage(gstate.user.value.photoUrl),
                          ),
                          BaseChip(
                            backgroundColor:
                                HexColor.fromHex(gstate.user.value.level.color),
                            text: gstate.user.value.level.name,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
