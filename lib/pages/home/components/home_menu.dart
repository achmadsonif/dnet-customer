import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/my_order/my_order_detail_page.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/pages/service/service_list_page.dart';
import 'package:mobile_customer/pages/shop/lain_lain/lain_form_page.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/card/base_card.dart';

import '../../../utils/base_utils.dart';
import 'section_title.dart';

class HomeMenu extends StatefulWidget {
  const HomeMenu({
    Key key,
  }) : super(key: key);

  @override
  HomeMenuState createState() => HomeMenuState();
}

class HomeMenuState extends State<HomeMenu> {
  List<OrderModel> _dataList = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<void> getData() async {
    Map<String, dynamic> param = {
      'page': 1,
      'limit': 3,
    };

    try {
      Map response = await Webservice().get('joborder/ongoing', data: param);
      Iterable iter = response['data'];
      List<OrderModel> newDataList =
          iter.map((i) => OrderModel.fromJson(i)).toList();
      setState(() {
        _dataList = newDataList;
      });
    } catch (err) {}
  }

  @override
  Widget build(BuildContext context) {
    final gstate = Get.put(GlobalController());
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SectionTitle(
            title: "Ongoing Services",
            press: () {
              gstate.tabIndex.value = 1;
            },
          ),
        ),
        SizedBox(
          height: 20,
        ),
        for (var item in _dataList)
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) => MyOrderDetailPage(
                          item: item,
                        )),
              );
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: BaseCard(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            Container(
                              width: 30,
                              height: 30,
                              child: ProductImage(url: item.service?.iconUrl),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: (item.service != null)
                                  ? Text(
                                      item.service.name,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(fontSize: 12),
                                    )
                                  : Text(item.projectName ?? '',
                                      style: TextStyle(fontSize: 12)),
                            ),
                            if (item.service == null)
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: BaseCard(
                                  color: Color(0xff32BEA6),
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Text(
                                      'Custom',
                                      style: TextStyle(
                                          fontSize: 10, color: Colors.white),
                                    ),
                                  ),
                                  radius: 15,
                                ),
                              )
                          ],
                        ),
                      ),
                      Text(
                          (item.grandTotal != null)
                              ? item.grandTotal.toCurrency()
                              : '',
                          style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
          )
        // SpecialOfferCard(
        //   image: "assets/images/ImageBanner2.png",
        //   category: "Pesan Teknisi",
        //   desc: '',
        //   press: () {
        //     Navigator.of(context).push(
        //       MaterialPageRoute(builder: (context) => ServiceListPage()),
        //     );
        //   },
        // ),
        // SpecialOfferCard(
        //   image: "assets/images/ImageBanner3.png",
        //   category: "Pesan Custom",
        //   desc: '',
        //   press: () {
        //     Navigator.of(context).push(
        //       MaterialPageRoute(builder: (context) => LainLainFormPage()),
        //     );
        //   },
        // ),
      ],
    );
  }
}

class SpecialOfferCard extends StatelessWidget {
  const SpecialOfferCard({
    Key key,
    @required this.category,
    @required this.image,
    @required this.desc,
    @required this.press,
  }) : super(key: key);

  final String category, image;
  final String desc;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: GestureDetector(
        onTap: press,
        child: SizedBox(
          height: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  child: Image.asset(
                    image,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFF343434).withOpacity(0.4),
                        Color(0xFF343434).withOpacity(0.15),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 15.0,
                    vertical: 10,
                  ),
                  child: Text.rich(
                    TextSpan(
                      style: TextStyle(color: Colors.white),
                      children: [
                        TextSpan(
                          text: "$category\n",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(text: "$desc")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
