import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/my_order/my_order_history.dart';
import 'package:mobile_customer/pages/my_order/my_order_page.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/pages/profile/profile_page.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/pages/blank_page.dart';
import 'package:get/get.dart';

import 'components/home_body_page.dart';

class HomePage extends StatefulWidget {
  final int tabIndex;
  HomePage({Key key, this.tabIndex}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final gstate = Get.put(GlobalController());
  ValueNotifier<bool> isDialOpen = ValueNotifier(false);

  List<Widget> _children = [];

  @override
  void initState() {
    _children = [
      HomeBodyPage(
        openCloseDial: isDialOpen,
      ),
      MyOrderPage(),
      ProfilePage(),
    ];
    Future.delayed(Duration(seconds: 2), () {
      if (widget.tabIndex != null) gstate.tabIndex.value = widget.tabIndex;
    });
    super.initState();
  }

  void onTabTapped(int index) {
    gstate.tabIndex.value = index;
  }

  Future<bool> _onWillPop() async {
    if (isDialOpen.value) {
      isDialOpen.value = false;
      return false;
    }
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            contentPadding:
                EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 0),

            // title: new Text('Are you sure?'),
            content: new Text('Yakin ingin keluar aplikasi ?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Tidak'),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Ya'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Obx(() => _children[gstate.tabIndex.value]), //
        bottomNavigationBar: Obx(
          () => BottomNavigationBar(
            backgroundColor: Colors.white,
            elevation: 0,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            onTap: onTabTapped, // new
            currentIndex: gstate.tabIndex.value, // new
            items: [
              BottomNavigationBarItem(
                icon: Image(
                  width: 25,
                  image: AssetImage('assets/icons/Icon Home - bw.png'),
                ),
                label: '',
                activeIcon: Image(
                  width: 25,
                  image: AssetImage('assets/icons/Icon Home.png'),
                ),
              ),
              BottomNavigationBarItem(
                icon: Image(
                  width: 25,
                  image: AssetImage('assets/icons/Job - bw.png'),
                ),
                label: '',
                activeIcon: Image(
                  width: 25,
                  image: AssetImage('assets/icons/Job.png'),
                ),
              ),
              // BottomNavigationBarItem(
              //   icon: Icon(Icons.self_improvement, color: Colors.grey),
              //   label: '',
              //   activeIcon: Icon(Icons.self_improvement, color: primaryColor),
              // ),
              BottomNavigationBarItem(
                icon: Image(
                  width: 25,
                  image: AssetImage('assets/icons/setting - bw.png'),
                ),
                label: '',
                activeIcon: Image(
                  width: 25,
                  image: AssetImage('assets/icons/setting.png'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
