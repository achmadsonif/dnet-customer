import 'dart:async';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/register_attachment_model.dart';
import 'package:mobile_customer/pages/auth/register_attach_page.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/notif_utils.dart';
import 'package:mobile_customer/utils/size_config.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:timer_count_down/timer_count_down.dart';
import '../../utils/constants.dart';
import '../../utils/webservice.dart';
import 'web_auth.dart';
import '../../widget/alertx.dart';
import '../../widget/button/button_save.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpPage extends StatefulWidget {
  final String phone;
  @override
  _OtpPageState createState() => _OtpPageState();

  OtpPage({this.phone});
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController textEditingController = TextEditingController();
  StreamController<ErrorAnimationType> errorController;

  bool hasError = false;
  String phone = "";
  bool isWaiting = false;

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  Future<void> submitSignIn() async {
    if (phone.isEmpty) {
      return Alertx.warning('OTP number cannot be empty');
    }
    var data = {
      'token': phone.trim(),
    };
    await Webservice()
        .post("auth/activate/phone", data: data)
        .then((value) => getAttach());
  }

  Future<void> getAttach() async {
    try {
      Map response = await Webservice().get('auth/register/attach');
      print(response);
      Iterable iter = response['required'];
      List<RegisterAttachment> dataList =
          iter.map((i) => RegisterAttachment.fromJson(i)).toList();
      if (dataList.length > 0) {
        Get.offAll(RegisterAttachPage(dataList: dataList));
      } else {
        try {
          await WebAuth().registerDone();
          Get.offAll(HomePage());
        } on Error catch (e) {
          print(e);
        }
      }
    } catch (e) {
      print(e);
      // Alertx.warning('Gagal Load Lampiran');
    }
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  void resendCode() async {
    if (isWaiting) return;
    var data = {
      'phone': widget.phone,
    };
    try {
      setState(() {
        isWaiting = true;
      });
      await Webservice().post("auth/resend/phone", data: data);
    } on Error catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        showBackButton: false,
        title: 'OTP',
        showNotif: false,
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Text(
            "Verify Phone Number",
            style: h1,
            textAlign: TextAlign.center,
          ),
          Text(
            'Enter the 6-digit code sent to +62${int.parse(widget.phone)}',
            textAlign: TextAlign.center,
            style: h3,
          ),
          SizedBox(
            height: 40,
          ),
          Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
              child: PinCodeTextField(
                appContext: context,
                length: 6,
                autoFocus: true,
                obscureText: false,
                blinkWhenObscuring: true,
                animationType: AnimationType.fade,
                validator: (v) {
                  if (v.length < 6) {
                    return "";
                  } else {
                    return null;
                  }
                },
                pinTheme: PinTheme(
                    activeColor: primaryColor,
                    selectedColor: primaryColor,
                    inactiveColor: Colors.grey),
                cursorColor: Colors.black,
                animationDuration: Duration(milliseconds: 300),
                errorAnimationController: errorController,
                controller: textEditingController,
                keyboardType: TextInputType.number,
                onCompleted: (v) {
                  print("Completed");
                },
                onChanged: (value) {
                  print(value);
                  setState(() {
                    phone = value;
                  });
                },
                beforeTextPaste: (text) {
                  print("Allowing to paste $text");
                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                  return false;
                },
              )),
          SizedBox(
            height: 30,
          ),
          ButtonSave(
            label: 'Continue',
            onPressed: submitSignIn,
          ),
          SizedBox(
            height: 30,
          ),
          (isWaiting)
              ? buildTimer()
              : InkWell(
                  onTap: resendCode,
                  child: Align(
                    alignment: Alignment.center,
                    child: RichText(
                      text: TextSpan(
                        text: 'Dont receive code ? ',
                        style: h3,
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Resend Again ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: secondaryColor)),
                        ],
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  Widget buildTimer() {
    return Countdown(
      seconds: 20,
      build: (BuildContext context, double time) =>
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text("Please wait to request otp again "),
        Text(
          "00:${time.toInt().toString().padLeft(2, '0')}",
          style: TextStyle(color: primaryColor),
        )
      ]),
      interval: Duration(milliseconds: 100),
      onFinished: () {
        print('Timer is done!');
        setState(() {
          isWaiting = false;
        });
      },
    );
  }
}
