import 'package:get/get.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/notif_utils.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/form/form_input.dart';

import 'register_page.dart';

import '../../utils/constants.dart';
import '../../utils/webservice.dart';

import 'forgot_password_page.dart';
import 'web_auth.dart';
import '../../widget/alertx.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  static final String path = "lib/src/pages/login/login7.dart";
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool passwordVisible = false;
  bool isLoading = false;
  bool isLoadingSignUp = false;

  @override
  void initState() {
    super.initState();
    _emailController.text = '';
    _passwordController.text = '';
  }

  Future<void> submitSignIn() async {
    if (_key.currentState.validate()) {
      var data = {
        'email': _emailController.text.trim(),
        'password': _passwordController.text.trim()
      };
      try {
        var res = await Webservice().post(
          "auth/login",
          data: data,
        );
        if (res != null) {
          await WebAuth().login(res);
          Webservice().post('auth/setdevice/',
              data: {'device_id': await NotifUtils().getToken()},
              showDialog: false);

          await WebAuth().getUserFromServer();
          Get.offAll(HomePage());
        }
      } on Error catch (e, s) {
        print(e);
        print(s);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'LOGIN',
        showNotif: false,
      ),
      body: Form(
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            Text(
              "Welcome Back",
              style: h1,
              textAlign: TextAlign.center,
            ),
            Text(
              'Sign in with your email and password  \nor continue with phone number',
              textAlign: TextAlign.center,
              style: h3,
            ),
            SizedBox(
              height: 40,
            ),
            FormInput(
              title: 'No Hp/Email',
              placeholder: 'No Hp / Email',
              controller: _emailController,
              validatorRequired: true,
              // keyboardType: TextInputType.emailAddress,
              icon: Icons.email,
            ),
            FormInput(
                title: 'Password',
                placeholder: 'Password',
                controller: _passwordController,
                validatorRequired: true,
                // keyboardType: TextInputType.emailAddress,
                icon: Icons.lock,
                obscureText: !passwordVisible),
            SizedBox(
              height: 25,
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(32, 10, 10, 10),
              child: Align(
                alignment: Alignment.bottomRight,
                child: InkWell(
                  child: Text(
                    "FORGOT PASSWORD ?",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.w700),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ForgotPasswordPage()));
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            ButtonSave(
              label: 'Login',
              onPressed: submitSignIn,
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Belum Punya Akun? ",
                  style: TextStyle(fontSize: 16),
                ),
                GestureDetector(
                  onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => RegisterPage())),
                  child: Text(
                    "Sign Up",
                    style: TextStyle(fontSize: 16, color: primaryColor),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
