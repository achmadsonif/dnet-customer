import 'package:flutter/services.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/utils/notif_utils.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';

import '../../utils/constants.dart';
import '../../utils/webservice.dart';
import '../../widget/alertx.dart';
import '../../widget/base/base_app_bar.dart';
import '../../widget/button/button_save.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as g;
import 'package:file_picker/file_picker.dart';
import 'package:dio/dio.dart' as d;

import 'register_attachment_model.dart';

class RegisterAttachPage extends StatefulWidget {
  final List<RegisterAttachment> dataList;

  @override
  _RegisterAttachPageState createState() => _RegisterAttachPageState();

  RegisterAttachPage({this.dataList});
}

class _RegisterAttachPageState extends State<RegisterAttachPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  Future<String> attachListFuture;
  List<TextEditingController> listController = [];
  List<FilePickerResult> _paths = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.dataList.length; i++) {
      setState(() {
        listController.insert(i, new TextEditingController());
        _paths.insert(i, null);
      });
    }
  }

  Future<void> save() async {
    for (int i = 0; i < widget.dataList.length; i++) {
      if (listController[i].text.isEmpty) {
        Alertx.warning(widget.dataList[i].name + ' Required');
        break;
      }
    }
    bool success = false;
    for (int i = 0; i < widget.dataList.length; i++) {
      var data = {
        "filename": widget.dataList[i].name,
        "file": await d.MultipartFile.fromFile(_paths[i].files.first.path,
            filename: _paths[i].files.first.name),
        "type": 'tech',
        "attachment_id": widget.dataList[i].id
      };
      await Webservice()
          .post('auth/register/attach', data: data, isForm: true)
          .then((value) => success = true);
    }
    if (success) {
      try {
        await WebAuth().registerDone();

        Get.offAll(HomePage());
      } on Error catch (e) {
        print(e);
      }
    }
  }

  void _openFileExplorer(int idx) async {
    Alertx.loading();
    try {
      _paths[idx] = (await FilePicker.platform.pickFiles(
        type: FileType.any,
        allowMultiple: false,
      ));
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
    if (!mounted) return;
    setState(() {
      listController[idx].text =
          _paths[idx] != null ? _paths[idx].files.first.name : '';
    });
    if (g.Get.isDialogOpen) g.Get.back();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Register Lampiran',
        showNotif: false,
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Text("Register Lampiran", style: headingStyle),
            Text(
              'Complete your detail information to register',
              textAlign: TextAlign.center,
              style: h3,
            ),
            SizedBox(
              height: 40,
            ),
            for (int i = 0; i < widget.dataList.length; i++)
              generateFilePicker(widget.dataList[i], i),
            widget.dataList.length > 0 ? SizedBox(height: 15.0) : Text(''),
            widget.dataList.length > 0
                ? Container(
                    width: double.infinity,
                    child: ButtonSave(
                      label: 'Register',
                      onPressed: save,
                    ))
                : Text('')
          ],
        ),
      ),
    );
  }

  // Progress indicator widget to show loading.
  Widget loadingView() => Center(
        child: CircularProgressIndicator(
          backgroundColor: primaryColor,
        ),
      );
  // View to empty data message
  Widget noDataView(String msg) => Center(
        child: Text(
          msg,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800),
        ),
      );

  Widget generateFilePicker(RegisterAttachment regattach, int idx) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                readOnly: true,
                controller: listController[idx],
                onTap: () {
                  _openFileExplorer(idx);
                },
                decoration: InputDecoration(
                    labelText: regattach.name,
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: primaryColor, width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    // If  you are using latest version of flutter then lable text and hint text shown like this
                    // if you r using flutter less then 1.20.* then maybe this is not working properly
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    suffixIcon: Icon(
                      Icons.file_upload,
                      color: Colors.black,
                    )),
              )
            ]));
  }
}
