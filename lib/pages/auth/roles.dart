import '../../pages/auth/web_auth.dart';

import 'response/login_response.dart';

enum Role {
  user,
  admin,
  guest
}

class Roles {

  Future<bool> isAdmin() async {
    Role role = await getRole();
    if (role == Role.admin) {
      return true;
    }
    return false;
  }

  Future<Role> getRole() async {
    LoginResponse login = await WebAuth.getLogin();
    Role role = roleFromString(login.roles[0]) ?? Role.guest;
    return role;
  }

  Role roleFromString(String value) {
    return Role.values.firstWhere((type) => type.toString().split(".").last == value,
        orElse: () => null);
  }
}