import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/login_page.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/form/form_input.dart';
import 'package:mobile_customer/widget/pages/success_page.dart';

import '../../utils/webservice.dart';
import '../../widget/alertx.dart';
import '../../widget/button/button_rounded.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'forgot_password_token_page.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: BaseAppBar(
        title: 'Forgot Password',
        showNotif: false,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: Form(
            key: _key,
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Forgot Pasword',
                  textAlign: TextAlign.center,
                  style: h1,
                ),
                Text(
                  'Enter your email address and we will send you a reset instruction',
                  textAlign: TextAlign.center,
                  style: h3,
                ),
                SizedBox(
                  height: 40,
                ),
                FormInput(
                  title: 'Email',
                  controller: _emailController,
                  icon: Icons.email,
                  validatorRequired: true,
                ),
                SizedBox(
                  height: 20,
                ),
                ButtonSave(
                  // icon: Icons.send,
                  label: 'Send Mail',
                  onPressed: resetPassword,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> resetPassword() async {
    if (_key.currentState.validate()) {
      var data = {"email": _emailController.text.trim()};

      await Webservice()
          .post('auth/reset_password', data: data)
          .then((value) => Get.off(ForgotTokenPage()));
    }
  }
}
