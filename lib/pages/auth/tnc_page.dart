import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:open_file/open_file.dart';
import 'package:get/get.dart' as g;

class TncPage extends StatefulWidget {
  @override
  TncPageState createState() => TncPageState();
}

class TncPageState extends State<TncPage> {
  int progress = 0;
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  Future<String> downloadFile(String url, String fileName) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';

    Alertx.loading();
    try {
      Directory appDocDirectory = await getApplicationDocumentsDirectory();

      new Directory(appDocDirectory.path + '/' + 'timteknis')
          .create(recursive: true)
          // The created directory is returned as a Future.
          .then((Directory directory) {
        print('Path of New Dir: ' + directory.path);
      });

      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if (response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = appDocDirectory.path + '/' + 'timteknis/' + fileName;
        file = File(filePath);
        await file.writeAsBytes(bytes);
        OpenFile.open(appDocDirectory.path + '/' + 'timteknis/' + fileName);
      } else
        filePath = 'Error code: ' + response.statusCode.toString();
    } catch (ex, st) {
      print(ex.toString());
      print(st.toString());
      filePath = 'Can not fetch url';
    }

    if (g.Get.isDialogOpen) g.Get.back();
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Terms & Conditions',
        showNotif: false,
      ),
      body: Column(
        children: [
          if (progress < 100) LinearProgressIndicator(),
          Expanded(
            child: Container(
              child: WebView(
                // javascriptMode: JavascriptMode.unrestricted,
                onProgress: (int currentprogress) {
                  setState(() {
                    progress = currentprogress;
                  });
                  print("loading (progress : $currentprogress%)");
                },
                initialUrl: URL + 'termcondition/read',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: ButtonSave(
              label: 'Download',
              onPressed: () => downloadFile(URL + "termcondition/read?download",
                  "timteknis_customer_tnc.pdf"),
            ),
          ),
        ],
      ),
    );
  }
}
