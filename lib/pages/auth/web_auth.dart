import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/utils/notif_utils.dart';
import 'package:mobile_customer/utils/webservice.dart';
import '../../pages/auth/login_page.dart';
import '../../main.dart';
import '../../pages/auth/response/login_response.dart';
import '../../widget/alertx.dart';

import 'global_controller.dart';
import 'register_attach_page.dart';
import 'register_attachment_model.dart';
import 'user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../utils/base_utils.dart';

class WebAuth {
  static const String TOKEN = 'token';
  static const String USER = 'user';
  static const String LOGINDATA = 'logindata';

  static Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? '';
    return token;
  }

  Future<User> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String userStr = prefs.getString('user');
    if (userStr != null) {
      User user = User.fromJson(json.decode(userStr));
      return user;
    }
    return null;
  }

  Future<User> getUserFromServer() async {
    try {
      Map response = await Webservice().get('profile/read');
      User user = User.fromJson(response['data']);
      if (user.isActive == false) {
        WebAuth.logOut(auto: true);
      } else {
        updateUser(user);
        final gstate = Get.put(GlobalController());
        gstate.setUser(user);
      }
      if (user.status == -1) {
        try {
          Map response = await Webservice().get('auth/register/attach');
          print(response);
          Iterable iter = response['required'];
          List<RegisterAttachment> dataList =
              iter.map((i) => RegisterAttachment.fromJson(i)).toList();
          if (dataList.length > 0) {
            Get.offAll(RegisterAttachPage(dataList: dataList));
          } else {
            await WebAuth().registerDone();
            Get.offAll(HomePage());
          }
        } catch (e) {
          print(e);
          // Alertx.warning('Gagal Load Lampiran');
        }
      }
      return user;
    } catch (e) {
      print(e);
    }
    return null;
  }

  Future<bool> getNotif() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String lastSeenNotif = prefs.getString('lastSeenNotif');
      if (lastSeenNotif == null) {
        lastSeenNotif = DateTime.now().toDateNotif();
        setNotif();
      }
      Map response = await Webservice()
          .get('notification/read?last_seen=' + lastSeenNotif);
      bool t = false;
      if (response['data'].length > 0) t = true;
      return t;
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<void> setNotif() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('lastSeenNotif', DateTime.now().toDateNotif());
  }

  Future<void> updateUser(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(USER, json.encode(user.toJson()));
  }

  Future<void> login(Map response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // LoginResponse loginResponse = LoginResponse.fromJson(response["data"]);

    // await prefs.setString(TOKEN, loginResponse.accessToken);
    await prefs.setString(USER, json.encode(response["data"]));
    final gstate = Get.put(GlobalController());
    gstate.reset();

    // User user =User.fromJson(response.data);
    // await prefs.setString(USER, json.encode(user.toJson()));
  }

  static Future<LoginResponse> getLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String dataString = prefs.getString(LOGINDATA);
    LoginResponse log = LoginResponse.fromJson(json.decode(dataString));
    return log;
  }

  static Future<bool> logOut({bool auto = false}) async {
    bool confirm;
    if (auto) {
      confirm = true;
    } else {
      confirm = await showDialog(
            context: Get.context,
            builder: (context) => AlertDialog(
              // title: new Text('Are you sure?'),
              content: Text('Yakin ingin logout ?'),
              contentPadding:
                  EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 0),

              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: Text('Batal'),
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: Text('Logout'),
                ),
              ],
            ),
          ) ??
          false;
    }
    if (confirm) {
      await Webservice().get('auth/logout');
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove(TOKEN);
      prefs.remove(USER);
      prefs.remove(LOGINDATA);
      final ck = Get.put(CheckoutController());
      ck.reset();
      Get.offAll(LoginPage());
    }
  }

  Future<void> registerDone() async {
    await Webservice().get('auth/register/done');
    await Webservice().post('auth/setdevice/',
        data: {'device_id': await NotifUtils().getToken()});
    await WebAuth().getUserFromServer();
  }
}
