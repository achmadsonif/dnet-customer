class LoginResponse {
  String username;
  List<String> roles;
  String expiresIn;
  String accessToken;

  LoginResponse({this.username, this.roles, this.expiresIn, this.accessToken});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    roles = json['roles'].cast<String>();
    expiresIn = json['expiresIn'];
    accessToken = json['accessToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['roles'] = this.roles;
    data['expiresIn'] = this.expiresIn;
    data['accessToken'] = this.accessToken;
    return data;
  }
}
