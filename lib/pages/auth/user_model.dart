class User {
  int id;
  String name;
  String gender;
  String email;
  String emailVerifiedAt;
  String phone;
  String phoneVerifiedAt;
  String address;
  String approvedAt;
  String rating;
  int status;
  bool isActive;
  int levelId;
  String createdAt;
  String updatedAt;
  String photoUrl;
  Level level;

  User(
      {this.id,
      this.name,
      this.gender,
      this.email,
      this.emailVerifiedAt,
      this.phone,
      this.phoneVerifiedAt,
      this.address,
      this.approvedAt,
      this.rating,
      this.status,
      this.isActive,
      this.levelId,
      this.createdAt,
      this.updatedAt,
      this.photoUrl,
      this.level});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    gender = json['gender'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    phone = json['phone'];
    phoneVerifiedAt = json['phone_verified_at'];
    address = json['address'];
    approvedAt = json['approved_at'];
    rating = json['rating'];
    status = json['status'];
    isActive = json['is_active'];
    levelId = json['level_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    photoUrl = json['photoUrl'];
    level = json['level'] != null ? new Level.fromJson(json['level']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['gender'] = this.gender;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['phone'] = this.phone;
    data['phone_verified_at'] = this.phoneVerifiedAt;
    data['address'] = this.address;
    data['approved_at'] = this.approvedAt;
    data['rating'] = this.rating;
    data['status'] = this.status;
    data['is_active'] = this.isActive;
    data['level_id'] = this.levelId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['photoUrl'] = this.photoUrl;
    if (this.level != null) {
      data['level'] = this.level.toJson();
    }
    return data;
  }
}

class Level {
  int id;
  String name;
  String color;
  String max;
  String min;
  String type;

  Level({this.id, this.name, this.color, this.max, this.min, this.type});

  Level.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    color = json['color'];
    max = json['max'];
    min = json['min'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['color'] = this.color;
    data['max'] = this.max;
    data['min'] = this.min;
    data['type'] = this.type;
    return data;
  }
}
