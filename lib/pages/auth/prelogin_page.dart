import 'package:mobile_customer/pages/auth/login_page.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/size_config.dart';
import '../../widget/button/button_save.dart';
import 'package:flutter/material.dart';

class PreLoginPage extends StatefulWidget {
  @override
  _PreLoginPageState createState() => _PreLoginPageState();
}

class _PreLoginPageState extends State<PreLoginPage> {
  int currentPage = 0;
  PageController controller = PageController();
  List<Map<String, String>> splashData = [
    {
      "text": "Quality IT Solution",
      "desc": "Menyediakan berbagai solusi \nIT dengan teknisi \nberpengalaman",
      "image": "assets/images/quality.png"
    },
    {
      "text": "Big Project",
      "desc": "Butuh pasang BTS? \natau project besar lain? \nTimteknis bisa",
      "image": "assets/images/BigProject.png"
    },
    {
      "text": "Payment",
      "desc":
          "Transaksi cepat, mudah dan aman \nkarena kami integrasi dengan \nsistem payment terpecaya",
      "image": "assets/images/payment.png"
    },
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        body: SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              child: PageView.builder(
                controller: controller,
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: splashData.length,
                itemBuilder: (context, index) => SplashContent(
                  image: splashData[index]["image"],
                  text: splashData[index]['text'],
                  desc: splashData[index]['desc'],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      splashData.length,
                      (index) => buildDot(index: index),
                    ),
                  ),
                  SizedBox(height: 30),
                  (currentPage == (splashData.length - 1))
                      ? ButtonSave(
                          label: "GET STARTED",
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LoginPage()));
                          },
                        )
                      : ButtonSave(
                          label: "NEXT",
                          onPressed: () {
                            setState(() {
                              currentPage = currentPage + 1;
                            });
                            controller.nextPage(
                                duration: Duration(milliseconds: 500),
                                curve: Curves.easeIn);
                          },
                        ),
                  SizedBox(height: 30),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    child: Text(
                      (currentPage == (splashData.length - 1)) ? '' : 'Skip',
                      style: TextStyle(decoration: TextDecoration.underline),
                    ),
                  ),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }

  AnimatedContainer buildDot({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? primaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }

  Widget SplashContent({String image, String text, String desc}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Image.asset(
          image,
          height: 250,
          width: double.infinity,
        ),
        Column(
          children: [
            Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 36,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            Text(
              desc,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ],
    );
  }
}
