import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/login_page.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/form/form_input.dart';

import '../../utils/constants.dart';
import '../../utils/webservice.dart';

import '../../widget/alertx.dart';
import '../../widget/button/button_rounded.dart';
import 'package:flutter/material.dart';

class ForgotTokenPage extends StatefulWidget {
  static final String path = "lib/src/pages/login/login7.dart";
  @override
  _ForgotTokenPageState createState() => _ForgotTokenPageState();
}

class _ForgotTokenPageState extends State<ForgotTokenPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _tokenController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool passwordVisible = false;
  bool isLoading = false;
  bool isLoadingSignUp = false;

  @override
  void initState() {
    super.initState();
    _tokenController.text = '';
    _passwordController.text = '';
  }

  Future<void> setPassword() async {
    if (_key.currentState.validate()) {
      var data = {
        'token': _tokenController.text.trim(),
        'password': _passwordController.text.trim()
      };
      var res = await Webservice().post("auth/set_password", data: data);
      if (res != null) {
        Alertx.warning('Password Successfully changed');
        Get.off(LoginPage());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'LOGIN',
      ),
      body: Form(
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            Text(
              "Reset Password",
              style: headingStyle,
              textAlign: TextAlign.center,
            ),
            Text(
              'Set your new password with token that you receive in email',
              textAlign: TextAlign.center,
              style: h3,
            ),
            SizedBox(
              height: 40,
            ),
            FormInput(
              title: 'Token',
              placeholder: 'Enter Your Token',
              controller: _tokenController,
              validatorRequired: true,
              icon: Icons.vpn_key,
            ),
            SizedBox(
              height: 20,
            ),
            FormInput(
                title: 'Password',
                placeholder: 'Enter Your Password',
                controller: _passwordController,
                validatorRequired: true,
                icon: Icons.lock,
                obscureText: !passwordVisible),
            SizedBox(
              height: 20,
            ),
            ButtonRounded(
              color: primaryColor,
              label: 'Set Password',
              onPressed: setPassword,
            ),
          ],
        ),
      ),
    );
  }
}
