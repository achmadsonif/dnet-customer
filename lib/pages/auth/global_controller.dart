import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'user_model.dart';

class GlobalController extends GetxController {
  var user = User().obs;
  void setUser(User val) => user.value = val;

  var tabIndex = 0.obs;
  var totalNotif = 0.obs;

  setNotif(int n) async {
    totalNotif.value = n;
  }

  reset() {
    tabIndex.value = 0;
  }
}
