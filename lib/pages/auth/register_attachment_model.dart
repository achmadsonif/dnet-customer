class RegisterAttachment {
  int id;
  String name;
  String description;
  String type;
  String validAt;

  RegisterAttachment(
      {this.id, this.name, this.description, this.type, this.validAt});

  RegisterAttachment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    type = json['type'];
    validAt = json['valid_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['type'] = this.type;
    data['valid_at'] = this.validAt;
    return data;
  }
}

