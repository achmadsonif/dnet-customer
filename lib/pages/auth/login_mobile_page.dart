import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';

import 'otp_page.dart';

import '../../utils/constants.dart';
import '../../utils/webservice.dart';

import 'web_auth.dart';
import '../../widget/alertx.dart';
import '../../widget/button/button_rounded.dart';
import 'package:flutter/material.dart';

class LoginMobilePage extends StatefulWidget {
  static final String path = "lib/src/pages/login/login7.dart";
  @override
  _LoginMobilePageState createState() => _LoginMobilePageState();
}

class _LoginMobilePageState extends State<LoginMobilePage> {
  TextEditingController _phoneController = TextEditingController();
  bool isLoading = false;
  bool isLoadingSignUp = false;

  @override
  void initState() {
    super.initState();
    _phoneController.text = '';
  }

  Future<void> submitSignIn() async {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => OtpPage()),
    );
    return;
    if (_phoneController.text.isEmpty) {
      return Alertx.warning('Phone number cannot be empty');
    }
    var data = {
      'phone': _phoneController.text.trim().toLowerCase(),
    };
    var res = await Webservice().post("auth/login", data: data);
    if (res != null) {
      await WebAuth().login(res);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'LOGIN',
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Text(
            'Get started Dnet',
            textAlign: TextAlign.center,
            style: h1,
          ),
          Text(
            'Enter your phone number to use application',
            textAlign: TextAlign.center,
            style: h3,
          ),
          SizedBox(
            height: 40,
          ),
          Material(
            elevation: 2.0,
            borderRadius: BorderRadius.all(Radius.circular(30)),
            child: TextField(
              controller: _phoneController,
              autofocus: true,
              keyboardType: TextInputType.number,
              // onChanged: (String value){},
              cursorColor: Colors.deepOrange,
              decoration: InputDecoration(
                  hintText: "Phone number",
                  prefixIcon: Material(
                    elevation: 0,
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    child: Icon(
                      Icons.phone,
                      color: primaryColor,
                    ),
                  ),
                  border: InputBorder.none,
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          ButtonRounded(
            color: primaryColor,
            label: 'Continue',
            onPressed: submitSignIn,
          ),
        ],
      ),
    );
  }
}
