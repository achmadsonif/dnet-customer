import 'package:flutter/services.dart';
import 'package:mobile_customer/pages/auth/otp_page.dart';
// import 'package:mobile_customer/pages/auth/register_attach_page.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/notif_utils.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/form/form_input.dart';
import 'package:mobile_customer/widget/form/form_radio.dart';
import '../../widget/base/base_app_bar.dart';
import '../../utils/webservice.dart';

import 'package:flutter/material.dart';

import 'tnc_page.dart';
// import 'package:mobile_customer/pages/auth/register_attachment_model.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _fullnameController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _hpController = new TextEditingController();

  List<RadioModel> listItemRadio = [
    RadioModel('Laki-Laki', 'Laki-Laki'),
    RadioModel('Perempuan', 'Perempuan'),
  ];
  RadioModel _genderSelected;
  bool acceptTnc = false;

  Future<void> save() async {
    if (!acceptTnc) {
      Alertx.warning("Please Accept Term & Condition Before Submit!!!");
      return;
    }
    if (_key.currentState.validate()) {
      String phone = '0' + int.parse(_hpController.text.trim()).toString();
      var data = {
        "name": _fullnameController.text.trim(),
        "gender": _genderSelected.id,
        "email": _emailController.text.trim(),
        "address": _addressController.text.trim(),
        "password": _passwordController.text.trim(),
        "phone": phone,
      };

      try {
        await Webservice().post('auth/register/', data: data);

        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => OtpPage(
                    phone: phone,
                  )),
        );
      } on Error catch (e) {
        print(e);
      }
    }
  }

  Future<void> getAttach() async {
    try {
      Map response = await Webservice().get('auth/register/attach');
      print(response);
      Iterable iter = response['required'];
      // List<RegisterAttachment> dataList = iter.map((i) => RegisterAttachment.fromJson(i)).toList();
      // Get.to(RegisterAttachPage(dataList: dataList));
    } catch (e) {
      print(e);
      // Alertx.warning('Gagal Load Lampiran');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Daftar',
        showNotif: false,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              Text("Form Pendaftaran", style: h1),
              Text(
                'Isi biodata anda',
                textAlign: TextAlign.center,
                style: h3,
              ),
              SizedBox(
                height: 40,
              ),
              FormInput(
                title: 'Nama Lengkap',
                controller: _fullnameController,
                validatorRequired: true,
              ),
              FormInput(
                title: 'Alamat',
                controller: _addressController,
                validatorRequired: true,
                maxLine: 3,
              ),
              FormInput(
                title: 'No Hp',
                controller: _hpController,
                validatorRequired: true,
                keyboardType: TextInputType.number,
                prefix: Container(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    '+62',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              FormInput(
                title: 'Email',
                controller: _emailController,
                validatorRequired: true,
                keyboardType: TextInputType.emailAddress,
              ),
              FormRadio(
                title: 'Jenis Kelamin',
                selected: _genderSelected,
                listItem: listItemRadio,
                validatorRequired: true,
                onChanged: (value) {
                  setState(() {
                    _genderSelected = value;
                  });
                },
              ),
              FormInput(
                title: 'Password',
                controller: _passwordController,
                validatorRequired: true,
                obscureText: true,
              ),
              SizedBox(height: 15.0),
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Checkbox(
                    focusColor: Colors.lightBlue,
                    activeColor: Colors.orange,
                    value: acceptTnc,
                    onChanged: (newValue) {
                      setState(() => acceptTnc = newValue);
                    }),
                SizedBox(width: 20.0),
                Text('Accept '),
                GestureDetector(
                    onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => TncPage())),
                    child: Text('Terms & Conditions',
                        style: TextStyle(fontSize: 16, color: primaryColor)))
              ]),
              SizedBox(height: 15.0),
              Container(
                  // width: double.infinity,
                  child: ButtonSave(
                label: 'Register',
                onPressed: save,
              ))
            ],
          ),
        ),
      ),
    );
  }
}
