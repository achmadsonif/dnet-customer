import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/chat/chat_page.dart';
import 'package:mobile_customer/pages/complaint/complaint_page.dart';
import 'package:mobile_customer/pages/my_order/resume_order_short.dart';
import 'package:mobile_customer/pages/my_order/job_task_page.dart';
import 'package:mobile_customer/pages/payment/payment_page.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_rounded.dart';

import 'order_model.dart';

class MyOrderDetailPageOld extends StatelessWidget {
  final OrderModel item;
  const MyOrderDetailPageOld({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Order',
      ),
      body: ListView(
        children: [
          ResumeOrderShort(
            item: item,
          ),
          SizedBox(
            height: 40,
          ),
          Chip(
            backgroundColor: Colors.orange,
            label: Text(item.status.name,
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (item.status.id == 1)
                Text(
                  'Sedang mencari teknisi',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: primaryColor),
                ),
              if (item.status.id == 4)
                Text(
                  'Lakukan pembayaran',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: primaryColor),
                ),
              if (item.status.id == 5)
                Text(
                  'Teknisi sedang berangkat',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: primaryColor),
                ),
              if (item.status.id == 6)
                Text(
                  'Teknisi sedang bekerja',
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: primaryColor),
                ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          if (item.status.id == 4)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: InkWell(
                child: ButtonRounded(
                  label: 'Payment',
                  icon: Icons.payment,
                  color: primaryColor,
                ),
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => PaymentPage(
                            url: URL + 'pay/${item.jobId}',
                          )),
                ),
              ),
            ),
          if (item.status.id == 3 ||
              item.status.id == 5 ||
              item.status.id == 6 ||
              item.status.id == 7)
            Padding(
              padding: const EdgeInsets.all(10),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => ChatPage(
                              item: item,
                            )),
                  );
                },
                child: ButtonRounded(
                  label: 'Chat',
                  icon: Icons.chat,
                  color: primaryColor,
                ),
              ),
            ),
          // if (item.status.id == 7)
          Padding(
            padding: const EdgeInsets.all(10),
            child: InkWell(
              child: ButtonRounded(
                label: 'Confirm',
                icon: Icons.check,
                color: Colors.green,
              ),
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) => JobTaskPage(item: item)),
              ),
            ),
          ),
          if (item.status.id == 8)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: InkWell(
                child: ButtonRounded(
                  label: 'Complaint',
                  icon: Icons.report,
                  color: Colors.redAccent,
                ),
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => ComplaintPage(item: item)),
                ),
              ),
            ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
