import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/chat/chat_page.dart';
import 'package:mobile_customer/pages/complaint/complaint_page.dart';
import 'package:mobile_customer/pages/my_order/job_task_page.dart';
import 'package:mobile_customer/pages/my_order/normal_order_detail_page.dart';
import 'package:mobile_customer/pages/my_order/timeline_page.dart';
import 'package:mobile_customer/pages/payment/payment_page.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_rounded.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/listtile_custome.dart';
import 'package:mobile_customer/widget/pages/loading_page.dart';
import '../../utils/base_utils.dart';

import 'custom_order_detail_page.dart';
import 'order_model.dart';

class MyOrderDetailPage extends StatefulWidget {
  final OrderModel item;
  final int id;
  const MyOrderDetailPage({Key key, this.item, this.id}) : super(key: key);

  @override
  _MyOrderDetailPageState createState() => _MyOrderDetailPageState();
}

class _MyOrderDetailPageState extends State<MyOrderDetailPage> {
  ValueNotifier<bool> isDialOpen = ValueNotifier(false);

  OrderModel item;
  @override
  void initState() {
    item = widget.item;
    super.initState();
    if (widget.item == null) {
      getData();
    }
  }

  Future<void> getData() async {
    try {
      var res = await Webservice().get('joborder/read?match[id]=${widget.id}');
      OrderModel newData = OrderModel.fromJson(res['data'][0]);
      setState(() {
        item = newData;
      });
    } on Error catch (e, s) {
      print(e);
      print(s);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (item == null) {
      return Scaffold(body: LoadingPage());
    } else {
      print(item.id);
      if (item.service != null)
        return NormalOrderDetailPage(
          item: item,
        );
      else
        return CustomOrderDetailPage(
          item: item,
        );
    }
  }
}
