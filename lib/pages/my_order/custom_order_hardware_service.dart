import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/utils/constants.dart';

import '../../utils/base_utils.dart';
import 'order_model.dart';

class CustomOrderHardwareService extends StatelessWidget {
  final List<Project> project;
  const CustomOrderHardwareService({Key key, this.project}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 410,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: project.length,
          itemBuilder: (BuildContext context, int index) {
            Project subproject = project[index];
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xffc4c4c4)),
                borderRadius: BorderRadius.all(Radius.circular(
                        5.0) //                 <--- border radius here
                    ),
              ),
              width: 300,
              child: Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 15, right: 15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                'Sub Project',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(' - ${index + 1} ${subproject.projectName}'),
                            ],
                          ),
                          SizedBox(height: 10),
                          InfoSubProject(
                              icon: Icons.person_outline,
                              label: 'Jumlah Teknisi',
                              content: '${subproject.technicianAmount} Orang'),
                          InfoSubProject(
                              icon: Icons.location_on_outlined,
                              label: 'Address',
                              content: subproject.note),
                          InfoSubProject(
                              icon: Icons.date_range_outlined,
                              label: 'Start Date',
                              content:
                                  DateTime.parse(subproject.startAt).toDate()),
                          InfoSubProject(
                              icon: Icons.date_range_outlined,
                              label: 'End Date',
                              content:
                                  DateTime.parse(subproject.endAt).toDate()),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(Icons.label_outline),
                                    SizedBox(width: 10),
                                    Text(
                                      'Price',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                Text(int.parse(subproject.price).toCurrency(),
                                    style: TextStyle(color: primaryColor)),
                              ],
                            ),
                          ),
                          SizedBox(height: 15),
                          Row(
                            children: [
                              Text(
                                'Hardware List',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              // Text(' - ${checkoutController.totalItem} items'),
                            ],
                          ),
                          (subproject.orderproduct.isEmpty)
                              ? Column(
                                  children: [
                                    SizedBox(height: 20),
                                    Row(
                                      children: [
                                        Text('Tidak ada hardware',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontStyle: FontStyle.italic)),
                                      ],
                                    )
                                  ],
                                )
                              : Container(
                                  height: 120,
                                  child: ListView(
                                    children: [
                                      for (var order in subproject.orderproduct)
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 40,
                                                height: 40,
                                                decoration: BoxDecoration(
                                                  color: Color(0xFFF5F6F9),
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                ),
                                                child: ProductImage(
                                                  url: (order.product.thumbnail
                                                          .isNotEmpty)
                                                      ? order
                                                          .product
                                                          .thumbnail[0]
                                                          .thumbnailUrl
                                                      : null,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                              SizedBox(width: 10),
                                              Flexible(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      order.product.name,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          '${order.qty} x ${(order.product.price - order.product.discount).toCurrency()}',
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                        Text(
                                                          (order.qty *
                                                                  (order.product
                                                                          .price -
                                                                      order
                                                                          .product
                                                                          .discount))
                                                              .toCurrency(),
                                                          style: TextStyle(
                                                              color:
                                                                  primaryColor,
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Divider(
                        thickness: 1,
                        color: Color(0xffc4c4c4),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Total - Sub Project',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold)),
                            if (subproject.totalPrice != null)
                              Text(
                                subproject.totalPrice.toCurrency(),
                                style: TextStyle(
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }),
    );
  }
}

class InfoSubProject extends StatelessWidget {
  const InfoSubProject({
    Key key,
    this.content,
    this.label,
    this.icon,
  }) : super(key: key);

  final String content;
  final String label;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(icon),
              SizedBox(width: 10),
              Text(
                label,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Text(content),
        ],
      ),
    );
  }
}
