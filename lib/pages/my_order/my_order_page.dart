import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/my_order/my_order_history.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';

import 'my_order_ongoing.dart';

class MyOrderPage extends StatefulWidget {
  MyOrderPage({Key key}) : super(key: key);

  @override
  _MyOrderPageState createState() => _MyOrderPageState();
}

class _MyOrderPageState extends State<MyOrderPage> {
  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 2,
      appBar: BaseAppBar(showNotif: true, title: 'Job List'),
      body: DefaultTabController(
        length: 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              // color: Colors.amber,
              constraints: BoxConstraints.expand(height: 50),
              child: TabBar(
                  labelColor: primaryColor,
                  unselectedLabelColor: Colors.grey,
                  tabs: [
                    Tab(text: "Ongoing"),
                    Tab(text: "History"),
                  ]),
            ),
            Expanded(
              child: Container(
                child: TabBarView(children: [
                  MyOrderOngoing(),
                  MyOrderHistory(),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
