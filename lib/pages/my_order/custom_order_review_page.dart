import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/form/form_input.dart';

class CustomOrderReviewPage extends StatefulWidget {
  final OrderModel item;
  CustomOrderReviewPage({Key key, this.item}) : super(key: key);

  @override
  _CustomOrderReviewPageState createState() => _CustomOrderReviewPageState();
}

class _CustomOrderReviewPageState extends State<CustomOrderReviewPage> {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _textController = TextEditingController();
  Future<void> save() async {
    if (_key.currentState.validate()) {
      var data = {
        'status_note': _textController.text.trim(),
      };
      try {
        await Webservice().put('joborder/review/${widget.item.id}', data: data);
        Navigator.of(context).pop(true);
      } on Error catch (e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      appBar: BaseAppBar(
        title: 'Review',
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: [
          Form(
              key: _key,
              child: Column(
                children: [
                  FormInput(
                    controller: _textController,
                    maxLine: 10,
                  ),
                  ButtonSave(
                    label: 'Kirim',
                    onPressed: save,
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
