import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/my_order/custom_order_review_page.dart';
import 'package:mobile_customer/pages/product/product_detail_component/top_rounded_container.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/listtile_custome.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../../utils/base_utils.dart';

import 'custom_order_hardware_service.dart';
import 'normal_order_hardware_service.dart';
import 'order_model.dart';
import 'timeline_page.dart';

class CustomOrderDetailPage extends StatefulWidget {
  final OrderModel item;
  CustomOrderDetailPage({Key key, this.item}) : super(key: key);

  @override
  _CustomOrderDetailPageState createState() => _CustomOrderDetailPageState();
}

class _CustomOrderDetailPageState extends State<CustomOrderDetailPage> {
  ValueNotifier<bool> isDialOpen = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        }
        return true;
      },
      child: BaseScaffold(
        backgroudType: 1,
        floatingActionButton: buildSpeedDial(context),
        appBar: BaseAppBar(
          bgColor: Colors.grey[300],
          title: 'Order',
        ),
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                  color: Colors.grey[300],
                  padding: const EdgeInsets.only(top: 15),
                  child: TopRoundedContainer(
                    color: Colors.white,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 20),
                    children: [
                      Text(
                        widget.item.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 28,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'PIC Name',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      ListTileDetailRow(
                        icon: Icons.work_outline,
                        title: 'Project Name',
                        subtitle: widget.item.projectName ?? '',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.location_on_outlined,
                        title: 'Address',
                        subtitle: widget.item.address ?? '',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.phone_outlined,
                        title: 'Phone',
                        subtitle: widget.item.phone,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.mail_outline,
                        title: 'Email',
                        subtitle: widget.item.email,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.date_range_rounded,
                        title: 'Start Date',
                        subtitle: (widget.item.projectStartAt != null)
                            ? DateTime.parse(widget.item.projectStartAt)
                                .toDate()
                            : '',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTileDetailRow(
                        icon: Icons.date_range_rounded,
                        title: 'End Date',
                        subtitle: (widget.item.projectEndAt != null)
                            ? DateTime.parse(widget.item.projectEndAt).toDate()
                            : '',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      // if (widget.item.project.isEmpty) ...[
                      //   note('Jumlah Teknisi', Icons.people_alt_outlined),
                      //   SizedBox(
                      //     height: 10,
                      //   ),
                      //   Text(widget.item.technicianNote ?? ''),
                      //   Divider(
                      //     color: Colors.black,
                      //   ),
                      //   note('Alat yang dibutuhkan', Icons.construction),
                      //   SizedBox(
                      //     height: 10,
                      //   ),
                      //   Text(widget.item.hardwareNote ?? ''),
                      //   Divider(
                      //     color: Colors.black,
                      //   ),
                      // ],
                      note('Note', Icons.sticky_note_2_outlined),
                      SizedBox(
                        height: 10,
                      ),
                      Text(widget.item.note),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        color: Colors.black,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      if ((widget.item.grandTotal != null))
                        CustomOrderHardwareService(
                            project: widget.item.project),
                      SizedBox(
                        height: 20,
                      ),
                      if (widget.item.grandTotal != null) ...[
                        Divider(
                          color: Colors.black,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Grand Total',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              Text(
                                (widget.item.grandTotal != null)
                                    ? widget.item.grandTotal.toCurrency()
                                    : '',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: primaryColor),
                              ),
                            ],
                          ),
                        ),
                      ],
                      SizedBox(
                        height: 20,
                      ),
                      if (widget.item.status.id == 13)
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                  child: Text("Reject",
                                      style: TextStyle(fontSize: 14)),
                                  style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.white),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.red),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(18.0),
                                              side: BorderSide(
                                                  color: Colors.red)))),
                                  onPressed: () => reject()),
                              ElevatedButton(
                                  child: Text("Accept",
                                      style: TextStyle(fontSize: 14)),
                                  style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.white),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.green),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(18.0),
                                              side: BorderSide(
                                                  color: Colors.green)))),
                                  onPressed: () => accept()),
                            ]),
                      SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BaseCard(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: primaryColor,
                      size: 40,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Row note(String label, IconData icon) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: [
            Icon(icon),
            SizedBox(
              width: 10,
            ),
            Text(
              label,
              style: TextStyle(),
            ),
          ],
        ),
        Flexible(
            flex: 3,
            child: Container(
              margin: EdgeInsets.only(top: 5),
              child: Divider(
                color: Colors.black,
              ),
            )),
      ],
    );
  }

  SpeedDial buildSpeedDial(BuildContext context) {
    return SpeedDial(
      icon: Icons.expand_less,
      activeIcon: Icons.expand_more,
      buttonSize: 50.0,
      childrenButtonSize: 45,
      visible: true,
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      tooltip: 'Speed Dial',
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: primaryColor,
      foregroundColor: Colors.white,
      switchLabelPosition: false,
      openCloseDial: isDialOpen,
      elevation: 2,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
          child: Image.asset(
            'assets/icons/job_track.png',
            width: 20,
          ),
          backgroundColor: Colors.white,
          labelWidget: labelFab('Job Track'),
          onTap: () async {
            await Get.to(TimelinePage(item: widget.item));
          },
        ),
        // if (widget.item.status.id == 13)
        //   SpeedDialChild(
        //     child: Icon(
        //       Icons.edit,
        //       color: primaryColor,
        //     ),
        //     backgroundColor: Colors.white,
        //     labelWidget: labelFab('Review'),
        //     onTap: () async {
        //       await Get.to(CustomOrderReviewPage(item: widget.item));
        //     },
        //   ),
      ],
    );
  }

  Widget labelFab(String text) {
    return BaseCard(
      radius: 25,
      color: primaryColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Text(
          text,
          style: TextStyle(fontSize: 14, color: Colors.white),
        ),
      ),
    );
  }

  Future<void> accept() async {
    bool confirm = await Alertx.confirmDialog(title: 'Accept');
    if (confirm) {
      var data = {'approve': 1, 'status_note': 'approve'};
      try {
        await Webservice()
            .put('/joborder/approval/${widget.item.id}', data: data);
        Navigator.of(context).pop(true);
      } on Error catch (e) {
        print(e);
      }
    }
  }

  Future<void> reject() async {
    bool confirm = await Alertx.confirmDialog(title: 'Reject');
    if (confirm) {
      var data = {'approve': 2, 'status_note': 'reject'};
      try {
        await Webservice()
            .put('/joborder/approval/${widget.item.id}', data: data);
        Navigator.of(context).pop(true);
      } on Error catch (e) {
        print(e);
      }
    }
  }
}
