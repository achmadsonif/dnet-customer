import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/list_viewx.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'card_job.dart';
import 'my_order_detail_page.dart';
import 'order_model.dart';

class MyOrderHistory extends StatefulWidget {
  const MyOrderHistory({Key key}) : super(key: key);

  @override
  _MyOrderHistoryState createState() => _MyOrderHistoryState();
}

class _MyOrderHistoryState extends State<MyOrderHistory> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  String _searchController = '';
  List<Object> _dataList = [];
  int page = 1;

  @override
  void initState() {
    super.initState();
    // initializeDateFormatting();
  }

  Future<void> _loadMore() async {
    Map<String, dynamic> param = {
      'page': page,
      'limit': LENGTH_DATA,
      'sort': 'id',
      'order': 'DESC',
    };
    if (_searchController.isNotEmpty) {
      param['search'] = _searchController;
    }

    try {
      Map response = await Webservice().get('joborder/history', data: param);
      Iterable iter = response['data'];
      List<OrderModel> newDataList =
          iter.map((i) => OrderModel.fromJson(i)).toList();
      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> _refresh() async {
    page = 1;
    _dataList.clear();
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Widget _buildItemsForListView(BuildContext context, int i) {
    OrderModel item = _dataList[i];

    return InkWell(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
        child: CardJob(
          item: item,
        ),
      ),
      onTap: () async {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => MyOrderDetailPage(item: item)));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,

      // appBar: BaseAppBar(
      //   title: 'History',
      //   onSubmitSearch: (val) {
      //     print(val);
      //     _searchController = val;
      //     _refreshController.requestRefresh();
      //   },
      // ),
      body: ListViewx(
          controller: _refreshController,
          onRefresh: _refresh,
          onLoading: _loadMore,
          child: ListView.builder(
              itemCount: _dataList.length,
              itemBuilder: _buildItemsForListView)),
    );
  }
}
