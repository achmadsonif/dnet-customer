import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_chip.dart';
import '../../utils/base_utils.dart';

class CardJob extends StatelessWidget {
  final OrderModel item;
  const CardJob({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isCustom = (item.service == null);
    String dateLabel = DateTime.parse(item.createdAt).toDateTime();
    // if (isCustom) {
    //   dateLabel = (item.projectStartAt != null)
    //       ? DateTime.parse(item.projectStartAt).toDate()
    //       : '';
    // } else {
    //   dateLabel = (item.schedule.isNotEmpty != null)
    //       ? DateTime.parse(item.schedule[0].startAt).toDateTime()
    //       : '';
    // }

    return Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
            padding: EdgeInsets.all(15),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          dateLabel,
                          style: TextStyle(color: Colors.grey[700]),
                        ),
                        Row(
                          children: [
                            if (isCustom)
                              BaseChip(
                                backgroundColor: Color(0xffFFCC29),
                                text: 'Custom',
                              ),
                            BaseChip(
                              backgroundColor: Color(0xff81C04C),
                              text: item.status.name,
                            ),
                          ],
                        ),
                      ]),
                  Text((isCustom) ? item.projectName ?? '' : item.service.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16)),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            Icon(
                              Icons.location_pin,
                              color: primaryColor,
                            ),
                            SizedBox(width: 10),
                            Flexible(
                              child: Text(item.address,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12)),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                            (item.grandTotal != null)
                                ? item.grandTotal.toCurrency()
                                : '',
                            style: TextStyle(
                                color: primaryColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                ])));
  }
}
