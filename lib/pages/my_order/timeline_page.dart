import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:timeline_list2/timeline.dart';
import 'package:timeline_list2/timeline_model.dart';
import '../../utils/base_utils.dart';

class TimelinePage extends StatefulWidget {
  final OrderModel item;
  TimelinePage({Key key, this.item}) : super(key: key);

  @override
  _TimelinePageState createState() => _TimelinePageState();
}

class _TimelinePageState extends State<TimelinePage> {
  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
        appBar: BaseAppBar(
          title: 'Job Track',
        ),
        body: ListView(children: <Widget>[
          Container(
              padding: EdgeInsets.only(left: 20),
              width: double.infinity,
              child: Timeline(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                children: <TimelineModel>[
                  for (int i = 0; i < widget.item.track.length; i++)
                    TimelineModel(
                      Container(
                          constraints: BoxConstraints(
                            minHeight: 70, //minimum height
                          ),
                          padding:
                              EdgeInsets.only(top: 20, left: 20, bottom: 10),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.item.track[i].note,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                Text(
                                  DateTime.parse(widget.item.track[i].createdAt)
                                      .toDateTime(),
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                )
                              ])),
                      leading: CircleAvatar(
                        backgroundColor: primaryColor,
                        child: Text(
                          (i + 1).toString(),
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      // icon: Icon(Icons.check, color: Colors.white),
                      iconBackground: primaryColor,
                    ),
                ],
                position: TimelinePosition.Left,
                iconSize: 25,
                lineColor: primaryColor,
              ))
        ]));
  }
}
