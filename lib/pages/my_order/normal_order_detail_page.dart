import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/chat/chat_page.dart';
import 'package:mobile_customer/pages/complaint/complaint_page.dart';
import 'package:mobile_customer/pages/complaint/rating_page.dart';
import 'package:mobile_customer/pages/my_order/job_task_page.dart';
import 'package:mobile_customer/pages/my_order/normal_order_hardware_service.dart';
import 'package:mobile_customer/pages/my_order/timeline_page.dart';
import 'package:mobile_customer/pages/payment/payment_page.dart';
import 'package:mobile_customer/pages/product/product_detail_component/bottom_rounded_container.dart';
import 'package:mobile_customer/pages/product/product_detail_component/top_rounded_container.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_rounded.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/listtile_custome.dart';
import 'package:mobile_customer/widget/pages/loading_page.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../utils/base_utils.dart';
import 'package:dart_date/dart_date.dart';

import 'order_model.dart';

class NormalOrderDetailPage extends StatefulWidget {
  final OrderModel item;
  const NormalOrderDetailPage({Key key, this.item}) : super(key: key);

  @override
  _NormalOrderDetailPageState createState() => _NormalOrderDetailPageState();
}

class _NormalOrderDetailPageState extends State<NormalOrderDetailPage> {
  ValueNotifier<bool> isDialOpen = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    print('order id : ' + widget.item.id.toString());
    print('status : ' + widget.item.status.id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        }
        return true;
      },
      child: BaseScaffold(
        backgroudType: 1,
        floatingActionButton: buildSpeedDial(context),
        appBar: BaseAppBar(
          title: 'Order',
          bgColor: Colors.grey[300],
        ),
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                  color: Colors.grey[300],
                  padding: const EdgeInsets.only(top: 15),
                  child: TopRoundedContainer(
                    color: Colors.white,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 0, left: 20, right: 20),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        if (widget.item.technician != null)
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Color(0xffc4c4c4)),
                              borderRadius: BorderRadius.all(Radius.circular(
                                      5.0) //                 <--- border radius here
                                  ),
                            ),
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(bottom: 40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    CircleAvatar(
                                      radius: 20.0,
                                      backgroundImage: NetworkImage(
                                          widget.item.technician.photoUrl),
                                    ),
                                    SizedBox(width: 10),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(widget.item.technician.name,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14)),
                                            Icon(
                                              Icons.star,
                                              color: Colors.yellow,
                                              size: 15,
                                            ),
                                            Text(double.parse(widget
                                                    .item.technician.rating)
                                                .toStringAsFixed(1)),
                                          ],
                                        ),
                                        SizedBox(height: 2),
                                        Text('Teknisi')
                                      ],
                                    ),
                                  ],
                                ),
                                if (widget.item.status.id == 3 ||
                                    widget.item.status.id == 5 ||
                                    widget.item.status.id == 6 ||
                                    widget.item.status.id == 7)
                                  Row(
                                    children: [
                                      CircleAvatar(
                                        radius: 18.0,
                                        backgroundColor: Colors.grey,
                                        child: CircleAvatar(
                                          backgroundColor: Colors.white,
                                          radius: 16.0,
                                          child: InkWell(
                                            onTap: () {
                                              launch(
                                                  "tel://${widget.item.technician.phone}");
                                            },
                                            child: Icon(
                                              Icons.phone_outlined,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                      CircleAvatar(
                                        radius: 18.0,
                                        backgroundColor: Colors.grey,
                                        child: CircleAvatar(
                                          backgroundColor: Colors.white,
                                          radius: 16.0,
                                          child: InkWell(
                                            onTap: () {
                                              Get.to(ChatPage(
                                                item: widget.item,
                                              ));
                                            },
                                            child: Icon(
                                              Icons.chat_outlined,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                              ],
                            ),
                          ),
                        Text(
                          widget.item.name,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 28, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'PIC Name',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.normal),
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        ListTileDetailRow(
                          icon: Icons.location_on_outlined,
                          title: 'Address',
                          subtitle: widget.item.address,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        ListTileDetailRow(
                          icon: Icons.phone_outlined,
                          title: 'Phone',
                          subtitle:
                              '0' + int.parse(widget.item.phone).toString(),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (widget.item.schedule.length > 0) ...[
                          for (var i = 0; i < widget.item.schedule.length; i++)
                            // if (widget.item.schedule.length <= 3)
                            ListTileDetailRow(
                              icon: (i == 0) ? Icons.date_range_rounded : null,
                              title: (i == 0) ? 'Date' : '',
                              bold: widget.item.schedule[i].isChosen,
                              icon1: (widget.item.schedule[i].isChosen == true)
                                  ? CircleAvatar(
                                      radius: 6,
                                      child: Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 8,
                                      ),
                                      backgroundColor: primaryColor,
                                    )
                                  : null,
                              subtitle: DateTime.parse(
                                          widget.item.schedule[i].startAt)
                                      .toDate() +
                                  ', ' +
                                  DateTime.parse(
                                          widget.item.schedule[i].startAt)
                                      .toTime(),
                            ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                              flex: 1,
                              child: Row(
                                children: [
                                  Icon(Icons.sticky_note_2_outlined),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "Note",
                                    style: TextStyle(),
                                  ),
                                ],
                              ),
                            ),
                            Flexible(
                                flex: 3,
                                child: Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Divider(
                                    color: Colors.black,
                                  ),
                                )),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                            (widget.item.note == null || widget.item.note == '')
                                ? 'Tidak ada catatan'
                                : widget.item.note.toString()),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        NormalOrderHardwareService(item: widget.item),
                        SizedBox(
                          height: 80,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BaseCard(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: primaryColor,
                      size: 40,
                    ),
                  ),
                ),
              ],
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: bottomButton(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget bottomButton() {
    if (widget.item.status.id == 4)
      return Container(
        height: 75,
        padding: const EdgeInsets.fromLTRB(30, 10, 80, 15),
        child: ButtonSave(
          label: 'Bayar Tagihan',
          onPressed: () {
            Get.to(PaymentPage(
              url: URL +
                  'pay/${widget.item.jobId}_' +
                  Random().nextInt(100).toString(),
            ));
          },
        ),
      );
    else if (widget.item.status.id == 7)
      return Container(
        height: 75,
        padding: const EdgeInsets.fromLTRB(30, 10, 80, 15),
        child: ButtonSave(
          label: 'Cek Hasil Kerja Teknisi',
          onPressed: () => Get.to(JobTaskPage(item: widget.item)),
        ),
      );
    else if (widget.item.status.id == 12 &&
        DateTime.parse(widget.item.schedule[0].startAt).isAfter(DateTime.now()))
      return Container(
        height: 75,
        padding: const EdgeInsets.fromLTRB(30, 10, 80, 15),
        child: ButtonSave(
          label: 'Retry',
          onPressed: () => retryOrder(),
        ),
      );
    return SizedBox.shrink();
  }

  SpeedDial buildSpeedDial(BuildContext context) {
    return SpeedDial(
      icon: Icons.expand_less,
      activeIcon: Icons.expand_more,
      buttonSize: 50.0,
      childrenButtonSize: 45,
      visible: true,
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      tooltip: 'Speed Dial',
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: primaryColor,
      foregroundColor: Colors.white,
      switchLabelPosition: false,
      openCloseDial: isDialOpen,
      elevation: 2,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
          child: Image.asset(
            'assets/icons/job_track.png',
            width: 20,
          ),
          backgroundColor: Colors.white,
          labelWidget: labelFab('Job Track'),
          onTap: () async {
            await Get.to(TimelinePage(item: widget.item));
          },
        ),
        if (widget.item.allowCancel)
          SpeedDialChild(
            child: Icon(
              Icons.cancel_outlined,
              color: primaryColor,
            ),
            backgroundColor: Colors.white,
            labelWidget: labelFab('Cancel'),
            onTap: cancelOrder,
          ),
        if (widget.item.status.id == 8 && widget.item.technicianRating == null)
          SpeedDialChild(
            child: Image(
              width: 15,
              image: AssetImage('assets/icons/Set Rating Untuk Teknisi.png'),
            ),
            backgroundColor: Colors.white,
            labelWidget: labelFab('Rating'),
            onTap: () async {
              await Get.to(RatingPage(item: widget.item));
            },
          ),
        if (widget.item.status.id == 8 && widget.item.payment?.payoutAt == null)
          SpeedDialChild(
            child: Image.asset(
              'assets/icons/complain.png',
              width: 20,
            ),
            backgroundColor: Colors.white,
            labelWidget: labelFab('Complaint'),
            onTap: () async {
              await Get.to(ComplaintPage(item: widget.item));
            },
          ),
      ],
    );
  }

  Widget labelFab(String text) {
    return BaseCard(
      radius: 25,
      color: primaryColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Text(
          text,
          style: TextStyle(fontSize: 14, color: Colors.white),
        ),
      ),
    );
  }

  Future<void> cancelOrder() async {
    var data = {
      'status_note': 'Batal',
    };
    try {
      bool confirm = await Alertx.confirmDialog2(
          desc: 'Membatalkan Job \n${widget.item.service.name}');
      if (confirm) {
        await Webservice().put('joborder/cancel/${widget.item.id}', data: data);
        Navigator.of(context).pop(true);
      }
    } on Error catch (e, s) {
      print(e);
      print(s);
    }
  }

  Future<void> retryOrder() async {
    try {
      // bool confirm = await Alertx.confirmDialog2(
      //     desc: 'Membatalkan Job \n${widget.item.service.name}');
      // if (confirm) {
      await Webservice().get('joborder/retry/${widget.item.id}');
      Navigator.of(context).pop(true);
      // }
    } on Error catch (e, s) {
      print(e);
      print(s);
    }
  }
}
