class OrderModel {
  int id;
  String jobId;
  String name;
  String email;
  String address;
  String phone;
  String location;
  String city;
  String province;
  String latitude;
  String longitude;
  String note;
  String technicianNote;
  String hardwareNote;
  Status status;
  String customerRating;
  String customerFeedback;
  String technicianRating;
  String technicianFeedback;
  int customerId;
  int technicianId;
  int serviceId;
  String projectStartAt;
  String projectEndAt;
  String createdAt;
  String updatedAt;
  String projectName;
  int grandTotal;
  Service service;
  Technician technician;
  bool allowCancel;

  List<Schedule> schedule;
  List<Track> track;
  List<Evidence> evidence;
  Payment payment;
  List<OrderProduct> orderproduct;
  List<Project> project;

  OrderModel({
    this.id,
    this.jobId,
    this.name,
    this.email,
    this.address,
    this.phone,
    this.location,
    this.city,
    this.province,
    this.latitude,
    this.longitude,
    this.note,
    this.status,
    this.customerRating,
    this.customerFeedback,
    this.technicianRating,
    this.technicianFeedback,
    this.customerId,
    this.technicianId,
    this.serviceId,
    this.projectStartAt,
    this.projectEndAt,
    this.createdAt,
    this.updatedAt,
    this.service,
    this.technician,
    this.projectName,
    this.schedule,
    this.track,
    this.evidence,
    this.payment,
    this.orderproduct,
    this.project,
    this.allowCancel,
  });

  OrderModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    jobId = json['jobid'];
    name = json['name'];
    email = json['email'];
    address = json['address'];
    phone = json['phone'];
    location = json['location'];
    city = json['city'];
    province = json['province'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    note = json['note'];
    technicianNote = json['technician_note'];
    hardwareNote = json['hardware_note'];
    status =
        json['status'] != null ? new Status.fromJson(json['status']) : null;
    customerRating = json['customer_rating'];
    customerFeedback = json['customer_feedback'];
    technicianRating = json['technician_rating'];
    technicianFeedback = json['technician_feedback'];
    customerId = json['customer_id'];
    technicianId = json['technician_id'];
    serviceId = json['service_id'];
    projectStartAt = json['project_start_at'];
    projectEndAt = json['project_end_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    allowCancel = json['allow_cancel'];
    service =
        json['service'] != null ? new Service.fromJson(json['service']) : null;
    technician = json['technician'] != null
        ? new Technician.fromJson(json['technician'])
        : null;
    grandTotal = json['grand_total'];
    projectName = json['project_name'];

    if (json['schedule'] != null) {
      schedule = new List<Schedule>();
      json['schedule'].forEach((v) {
        schedule.add(new Schedule.fromJson(v));
      });
    }
    if (json['track'] != null) {
      track = new List<Track>();
      json['track'].forEach((v) {
        track.add(new Track.fromJson(v));
      });
    }
    if (json['evidence'] != null) {
      evidence = new List<Evidence>();
      json['evidence'].forEach((v) {
        evidence.add(new Evidence.fromJson(v));
      });
    }
    payment =
        json['payment'] != null ? new Payment.fromJson(json['payment']) : null;
    if (json['orderproduct'] != null) {
      orderproduct = new List<OrderProduct>();
      json['orderproduct'].forEach((v) {
        orderproduct.add(new OrderProduct.fromJson(v));
      });
    }
    if (json['project'] != null) {
      project = [];
      json['project'].forEach((v) {
        project.add(new Project.fromJson(v));
      });
    }
  }
}

class Status {
  int id;
  String name;
  Null type;

  Status({this.id, this.name, this.type});

  Status.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['type'] = this.type;
    return data;
  }
}

class Schedule {
  int id;
  String startAt;
  String endAt;
  bool isChosen;
  int jobOrderId;

  Schedule({this.id, this.startAt, this.endAt, this.isChosen, this.jobOrderId});

  Schedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    startAt = json['start_at'];
    endAt = json['end_at'];
    isChosen = json['is_chosen'];
    jobOrderId = json['job_order_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['start_at'] = this.startAt;
    data['end_at'] = this.endAt;
    data['is_chosen'] = this.isChosen;
    data['job_order_id'] = this.jobOrderId;
    return data;
  }
}

class Track {
  int id;
  int status;
  String note;
  int jobOrderId;
  String createdAt;
  String updatedAt;
  StatusTrack statusTrack;

  Track(
      {this.id,
      this.status,
      this.note,
      this.jobOrderId,
      this.createdAt,
      this.updatedAt,
      this.statusTrack});

  Track.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    status = json['status'];
    note = json['note'];
    jobOrderId = json['job_order_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    statusTrack = json['status_track'] != null
        ? new StatusTrack.fromJson(json['status_track'])
        : null;
  }
}

class StatusTrack {
  int id;
  String name;

  StatusTrack({this.id, this.name});

  StatusTrack.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }
}

class Payment {
  int id;
  String transactionId;
  String paymentType;
  String transactionTime;
  String transactionStatus;
  String fraudStatus;
  String grossAmount;
  String payoutReferenceNo;
  String payoutStatus;
  String payoutAmount;
  String payoutAt;
  int jobOrderId;
  String createdAt;
  String updatedAt;

  Payment(
      {this.id,
      this.transactionId,
      this.paymentType,
      this.transactionTime,
      this.transactionStatus,
      this.fraudStatus,
      this.grossAmount,
      this.payoutReferenceNo,
      this.payoutStatus,
      this.payoutAmount,
      this.payoutAt,
      this.jobOrderId,
      this.createdAt,
      this.updatedAt});

  Payment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    transactionId = json['transaction_id'];
    paymentType = json['payment_type'];
    transactionTime = json['transaction_time'];
    transactionStatus = json['transaction_status'];
    fraudStatus = json['fraud_status'];
    grossAmount = json['gross_amount'];
    payoutReferenceNo = json['payout_reference_no'];
    payoutStatus = json['payout_status'];
    payoutAmount = json['payout_amount'];
    payoutAt = json['payout_at'];
    jobOrderId = json['job_order_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['transaction_id'] = this.transactionId;
    data['payment_type'] = this.paymentType;
    data['transaction_time'] = this.transactionTime;
    data['transaction_status'] = this.transactionStatus;
    data['fraud_status'] = this.fraudStatus;
    data['gross_amount'] = this.grossAmount;
    data['payout_reference_no'] = this.payoutReferenceNo;
    data['payout_status'] = this.payoutStatus;
    data['payout_amount'] = this.payoutAmount;
    data['payout_at'] = this.payoutAt;
    data['job_order_id'] = this.jobOrderId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Service {
  int id;
  String name;
  String iconUrl;
  int price;
  bool isActive;
  int categoryId;
  Category category;
  List<Task> task;

  Service(
      {this.id,
      this.name,
      this.iconUrl,
      this.price,
      this.isActive,
      this.categoryId,
      this.category,
      this.task});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    iconUrl = json['iconUrl'];
    price = int.parse(json['price']);
    isActive = json['is_active'];
    categoryId = json['category_id'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    if (json['task'] != null) {
      task = new List<Task>();
      json['task'].forEach((v) {
        task.add(new Task.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['is_active'] = this.isActive;
    data['category_id'] = this.categoryId;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    return data;
  }
}

class Category {
  int id;
  String title;
  List<License> license;

  Category({this.id, this.title, this.license});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    if (json['license'] != null) {
      license = new List<License>();
      json['license'].forEach((v) {
        license.add(new License.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    if (this.license != null) {
      data['license'] = this.license.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class License {
  int id;
  String title;
  int categoryId;

  License({this.id, this.title, this.categoryId});

  License.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    categoryId = json['category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['category_id'] = this.categoryId;
    return data;
  }
}

class Task {
  int id;
  String description;
  int serviceId;
  Evidence evidence;

  Task({this.id, this.description, this.serviceId, this.evidence});

  Task.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    description = json['description'];
    serviceId = json['service_id'];
    evidence = json['evidence'] != null
        ? new Evidence.fromJson(json['evidence'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['description'] = this.description;
    data['service_id'] = this.serviceId;
    if (this.evidence != null) {
      data['evidence'] = this.evidence.toJson();
    }
    return data;
  }
}

class Evidence {
  String description;
  String createdAt;
  String updatedAt;
  String photoUrl;

  Evidence({this.description, this.createdAt, this.updatedAt, this.photoUrl});

  Evidence.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    photoUrl = json['photoUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['photoUrl'] = this.photoUrl;
    return data;
  }
}

class Technician {
  int id;
  String name;
  String gender;
  String email;
  String emailVerifiedAt;
  String phone;
  String phoneVerifiedAt;
  String address;
  String latitude;
  String longitude;
  String verifiedAt;
  String rejectedAt;
  String rejectedReason;
  String bankAccount;
  String bankName;
  String identityNumber;
  String rating;
  int status;
  bool isActive;
  int levelId;
  String createdAt;
  String updatedAt;
  String photoUrl;
  String bankUrl;
  String identityUrl;
  Level level;

  Technician(
      {this.id,
      this.name,
      this.gender,
      this.email,
      this.emailVerifiedAt,
      this.phone,
      this.phoneVerifiedAt,
      this.address,
      this.latitude,
      this.longitude,
      this.verifiedAt,
      this.rejectedAt,
      this.rejectedReason,
      this.bankAccount,
      this.bankName,
      this.identityNumber,
      this.rating,
      this.status,
      this.isActive,
      this.levelId,
      this.createdAt,
      this.updatedAt,
      this.photoUrl,
      this.bankUrl,
      this.identityUrl,
      this.level});

  Technician.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    gender = json['gender'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    phone = json['phone'];
    phoneVerifiedAt = json['phone_verified_at'];
    address = json['address'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    verifiedAt = json['verified_at'];
    rejectedAt = json['rejected_at'];
    rejectedReason = json['rejected_reason'];
    bankAccount = json['bank_account'];
    bankName = json['bank_name'];
    identityNumber = json['identity_number'];
    rating = json['rating'];
    status = json['status'];
    isActive = json['is_active'];
    levelId = json['level_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    photoUrl = json['photoUrl'];
    bankUrl = json['bankUrl'];
    identityUrl = json['identityUrl'];
    level = json['level'] != null ? new Level.fromJson(json['level']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['gender'] = this.gender;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['phone'] = this.phone;
    data['phone_verified_at'] = this.phoneVerifiedAt;
    data['address'] = this.address;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['verified_at'] = this.verifiedAt;
    data['rejected_at'] = this.rejectedAt;
    data['rejected_reason'] = this.rejectedReason;
    data['bank_account'] = this.bankAccount;
    data['bank_name'] = this.bankName;
    data['identity_number'] = this.identityNumber;
    data['rating'] = this.rating;
    data['status'] = this.status;
    data['is_active'] = this.isActive;
    data['level_id'] = this.levelId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['photoUrl'] = this.photoUrl;
    data['bankUrl'] = this.bankUrl;
    data['identityUrl'] = this.identityUrl;
    if (this.level != null) {
      data['level'] = this.level.toJson();
    }
    return data;
  }
}

class Level {
  int id;
  String name;
  String color;
  String max;
  String min;
  String type;

  Level({this.id, this.name, this.color, this.max, this.min, this.type});

  Level.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    color = json['color'];
    max = json['max'];
    min = json['min'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['color'] = this.color;
    data['max'] = this.max;
    data['min'] = this.min;
    data['type'] = this.type;
    return data;
  }
}

class OrderProduct {
  int id;
  int qty;
  String price;
  int jobOrderId;
  int productId;
  Product product;

  OrderProduct(
      {this.id,
      this.qty,
      this.price,
      this.jobOrderId,
      this.productId,
      this.product});

  OrderProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    qty = json['qty'];
    price = json['price'];
    jobOrderId = json['job_order_id'];
    productId = json['product_id'];
    product =
        json['product'] != null ? new Product.fromJson(json['product']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['qty'] = this.qty;
    data['price'] = this.price;
    data['job_order_id'] = this.jobOrderId;
    data['product_id'] = this.productId;
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    return data;
  }
}

class Product {
  int id;
  String name;
  int price;
  String unit;
  String brand;
  String type;
  String code;
  int discount;
  bool isActive;
  int categoryId;
  List<Thumbnail> thumbnail;

  Product(
      {this.id,
      this.name,
      this.price,
      this.unit,
      this.brand,
      this.type,
      this.code,
      this.discount,
      this.isActive,
      this.categoryId,
      this.thumbnail});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = int.parse(json['price']);
    unit = json['unit'];
    brand = json['brand'];
    type = json['type'];
    code = json['code'];
    discount = json['discount'];
    isActive = json['is_active'];
    categoryId = json['category_id'];
    if (json['thumbnail'] != null) {
      thumbnail = [];
      json['thumbnail'].forEach((v) {
        thumbnail.add(new Thumbnail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['unit'] = this.unit;
    data['brand'] = this.brand;
    data['type'] = this.type;
    data['code'] = this.code;
    data['discount'] = this.discount;
    data['is_active'] = this.isActive;
    data['category_id'] = this.categoryId;
    return data;
  }
}

class Thumbnail {
  int id;
  String name;
  int productId;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String thumbnailUrl;

  Thumbnail(
      {this.id,
      this.name,
      this.productId,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.thumbnailUrl});

  Thumbnail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    thumbnailUrl = json['thumbnailUrl'];
  }
}

class Project {
  int id;
  String projectName;
  String startAt;
  String endAt;
  String subject;
  String content;
  String price;
  String note;
  String hardwareNote;
  int technicianAmount;
  int jobOrderId;
  String createdAt;
  String updatedAt;
  int totalPrice;
  int hardwarePrice;
  List<OrderProduct> orderproduct;

  Project(
      {this.id,
      this.projectName,
      this.startAt,
      this.endAt,
      this.subject,
      this.content,
      this.price,
      this.note,
      this.hardwareNote,
      this.technicianAmount,
      this.jobOrderId,
      this.createdAt,
      this.updatedAt,
      this.totalPrice,
      this.hardwarePrice,
      this.orderproduct});

  Project.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    projectName = json['project_name'];
    startAt = json['start_at'];
    endAt = json['end_at'];
    subject = json['subject'];
    content = json['content'];
    price = json['price'];
    note = json['note'];
    hardwareNote = json['hardware_note'];
    technicianAmount = json['technician_amount'];
    jobOrderId = json['job_order_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    totalPrice = json['total_price'];
    hardwarePrice = json['hardware_price'];
    if (json['product'] != null) {
      orderproduct = [];
      json['product'].forEach((v) {
        orderproduct.add(new OrderProduct.fromJson(v));
      });
    }
  }
}
