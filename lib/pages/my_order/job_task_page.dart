import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/complaint/rating_page.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/alertx.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/photo_view_zoom.dart';
import 'package:timeline_list2/timeline.dart';
import 'package:timeline_list2/timeline_model.dart';
import '../../utils/base_utils.dart';

class JobTaskPage extends StatefulWidget {
  final OrderModel item;
  JobTaskPage({Key key, this.item}) : super(key: key);

  @override
  _JobTaskPageState createState() => _JobTaskPageState();
}

class _JobTaskPageState extends State<JobTaskPage> {
  @override
  Widget build(BuildContext context) {
    var sectionMenu = TextStyle(
        fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black);
    return BaseScaffold(
        appBar: BaseAppBar(
          title: 'Report Page',
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  children: [
                    Text(
                      'Tracking Task',
                      style: sectionMenu,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      child: BaseCard(
                        color: primaryColor,
                        child: Padding(
                          padding: const EdgeInsets.all(15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.item.address,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      ((widget.item.technician != null)
                                              ? '${widget.item.technician.name} | '
                                              : '') +
                                          'Technician',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Icon(
                                  Icons.bookmark_border_outlined,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Text(
                      'Task Avaiable',
                      style: sectionMenu,
                    ),
                    Container(
                      width: double.infinity,
                      child: Timeline(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        children: <TimelineModel>[
                          for (int i = 0;
                              i < widget.item.service.task.length;
                              i++)
                            TimelineModel(
                              Container(
                                padding: EdgeInsets.only(top: 30, left: 20),
                                // height: 120,
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.item.service.task[i].description,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      (widget.item.service.task[i].evidence
                                                  .createdAt ==
                                              null)
                                          ? Text(
                                              'In Process',
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black),
                                            )
                                          : Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  DateTime.parse(widget
                                                          .item
                                                          .service
                                                          .task[i]
                                                          .evidence
                                                          .createdAt)
                                                      .toDateTime(),
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.black),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                  height: 60,
                                                  child: InkWell(
                                                    onTap: () => Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (_) =>
                                                                PhotoViewZoom(
                                                                  url: widget
                                                                      .item
                                                                      .service
                                                                      .task[i]
                                                                      .evidence
                                                                      .photoUrl,
                                                                ))),
                                                    // child: ProductImage(
                                                    //   url: widget.item.service.task[i]
                                                    //       .evidence.photoUrl,
                                                    // ),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        // borderRadius: BorderRadius.circular(8.0),
                                                        color: Colors.white,
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.grey,
                                                            blurRadius: 2.0,
                                                            spreadRadius: 0.0,
                                                            offset: Offset(2.0,
                                                                2.0), // shadow direction: bottom right
                                                          )
                                                        ],
                                                      ),
                                                      height: 50.0,
                                                      width: double.infinity,
                                                      padding:
                                                          EdgeInsets.all(10.0),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Row(
                                                            children: [
                                                              Container(
                                                                height: 50,
                                                                width: 50,
                                                                child:
                                                                    ProductImage(
                                                                  url: widget
                                                                      .item
                                                                      .service
                                                                      .task[i]
                                                                      .evidence
                                                                      .photoUrl,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width: 10),
                                                              Text(
                                                                widget
                                                                    .item
                                                                    .service
                                                                    .task[i]
                                                                    .evidence
                                                                    .description,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 12,
                                                                  color: Colors
                                                                      .black,
                                                                  // fontWeight:FontWeight.bold,
                                                                ),
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                maxLines: 1,
                                                              ),
                                                            ],
                                                          ),
                                                          Icon(
                                                            Icons.check,
                                                            color: primaryColor,
                                                            size: 15,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            )
                                    ]),
                              ),
                              leading: CircleAvatar(
                                backgroundColor: primaryColor,
                                child: Text(
                                  (i + 1).toString(),
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              // icon: Icon(Icons.check, color: Colors.white),
                              iconBackground: Colors.red,
                            ),
                        ],
                        position: TimelinePosition.Left,
                        iconSize: 25,
                        lineColor: primaryColor,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ]),
            ),
            Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 200,
                  child: ButtonSave(
                    label: 'Done',
                    onPressed: () async {
                      var data = {
                        'status_note': 'Sudah dikonfirmasikan',
                      };
                      try {
                        await Webservice().put(
                            'joborder/confirm/${widget.item.id}',
                            data: data);
                        Get.offAll(HomePage());
                        Get.to(RatingPage(
                          item: widget.item,
                        ));
                        // Navigator.pushAndRemoveUntil(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (BuildContext context) => RatingPage(
                        //               item: widget.item,
                        //             )),
                        //     (Route<dynamic> route) => route is HomePage);
                        // Navigator.of(context).pop();
                        // Navigator.of(context).pushReplacement(
                        //   MaterialPageRoute(
                        //       builder: (context) => RatingPage(
                        //             item: widget.item,
                        //           )),
                        // );
                        // Alertx().success('Job selesai', HomePage());
                      } on Error catch (e) {
                        print(e);
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            )
          ],
        ));
  }
}
