import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/base_style.dart';

class ResumeOrderShort extends StatelessWidget {
  final OrderModel item;
  const ResumeOrderShort({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            child: SvgPicture.asset(
              'assets/images/logo.svg',
              width: 200,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            item.service.name,
            textAlign: TextAlign.center,
            style: h1,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            item.address,
            textAlign: TextAlign.center,
            style: h3,
          ),
        ],
      ),
    );
  }
}
