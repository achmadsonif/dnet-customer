import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/utils/constants.dart';
import '../../utils/base_utils.dart';
import 'order_model.dart';

class NormalOrderHardwareService extends StatelessWidget {
  final OrderModel item;
  const NormalOrderHardwareService({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffc4c4c4)),
        borderRadius: BorderRadius.all(
            Radius.circular(5.0) //                 <--- border radius here
            ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Service List',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    Text(' - 1 items'),
                  ],
                ),
                SizedBox(height: 10),
                if (item.service != null)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            // Container(
                            //   width: 40,
                            //   height: 40,
                            //   decoration: BoxDecoration(
                            //     color: Color(0xFFF5F6F9),
                            //     borderRadius: BorderRadius.circular(15),
                            //   ),
                            //   child: ProductImage(
                            //     url: item.service.iconUrl,
                            //     fit: BoxFit.fill,
                            //   ),
                            // ),
                            Icon(Icons.miscellaneous_services),
                            SizedBox(width: 10),
                            Flexible(
                              child: Text(
                                item.service.name,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                                // overflow: TextOverflow.ellipsis,
                                // maxLines: 1,
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          item.service.price.toCurrency(),
                          style: TextStyle(color: primaryColor),
                        ),
                      )
                    ],
                  ),
                SizedBox(height: 15),
                Row(
                  children: [
                    Text(
                      'Hardware List',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    // Text(' - ${checkoutController.totalItem} items'),
                  ],
                ),
                Column(
                  children: [
                    for (var order in item.orderproduct)
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                color: Color(0xFFF5F6F9),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: ProductImage(
                                url: (order.product.thumbnail.isNotEmpty)
                                    ? order.product.thumbnail[0].thumbnailUrl
                                    : null,
                                fit: BoxFit.fill,
                              ),
                            ),
                            SizedBox(width: 10),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    order.product.name,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        '${(order.product.price - order.product.discount).toCurrency()} (${order.qty} buah) ',
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal),
                                      ),
                                      Text(
                                        (order.qty *
                                                (order.product.price -
                                                    order.product.discount))
                                            .toCurrency(),
                                        style: TextStyle(
                                            color: primaryColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                  ],
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1,
            color: Color(0xffc4c4c4),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Grand Total', style: TextStyle(color: Colors.black)),
                if (item.grandTotal != null)
                  Text(
                    item.grandTotal.toCurrency(),
                    style: TextStyle(
                        color: primaryColor, fontWeight: FontWeight.bold),
                  ),
              ],
            ),
          ),
          SizedBox(height: 5),
        ],
      ),
    );
  }
}
