import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/utils/constants.dart';
import '../../utils/base_utils.dart';

import 'service_model.dart';

class ServiceCard extends StatelessWidget {
  final ServiceModel item;
  final Function onTap;
  const ServiceCard({
    Key key,
    @required this.item,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(10),
        ),
        child: ListTile(
          leading: Container(
            height: 40,
            width: 40,
            child: ProductImage(
              url: item.iconUrl,
            ),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                  child: Text(
                item.name,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
              Text(
                item.price.toCurrency(),
                style: TextStyle(color: primaryColor, fontSize: 12),
              )
            ],
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(item.category.title),
            ],
          ),
        ),
      ),
    );
  }
}
