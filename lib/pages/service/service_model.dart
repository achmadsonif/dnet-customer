class ServiceModel {
  int id;
  String name;
  int price;
  bool isActive;
  int categoryId;
  int orderCount;
  String iconUrl;
  Category category;

  ServiceModel(
      {this.id,
      this.name,
      this.price,
      this.isActive,
      this.categoryId,
      this.orderCount,
      this.iconUrl,
      this.category});

  ServiceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = int.parse(json['price']);
    isActive = json['is_active'];
    categoryId = json['category_id'];
    orderCount = json['order_count'];
    iconUrl = json['iconUrl'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['is_active'] = this.isActive;
    data['category_id'] = this.categoryId;
    data['order_count'] = this.orderCount;
    data['iconUrl'] = this.iconUrl;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    return data;
  }
}

class Category {
  int id;
  String title;
  List<License> license;

  Category({this.id, this.title, this.license});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    if (json['license'] != null) {
      license = new List<License>();
      json['license'].forEach((v) {
        license.add(new License.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    if (this.license != null) {
      data['license'] = this.license.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class License {
  int id;
  String title;
  int categoryId;

  License({this.id, this.title, this.categoryId});

  License.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    categoryId = json['category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['category_id'] = this.categoryId;
    return data;
  }
}
