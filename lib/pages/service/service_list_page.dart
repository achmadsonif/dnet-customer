// import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_list_page.dart';
import 'package:mobile_customer/pages/service/service_card.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../utils/constants.dart';
import '../../widget/base/base_app_bar.dart';
import '../../widget/list_viewx.dart';
import '../../utils/webservice.dart';
import 'service_model.dart';

class ServiceListPage extends StatefulWidget {
  final ServiceModel service;

  const ServiceListPage({Key key, this.service}) : super(key: key);
  @override
  _ServiceListState createState() => new _ServiceListState();
}

class _ServiceListState extends State<ServiceListPage> {
  final checkoutController = Get.put(CheckoutController());

  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  String _searchController = '';
  List<Object> _dataList = [];
  int page = 1;

  @override
  void initState() {
    super.initState();
    // initializeDateFormatting();
  }

  Future<void> _loadMore() async {
    Map<String, dynamic> param = {
      'page': page,
      'limit': LENGTH_DATA,
      'sort': 'id',
      'order': 'DESC',
    };
    if (_searchController.isNotEmpty) {
      param['filter[name]'] = _searchController;
    }

    try {
      Map response = await Webservice().get('jobservice/read', data: param);
      Iterable iter = response['data'];
      List<ServiceModel> newDataList =
          iter.map((i) => ServiceModel.fromJson(i)).toList();
      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> _refresh() async {
    page = 1;
    setState(() {
      _dataList.clear();
    });
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Widget _buildItemsForListView(BuildContext context, int i) {
    ServiceModel item = _dataList[i];

    return InkWell(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
        child: ServiceCard(item: item),
      ),
      onTap: () async {
        checkoutController.service.value = item;
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ProductListPage(service: item)));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 2,
      appBar: BaseAppBar(
        title: 'Service',
        onSubmitSearch: (val) {
          print(val);
          _searchController = val;
          _refreshController.requestRefresh();
        },
      ),
      body: ListViewx(
          controller: _refreshController,
          onRefresh: _refresh,
          onLoading: _loadMore,
          child: ListView.builder(
              itemCount: _dataList.length,
              itemBuilder: _buildItemsForListView)),
    );
  }
}
