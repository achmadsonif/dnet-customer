import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/login_page.dart';
import 'package:mobile_customer/pages/auth/prelogin_page.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/notif_utils.dart';
import 'package:mobile_customer/utils/size_config.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:flutter/material.dart';

import 'auth/global_controller.dart';
import 'auth/user_model.dart';
import 'auth/web_auth.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), () => isLoggedIn());
  }

  Future<void> isLoggedIn() async {
    await NotifUtils().init();
    NotifUtils().getToken();

    User user;
    user = await WebAuth().getUser();
    if (user != null) {
      final gstate = Get.put(GlobalController());
      await gstate.setUser(user);
      Get.offAll(HomePage());
      return;
    }
    Get.offAll(PreLoginPage());
    return;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        body: SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Image(
                width: 200, image: AssetImage('assets/images/Logo Color.png')),
            Image(image: AssetImage('assets/images/welcome screen.png'))
          ],
        ),
      ),
    ));
  }
}
