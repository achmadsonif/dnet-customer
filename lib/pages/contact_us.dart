import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/loading_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../utils/constants.dart';
import '../widget/base/base_app_bar.dart';
import '../widget/base/base_scaffold.dart';
import '../widget/card/base_card.dart';
import '../widget/listtile_custome.dart';
import 'product/product_detail_component/bottom_rounded_container.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  ContactModel data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<void> getData() async {
    try {
      var res = await Webservice().get('contact/read');
      ContactModel newData = ContactModel.fromJson(res['data'][0]);
      setState(() {
        data = newData;
      });
    } on Error catch (e, s) {
      print(e);
      print(s);
    }
  }

  void launchURL(url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width * 0.8);
    return BaseScaffold(
        backgroudType: 1,
        appBar: BaseAppBar(
          title: 'Contact Us',
          bgColor: lightPrimaryColor,
        ),
        body: Stack(children: <Widget>[
          // SquareContainer(
          //         height: MediaQuery.of(context).size.height * 0.4,
          //         color: lightPrimaryColor,
          //         child: SizedBox(height:10)
          // ),
          BottomRoundedContainer(
              color: lightPrimaryColor, child: SizedBox(height: 150)),
          Align(
              alignment: Alignment.bottomCenter,
              child: Image(
                  width: double.infinity,
                  fit: BoxFit.fill,
                  image: AssetImage('assets/images/contactus.png'))),
          Container(
            padding: EdgeInsets.only(top: 30, left: 30, right: 30),
            child: (data == null)
                ? Center(child: CircularProgressIndicator())
                : ListView(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      BaseCard(
                          color: Colors.white,
                          child: Container(
                              height: 470,
                              padding:
                                  EdgeInsets.only(top: 20, right: 20, left: 20),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Center(
                                        child: Text(
                                      "Get In Touch",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    )),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    ListTileDetailRow(
                                      icon: Icons.location_on,
                                      title: 'Address',
                                      subtitle: ' ',
                                      color: primaryColor,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Flexible(
                                      child: Text(
                                        data.address,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                    Divider(
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ListTileDetailRow(
                                      icon: Icons.phone,
                                      title: 'Phone',
                                      subtitle: ' ',
                                      color: primaryColor,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Flexible(
                                      child: Text(
                                        data.phone,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                    Divider(
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ListTileDetailRow(
                                      icon: Icons.email,
                                      title: 'E-mail',
                                      subtitle: ' ',
                                      color: primaryColor,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Flexible(
                                      child: Text(
                                        data.email,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                    Divider(
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    ListTileDetailRow(
                                      icon: Icons.language,
                                      title: 'Website',
                                      subtitle: ' ',
                                      color: primaryColor,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Flexible(
                                      child: Text(
                                        data.website,
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                    Divider(
                                      color: Colors.black,
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text("Follow us on",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold)),
                                        ]),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          child: FaIcon(
                                            FontAwesomeIcons.facebook,
                                            color: primaryColor,
                                          ),
                                          onTap: () => launchURL(data.facebook),
                                        ),
                                        SizedBox(width: 20),
                                        InkWell(
                                          child: FaIcon(
                                              FontAwesomeIcons.instagram,
                                              color: primaryColor),
                                          onTap: () =>
                                              launchURL(data.instagram),
                                        ),
                                        SizedBox(width: 20),
                                        InkWell(
                                          child: FaIcon(
                                              FontAwesomeIcons.twitter,
                                              color: primaryColor),
                                          onTap: () => launchURL(data.twitter),
                                        ),
                                      ],
                                    ),
                                  ]))),
                    ],
                  ),
          )
        ]));
  }
}

class ContactModel {
  int id;
  String title = "";
  String address = "";
  String phone = "";
  String email = "";
  String website = "";
  String facebook = "";
  String instagram = "";
  String twitter = "";
  bool isActive;

  ContactModel({
    this.id,
    this.title,
    this.address,
    this.phone,
    this.email,
    this.website,
    this.facebook,
    this.instagram,
    this.twitter,
    this.isActive,
  });

  ContactModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    address = json['address'];
    phone = json['phone'];
    email = json['email'];
    website = json['website'];
    facebook = json['facebook'];
    instagram = json['instagram'];
    twitter = json['twitter'];
    isActive = json['is_active'];
  }
}
