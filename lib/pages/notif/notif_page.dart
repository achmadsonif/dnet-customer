import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import 'package:mobile_customer/widget/list_viewx.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../utils/base_utils.dart';
import 'package:date_time_format/date_time_format.dart';

import 'notif_model.dart';

class NotifPage extends StatefulWidget {
  NotifPage({Key key}) : super(key: key);

  @override
  _NotifPageState createState() => _NotifPageState();
}

class _NotifPageState extends State<NotifPage> {
  final gstate = Get.put(GlobalController());
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  String _searchController = '';
  List<Object> _dataList = [];
  int page = 1;

  Future<void> _loadMore() async {
    await gstate.setNotif(0);
    await WebAuth().setNotif();

    Map<String, dynamic> param = {
      'page': page,
      'limit': LENGTH_DATA,
      'sort': 'id',
      'order': 'DESC',
    };
    if (_searchController.isNotEmpty) {
      param['filter[name]'] = _searchController;
    }

    try {
      Map response = await Webservice().get('notification/read', data: param);
      Iterable iter = response['data'];
      List<NotifModel> newDataList =
          iter.map((i) => NotifModel.fromJson(i)).toList();
      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> _refresh() async {
    page = 1;
    _dataList.clear();
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Widget _buildItemsForListView(BuildContext context, int i) {
    NotifModel item = _dataList[i];

    return Column(
      children: [
        ListTile(
          leading: Container(
            width: 45,
            height: 45,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.transparent,
              shape: BoxShape.circle,
              border: Border.all(color: Color(0xffc4c4c4)),
            ),
            child: (item.icon != null)
                ? Image.network(
                    item.icon.iconUrl,
                  )
                : SizedBox.shrink(),
          ),
          title: Text(
            item.title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            DateTime.parse(item.createdAt).relative(
              abbr: true,
              levelOfPrecision: 1,
              appendIfAfter: 'ago',
              formatAfter: Duration(days: 1),
              format: r'j M Y, H:i',
            ),
          ),
        ),
        Divider(
          color: Colors.grey[600],
          thickness: 0.5,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 2,
      appBar: BaseAppBar(
        title: 'Notifikasi',
        showNotif: false,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: ListViewx(
            controller: _refreshController,
            onRefresh: _refresh,
            onLoading: _loadMore,
            child: ListView.builder(
                itemCount: _dataList.length,
                itemBuilder: _buildItemsForListView)),
      ),
    );
  }
}
