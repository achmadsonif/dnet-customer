class NotifModel {
  int id;
  String type;
  String sender;
  String title;
  String body;
  String notifiableType;
  int notifiableId;
  String sent;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  Icon icon;

  NotifModel(
      {this.id,
      this.type,
      this.sender,
      this.title,
      this.body,
      this.notifiableType,
      this.notifiableId,
      this.sent,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.icon});

  NotifModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    sender = json['sender'];
    title = json['title'];
    body = json['body'];
    notifiableType = json['notifiable_type'];
    notifiableId = json['notifiable_id'];
    sent = json['sent'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    icon = json['icon'] != null ? new Icon.fromJson(json['icon']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['sender'] = this.sender;
    data['title'] = this.title;
    data['body'] = this.body;
    data['notifiable_type'] = this.notifiableType;
    data['notifiable_id'] = this.notifiableId;
    data['sent'] = this.sent;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.icon != null) {
      data['icon'] = this.icon.toJson();
    }
    return data;
  }
}

class Icon {
  int id;
  String icon;
  String type;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String iconUrl;

  Icon(
      {this.id,
      this.icon,
      this.type,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.iconUrl});

  Icon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    icon = json['icon'];
    type = json['type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    iconUrl = json['iconUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['icon'] = this.icon;
    data['type'] = this.type;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['iconUrl'] = this.iconUrl;
    return data;
  }
}
