import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/photo_view_zoom.dart';
import '../../utils/base_utils.dart';

class Message extends StatelessWidget {
  final String user, text, typeText;
  final bool mine;
  final timestamp;
  Message(
      {Key key, this.user, this.text, this.mine, this.timestamp, this.typeText})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: Column(
        crossAxisAlignment:
            mine ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Material(
            color: mine ? primaryColor : Colors.white,
            borderRadius: BorderRadius.circular(15.0),
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  (typeText == 'image')
                      ? InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) =>
                                      PhotoViewZoom(url: text)),
                            );
                          },
                          child: SizedBox(
                              width: 100,
                              height: 100,
                              child: Image.network(text)),
                        )
                      : Text(
                          text,
                          style: TextStyle(
                            color: mine ? Colors.white : Colors.black,
                            fontSize: 15,
                            fontFamily: 'Poppins',
                          ),
                        ),
                  SizedBox(
                    height: 5,
                  ),
                  if (timestamp != null)
                    Text(
                      DateTime.fromMillisecondsSinceEpoch(
                              timestamp.seconds * 1000)
                          .toDateTime(),
                      style: TextStyle(
                          color: mine ? Colors.white : Colors.black,
                          fontSize: 12,
                          fontFamily: 'Poppins',
                          fontStyle: FontStyle.italic),
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
