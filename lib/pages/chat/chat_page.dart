import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/pages/my_order/order_model.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/utils/webservice.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/pages/loading_page.dart';
import 'package:dio/dio.dart' as d;

import 'message.dart';

// import 'package:chat/models/auth.dart';

class ChatPage extends StatefulWidget {
  final OrderModel item;
  final int id;

  ChatPage({this.item, this.id});
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  OrderModel item;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();

  String userName = 'adi';
  @override
  void initState() {
    item = widget.item;
    super.initState();
    if (widget.item == null) {
      getData();
    }
  }

  Future<void> getData() async {
    try {
      var res = await Webservice().get('joborder/read?match[id]=${widget.id}');
      OrderModel newData = OrderModel.fromJson(res['data'][0]);
      setState(() {
        item = newData;
      });
    } on Error catch (e, s) {
      print(e);
      print(s);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (item == null)
      return Scaffold(
        body: LoadingPage(),
      );
    else
      return BaseScaffold(
        appBar: BaseAppBar(
          title: 'Chat Teknisi',
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                //margin: EdgeInsets.symmetric(horizontal: 5),
                child: StreamBuilder<QuerySnapshot>(
                    stream: _firestore
                        .collection("chatcustomertechs")
                        .orderBy("timestamp", descending: true)
                        .where('jobid', isEqualTo: item.id)
                        .where('technicianid', isEqualTo: item.technician.id)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.blue,
                          ),
                        );
                      List<QueryDocumentSnapshot> docs = snapshot.data.docs;

                      List<Widget> messages = docs
                          .map((doc) => Message(
                                text: doc.data()['text'],
                                typeText: doc.data()['typeText'],
                                timestamp: doc.data()['timestamp'],
                                mine: 'customer' == doc.data()['type'],
                              ))
                          .toList();
                      return ListView(
                        reverse: true,
                        controller: scrollController,
                        children: messages,
                      );
                    }),
              ),
            ),
            Container(
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: TextField(
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 15,
                        ),
                        onSubmitted: (value) => sendChat(),
                        controller: messageController,
                        cursorColor: cornFlowerBlue,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20)),
                          filled: true,
                          fillColor: antiFlashWhite,
                          hintText: "Type a message",
                          hintStyle:
                              TextStyle(fontFamily: 'Montserrat', fontSize: 12),
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    onPressed: () => sendImage(),
                  ),
                  IconButton(
                    icon: Icon(Icons.send),
                    onPressed: sendChat,
                  ),
                ],
              ),
            ),
          ],
        ),
      );
  }

  Future<void> sendImage() async {
    try {
      var image = await ImagePicker().getImage(
          source: ImageSource.camera, maxHeight: 450.0, maxWidth: 450.0);

      if (image != null) {
        Map<String, dynamic> data = {
          'media': await d.MultipartFile.fromFile(image.path)
        };
        // print(image.path);
        var res =
            await Webservice().post('chatmedia', data: data, isForm: true);
        print(res);
        String pathFile = res['data']['mediaUrl'];
        print(pathFile);
        sendChat(text: pathFile, typeText: 'image');
      }
      print(image.path);
    } catch (e) {
      print(e);
    }
  }

  Future<void> sendChat({String text, String typeText = 'text'}) async {
    String textContain = (text != null) ? text : messageController.text;

    if (textContain.length > 0) {
      var res = await Webservice().get('joborder/read?match[id]=${item.id}');
      OrderModel newData = OrderModel.fromJson(res['data'][0]);
      if (newData.technician.id != item.technician.id) {
        Get.offAll(HomePage(tabIndex: 0));
        return;
      }

      await _firestore.collection("chatcustomertechs").add({
        'jobid': item.id,
        'type': 'customer',
        'typeText': typeText,
        'technicianid': item.technician.id,
        'technicianname': item.technician.name,
        'text': textContain,
        'timestamp': FieldValue.serverTimestamp(),
      });

      if (typeText == 'image') textContain = 'Mengirim gambar';
      sendNotifFcm(textContain);

      messageController.clear();
      // scrollController.animateTo(scrollController.position.maxScrollExtent,
      // duration: Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  Future<void> sendNotifFcm(String text) async {
    var data = {
      'id': item.id.toString(),
      'reference': 'chatcustomer',
      'type': 'chat',
      'sender': 'fcm',
      'title': 'Pesan Baru Diterima',
      'body': text,
    };
    await Webservice().post(
        BASE_URL + 'system/notify/technician/${item.technician.id}',
        data: data,
        showDialog: false);
  }
}
