import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/product/product_image.dart';
import 'package:mobile_customer/pages/product/product_model.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:mobile_customer/widget/card/base_card.dart';
import '../../utils/base_utils.dart';

class ProductCard extends StatelessWidget {
  final ProductModel product;
  const ProductCard({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            SizedBox(
              width: 50,
              height: 50,
              child: AspectRatio(
                aspectRatio: 0.88,
                child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Color(0xFFF5F6F9),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: ProductImage(
                      url: (product.thumbnails.isNotEmpty)
                          ? product.thumbnails[0].thumbnailUrl
                          : null,
                      fit: BoxFit.fill,
                    )),
              ),
            ),
            SizedBox(width: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.name,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    if (product.discount > product.price) ...[
                      Text(product.discount.toCurrency(),
                          style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              fontSize: 14)),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                    Text(product.price.toCurrency(),
                        style: TextStyle(color: primaryColor, fontSize: 14)),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
