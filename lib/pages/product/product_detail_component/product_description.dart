import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile_customer/pages/product/product_model.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';
import '../../../utils/base_utils.dart';

class ProductDescription extends StatelessWidget {
  const ProductDescription({
    Key key,
    @required this.product,
    this.pressOnSeeMore,
  }) : super(key: key);

  final ProductModel product;
  final GestureTapCallback pressOnSeeMore;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            product.name,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 20,
            right: 64,
          ),
          child: Text(
            'Merek : ${product.brand} \n\nType : ${product.type} \n\nSatuan : ${product.unit}',
          ),
        ),
        // Padding(
        //   padding: EdgeInsets.symmetric(
        //     horizontal: 20,
        //     vertical: 10,
        //   ),
        //   child: GestureDetector(
        //     onTap: () {},
        //     child: Row(
        //       children: [
        //         Text(
        //           "See More Detail",
        //           style: TextStyle(
        //               fontWeight: FontWeight.w600, color: primaryColor),
        //         ),
        //         SizedBox(width: 5),
        //         Icon(
        //           Icons.arrow_forward_ios,
        //           size: 12,
        //           color: primaryColor,
        //         ),
        //       ],
        //     ),
        //   ),
        // ),
        SizedBox(
          height: 30,
        ),
        Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Harga',
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    if (product.discount > product.price) ...[
                      Text(product.discount.toCurrency(),
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              decoration: TextDecoration.lineThrough,
                              fontSize: 16)),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                    Text(product.price.toCurrency(),
                        style: TextStyle(
                            color: primaryColor,
                            fontWeight: FontWeight.w600,
                            fontSize: 18)),
                  ],
                ),
              ],
            ))
      ],
    );
  }
}
