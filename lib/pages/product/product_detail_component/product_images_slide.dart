import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/product/product_model.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/utils/constants.dart';

import '../../shop/Product.dart';
import '../product_image.dart';

class ProductImagesSlide extends StatefulWidget {
  const ProductImagesSlide({
    Key key,
    @required this.product,
  }) : super(key: key);

  final ProductModel product;

  @override
  _ProductImagesSlideState createState() => _ProductImagesSlideState();
}

class _ProductImagesSlideState extends State<ProductImagesSlide> {
  int selectedImage = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 200,
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: widget.product.id.toString(),
              child: ProductImage(
                  url: (widget.product.thumbnails.isNotEmpty)
                      ? widget.product.thumbnails[selectedImage].thumbnailUrl
                      : null),
            ),
          ),
        ),
        // SizedBox(height: 20),
        // Container(
        //   height: 50,
        //   child: ListView.builder(
        //       scrollDirection: Axis.horizontal,
        //       itemCount: widget.product.thumbnails.length,
        //       itemBuilder: (BuildContext context, int index) {
        //         return buildSmallProductPreview(index);
        //       }),
        // ),
        SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 15),
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ...List.generate(widget.product.thumbnails.length,
                  (index) => buildSmallProductPreview(index)),
            ],
          ),
        )
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 250),
        margin: EdgeInsets.only(right: 15),
        padding: EdgeInsets.all(8),
        height: 48,
        width: 48,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: primaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: ProductImage(
            url: (widget.product.thumbnails.isNotEmpty)
                ? widget.product.thumbnails[index].thumbnailUrl
                : null),
      ),
    );
  }
}
