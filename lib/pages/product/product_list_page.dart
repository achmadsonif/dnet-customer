// import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

import 'dart:ui';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_detail_page.dart';
import 'package:mobile_customer/pages/service/service_model.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/pages/shop/product_cart_page.dart';
import 'package:mobile_customer/pages/shop/shop_form_page.dart';
import 'package:mobile_customer/widget/base/base_scaffold.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/button/rounded_icon_btn.dart';
import 'package:mobile_customer/widget/card/base_outline_card.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../utils/base_utils.dart';

import '../../utils/constants.dart';
import '../../widget/base/base_app_bar.dart';
import '../../widget/list_viewx.dart';
import '../../utils/webservice.dart';
import 'product_card.dart';
import 'product_model.dart';

class ProductListPage extends StatefulWidget {
  final ServiceModel service;
  final ProductModel product;

  const ProductListPage({Key key, this.product, this.service})
      : super(key: key);
  @override
  _ProductListState createState() => new _ProductListState();
}

class _ProductListState extends State<ProductListPage> {
  final checkoutController = Get.put(CheckoutController());
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  String _searchController = '';
  List<Object> _dataList = [];
  int page = 1;

  @override
  void initState() {
    super.initState();
    // initializeDateFormatting();
  }

  Future<void> _loadMore() async {
    Map<String, dynamic> param = {
      'page': page,
      'limit': LENGTH_DATA,
      'sort': 'id',
      'order': 'DESC',
    };
    if (_searchController.isNotEmpty) {
      param['filter[name]'] = _searchController;
    }

    try {
      Map response = await Webservice().get('product/read', data: param);
      Iterable iter = response['data'];
      List<ProductModel> newDataList =
          iter.map((i) => ProductModel.fromJson(i)).toList();
      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> _refresh() async {
    page = 1;
    setState(() {
      _dataList.clear();
    });
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Widget _buildItemsForListView(BuildContext context, int i) {
    ProductModel item = _dataList[i];

    return InkWell(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
        child: ProductCard(product: item),
      ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (context) => ProductDetailPage(product: item)),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      backgroudType: 2,
      appBar: BaseAppBar(
        title: 'Pilih Product',
        onSubmitSearch: (val) {
          print(val);
          _searchController = val;
          _refreshController.requestRefresh();
        },
      ),
      body: Column(
        children: [
          Flexible(
            child: ListViewx(
                controller: _refreshController,
                onRefresh: _refresh,
                onLoading: _loadMore,
                child: ListView.builder(
                    itemCount: _dataList.length,
                    // itemExtent: 75.0,
                    itemBuilder: _buildItemsForListView)),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 55,
                    width: 55,
                    child: InkWell(
                      child: Stack(
                        children: [
                          BaseOutlineCard(
                              child: Badge(
                            badgeContent: Obx(() =>
                                Text(checkoutController.totalItem.toString(),
                                    style: TextStyle(
                                      color: Colors.white,
                                    ))),
                            child: Icon(Icons.shopping_cart_outlined),
                          )),
                        ],
                      ),
                      onTap: () => Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProductCartPage())),
                    ),
                  ),
                  Container(
                    width: 230,
                    child: ButtonSave(
                      label: 'Continue',
                      onPressed: () => Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => ShopFormPage()),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
