class ProductModel {
  int id;
  String name;
  int price;
  String unit;
  String brand;
  String type;
  String code;
  int discount;
  bool isActive;
  int categoryId;
  Category category;
  List<Thumbnails> thumbnails;

  ProductModel(
      {this.id,
      this.name,
      this.price,
      this.unit,
      this.brand,
      this.type,
      this.code,
      this.discount,
      this.isActive,
      this.categoryId,
      this.category,
      this.thumbnails});

  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = int.parse(json['price']) - (json['discount'] ?? 0);
    unit = json['unit'];
    brand = json['brand'];
    type = json['type'];
    code = json['code'];
    discount = int.parse(json['price']);
    isActive = json['is_active'];
    categoryId = json['category_id'];
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    if (json['thumbnails'] != null) {
      thumbnails = new List<Thumbnails>();
      json['thumbnails'].forEach((v) {
        thumbnails.add(new Thumbnails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['unit'] = this.unit;
    data['brand'] = this.brand;
    data['type'] = this.type;
    data['code'] = this.code;
    data['discount'] = this.discount;
    data['is_active'] = this.isActive;
    data['category_id'] = this.categoryId;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    if (this.thumbnails != null) {
      data['thumbnails'] = this.thumbnails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Category {
  int id;
  String name;

  Category({this.id, this.name});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}

class Thumbnails {
  int id;
  String name;
  String thumbnailUrl;
  int productId;

  Thumbnails({this.id, this.name, this.thumbnailUrl, this.productId});

  Thumbnails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    thumbnailUrl = json['thumbnailUrl'];
    productId = json['product_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['thumbnailUrl'] = this.thumbnailUrl;
    data['product_id'] = this.productId;
    return data;
  }
}
