import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/product/product_model.dart';
import 'package:mobile_customer/pages/shop/checkout_controller.dart';
import 'package:mobile_customer/utils/base_style.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:mobile_customer/widget/button/button_save.dart';
import 'package:mobile_customer/widget/button/rounded_icon_btn.dart';

import 'product_detail_component/product_description.dart';
import 'product_detail_component/product_images_slide.dart';
import 'product_detail_component/top_rounded_container.dart';

class ProductDetailPage extends StatefulWidget {
  final ProductModel product;
  ProductDetailPage({Key key, this.product}) : super(key: key);

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  final checkoutController = Get.put(CheckoutController());

  int count = 1;
  @override
  void initState() {
    super.initState();
    CheckoutModel itemOnBucket = checkoutController.items.value.firstWhere(
        (element) => element.product.id == widget.product.id,
        orElse: () => null);
    if (itemOnBucket != null) {
      count = itemOnBucket.count;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: BaseAppBar(
        title: '',
        bgColor: Color(0xFFF5F6F9),
      ),
      body: ListView(
        children: [
          ProductImagesSlide(product: widget.product),
          TopRoundedContainer(
            color: Colors.white,
            child: Column(
              children: [
                ProductDescription(
                  product: widget.product,
                  pressOnSeeMore: () {},
                ),
                TopRoundedContainer(
                  color: Color(0xFFF6F7F9),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          RoundedIconBtn(
                            icon: Icons.remove,
                            colorIcon: Colors.red,
                            press: () {
                              if (count > 1)
                                setState(() {
                                  count--;
                                });
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Text('$count', style: h2),
                          ),
                          RoundedIconBtn(
                            icon: Icons.add,
                            colorIcon: Colors.green,
                            showShadow: true,
                            press: () {
                              setState(() {
                                count++;
                              });
                            },
                          ),
                        ],
                      ),
                      TopRoundedContainer(
                        color: Colors.white,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: 30,
                            right: 30,
                            bottom: 40,
                            top: 15,
                          ),
                          child: ButtonSave(
                            label: "Add To Cart",
                            onPressed: () {
                              checkoutController.addWithNumb(
                                  widget.product, count);
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
