import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProductImage extends StatelessWidget {
  final String url;
  final BoxFit fit;
  const ProductImage({Key key, this.url = '', this.fit = BoxFit.cover})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url != null && url.isNotEmpty) {
      return CachedNetworkImage(
        imageUrl: url,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(image: imageProvider, fit: fit),
          ),
        ),
        placeholder: (context, url) =>
            Image.asset('assets/images/default_image.png'),
        errorWidget: (context, url, error) =>
            Image.asset('assets/images/default_image.png'),
      );
    }
    return Image.asset('assets/images/default_image.png');
  }
}
