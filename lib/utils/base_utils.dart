import 'package:dart_date/dart_date.dart';
import 'package:intl/intl.dart';

extension CurrencyExt on num {
  String toCurrency() {
    final formatter = new NumberFormat("#,###", 'IDR');
    return 'Rp. ' + formatter.format(this);
  }
}

String localRegion = 'en_US';

extension DatetimeExt on DateTime {
  DateTime checkUtc() {
    if (this.isUtc) return this.toLocal();
    return this;
  }

  bool isSameDate(DateTime other) {
    return this.year == other.year &&
        this.month == other.month &&
        this.day == other.day;
  }

  String toDate() {
    DateTime res = checkUtc();
    return res.format('dd MMMM y');
  }

  String toDateTime() {
    DateTime res = checkUtc();
    return res.format('dd MMM y, HH:mm', localRegion);
  }

  String toDateShortMonth() {
    DateTime res = checkUtc();
    return res.format('dd MMM y', localRegion);
  }

  String toDateShort() {
    DateTime res = checkUtc();
    return res.format('dd MMM y', localRegion);
  }

  String toTime() {
    DateTime res = checkUtc();
    return res.format('HH:mm', localRegion);
  }

  String toDate2() {
    DateTime res = checkUtc();
    return res.format('y-MM-dd', localRegion);
  }

  String toDateNotif() {
    DateTime res = checkUtc();
    return res.format('y-MM-dd HH:mm', localRegion);
  }
}
