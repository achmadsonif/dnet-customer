// IP Laptop at home
import 'package:flutter/material.dart';

import 'size_config.dart';

const String BASE_URL = 'https://testgotech.dwp.io/api/';
const String URL = BASE_URL+'customer/';
const String API_KEY_GOOGLE = 'AIzaSyDuJ1FeYefqYbnCbf6Ab2zKIIPukC97JFo';

const String appLogo = 'assets/icon/icon.png';
const int LENGTH_DATA = 20;

const Map dd_gender = {"L": "Laki-laki", "P": "Perempuan"};

const String ROLE_ADMIN = 'admin';
const String ROLE_USER = 'user';
const String ROLE_MEMBER = 'member';

const int COLOR_BG_LOGIN = 0xfff5f6f2;
const int COLOR_PRIMARY = 0xffbda378;
const int COLOR_PURPLE = 0xff8c6d96;
const int COLOR_GREEN = 0xff779c4a;
const int COLOR_GREY_MENU = 0xff95a3ae;
const int COLOR_PINK_LIGHT = 0xffe7d5d5;
const int COLOR_GREY_LIGHT = 0xffd3d3d3;

// const Color primaryColor = Color(0xFFFF7643);
// const Color primaryColor = Color(0xFFF2484C);
const Color primaryColor = Color(0xFF01ADEC);
const Color lightPrimaryColor = Color(0x120197CD);
const Color secondaryColor = Color(0xFF979797);
const Color disabledColor = Colors.grey;
const Color kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your name";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kAddressNullError = "Please Enter your address";

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

extension HexColor on Color {
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

Color royalBlue = Color(0xff6f6ed6),
    cornFlowerBlue = Color(0xff7d7cda),
    buff = Color(0xfff9dd7a),
    antiFlashWhite = Color(0xffebeef3);
