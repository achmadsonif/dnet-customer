// import 'package:geocoding/geocoding.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
// import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trust_fall/trust_fall.dart';

import 'constants.dart';

class GpsUtils {
  static const String LATITUDE = 'latitude';
  static const String LONGITUDE = 'longitude';
  static const String LOCATION_NAME = 'location_name';

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<GpsLocation> getLastLocation() async {
    final SharedPreferences prefs = await _prefs;

    double _latitude = prefs.getDouble(LATITUDE) ?? -6.200000;
    double _longitude = prefs.getDouble(LONGITUDE) ?? 106.816666;
    String _locationName = prefs.getString(LOCATION_NAME) ?? 'Jakarta';

    return new GpsLocation(
        latitude: _latitude, longitude: _longitude, name: _locationName);
  }

  Future<GpsLocation> getLocation() async {
    double _latitude, _longitude;
    String _locationName;
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    Position position = await Geolocator.getCurrentPosition();
    _latitude = position.latitude;
    _longitude = position.longitude;

    _locationName = await getLocationName(_latitude, _longitude);

    final SharedPreferences prefs = await _prefs;
    prefs.setDouble(LATITUDE, _latitude);
    prefs.setDouble(LONGITUDE, _longitude);
    prefs.setString(LOCATION_NAME, _locationName);

    return new GpsLocation(
        latitude: _latitude, longitude: _longitude, name: _locationName);
  }

  Future<String> getLocationName(double latitude, double longitude) async {
    var location = await Geocoder.google(API_KEY_GOOGLE)
        .findAddressesFromCoordinates(new Coordinates(latitude, longitude));
    return location.first.addressLine;
  }

  Future<bool> cekMockLocation() async {
    try {
      // bool isJailBroken = await TrustFall.isJailBroken;
      // bool isRealDevice = await TrustFall.isRealDevice;
      bool canMockLocation = await TrustFall.canMockLocation;
      if (canMockLocation) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      print(error);
      return false;
    }
  }
}

class GpsLocation {
  final double latitude;
  final double longitude;
  final String name;

  GpsLocation({
    this.latitude,
    this.longitude,
    this.name = '',
  });
}
