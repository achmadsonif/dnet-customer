import 'package:dio/dio.dart';
import 'package:get/get.dart' as g;
import 'package:mobile_customer/pages/auth/web_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../utils/constants.dart';
import '../widget/alertx.dart';

class Webservice {
  Response response;
  Dio dio = new Dio();
  String errorMessage = 'Network error';

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? '';
    // print('token web :' + token);
    return token;
  }

  Future<void> setToken(String t) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', t);
    print('token web new:' + t);
  }

  Future<void> setBase() async {
    dio.options.baseUrl = URL;
    String token = await getToken();
    dio.options.headers['Authorization'] = 'Bearer $token';
    dio.options.responseType = ResponseType.json;

    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        print('REQUEST[${options.method}] => PATH: ${options.path}');
        print('PARAM : ${options.data}');
        return handler.next(options); //continue
      },
      onResponse: (response, handler) {
        if (response.headers["jwt"] != null)
          setToken(response.headers["jwt"][0]);
        return handler.next(response); // continue
      },
      onError: (DioError e, handler) {
        try {
          String errorMessage = e.response.data['message'].toString();
          if (errorMessage == "Akses terbatas, harap login terlebih dahulu" ||
              (errorMessage ==
                      "Maaf, akun anda telah terkunci. Harap menghubungi email info@gotech.dwp.io" &&
                  e.response.statusCode == 401)) {
            if (token.isNotEmpty) {
              WebAuth.logOut(auto: true);
              return null;
            }
          }
        } catch (e) {}
        return handler.next(e); //continue
      },
    ));
    // dio.interceptors.add(alice.getDioInterceptor());
  }

  Future<dynamic> get(String url, {Map<String, dynamic> data}) async {
    await setBase();

    try {
      response = await dio.get(url, queryParameters: data);
    } on DioError catch (e) {
      e.printError();
      print(e.response);
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null)
        errorMessage = e.response.data['message'].toString();

      throw errorMessage;
    }
    return response.data;
  }

  Future<dynamic> post(
    String url, {
    Map<String, dynamic> data,
    bool showDialog = true,
    bool isForm = false,
  }) async {
    await setBase();
    // FormData formData = new FormData.fromMap(data);
    if (showDialog) {
      Alertx.loading();
    }

    try {
      response =
          await dio.post(url, data: (isForm) ? FormData.fromMap(data) : data);
    } on DioError catch (e) {
      e.printError();
      print(e.response);
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if (e.response != null)
        errorMessage = e.response.data['message'].toString();
      // errorMessage = e.response.data['error'].toString();

      if (showDialog) Alertx.warning(errorMessage);

      throw errorMessage;
    }
    if (g.Get.isDialogOpen && showDialog) g.Get.back();
    return response.data;
  }

  Future<Map> put(String url,
      {Map<String, dynamic> data,
      bool showDialog = true,
      bool isForm = false}) async {
    await setBase();
    // FormData formData = new FormData.fromMap(data);
    if (showDialog) {
      Alertx.loading();
    }

    try {
      response =
          await dio.put(url, data: (isForm) ? FormData.fromMap(data) : data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if (e.response != null)
        errorMessage = e.response.data['message'].toString();

      if (showDialog) Alertx.warning(errorMessage);

      throw errorMessage;
    }
    if (g.Get.isDialogOpen) g.Get.back();

    return response.data;
  }

  Future<dynamic> delete(String url,
      {Map<String, dynamic> data, bool showDialog = true}) async {
    await setBase();

    if (showDialog) {
      Alertx.loading();
    }

    try {
      response = await dio.delete(url, queryParameters: data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null)
        errorMessage = e.response.data['message'].toString();
      if (showDialog) Alertx.warning(errorMessage);
      throw errorMessage;
    }
    if (g.Get.isDialogOpen) g.Get.back();
    return response.data;
  }
}
