import 'dart:convert';

import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/chat/chat_page.dart';
import 'package:mobile_customer/pages/my_order/my_order_detail_page.dart';

final gstate = Get.put(GlobalController());

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future onDidReceiveLocalNotification(
    int id, String title, String body, String payload) async {
  print('onselectenotifios');
  if (payload != null) {
    print('notification payload: $payload');
    var json = jsonDecode(payload);
    redirectPage(json);
  }
}

Future selectNotification(String payload) async {
  print('onselectenotif');
  if (payload != null) {
    print('notification payload: $payload');
    var json = jsonDecode(payload);
    redirectPage(json);
  }
  // await Navigator.push(
  //   context,
  //   MaterialPageRoute<void>(builder: (context) => SecondScreen(payload)),
  // );
}

Future<void> showNotification(RemoteMessage message) async {
  if (Platform.isIOS) return;
  // Parsing data notifikasi
  final dynamic data = message.data;
  print(data);

  // Parsing ID Notifikasi
  final int idNotification = 1;

  // Andoir channel detail.
  const AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails(
          'timteknis', 'Notification', 'All Notification is Here',
          importance: Importance.max,
          priority: Priority.high,
          styleInformation: BigTextStyleInformation(''),
          ticker: 'ticker');
  const NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);

  // show Notif
  await flutterLocalNotificationsPlugin.show(
      idNotification,
      message.notification.title,
      message.notification.body,
      platformChannelSpecifics,
      payload: jsonEncode(data));
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  // await Firebase.initializeApp();
  gstate.setNotif(1);
  print('Handling a background message ${message.messageId}');
}

class NotifUtils {
  FirebaseMessaging _messaging = FirebaseMessaging.instance;

  void init() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('launcher_icon');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsMacOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    // Set the background messaging handler early on, as a named top-level function
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    // Additional initialization of the State
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
      }
      showNotification(message);
      gstate.setNotif(1);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      redirectPage(message.data);
    });
  }

  Future<String> getToken() async {
    String _token = "";
    await _messaging.getToken().then((token) {
      _token = token;
      print('Token: $token');
    }).catchError((e) {
      print(e);
    });
    return _token;
  }

  Future<void> initMsg() async {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((initialMessage) => Future.delayed(Duration(seconds: 4), () {
              if (initialMessage != null) redirectPage(initialMessage.data);
            }));
  }
}

void redirectPage(Map json) {
  print('Isi Notification');
  print(json);
  if (json['reference'] == 'orderdetail') {
    Get.to(MyOrderDetailPage(
      id: int.parse(json['jobId']),
    ));
  } else if (json['reference'] == 'chatcustomer') {
    Get.to(ChatPage(
      id: int.parse(json['id']),
    ));
  }
}
