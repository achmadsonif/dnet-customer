import 'package:flutter/material.dart';
import 'constants.dart';

TextStyle h1 = TextStyle(fontWeight: FontWeight.w400, fontSize: 34, color: Colors.black);
TextStyle h2 = TextStyle(fontWeight: FontWeight.bold, fontSize: 20);
TextStyle h3 = TextStyle(color: Colors.grey);

var styleListTitle = TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: primaryColor);
var styleListDesc = TextStyle(color: Colors.grey);
var styleListPrice = TextStyle(fontWeight: FontWeight.bold);


