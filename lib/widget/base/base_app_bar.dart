import 'package:flutter/material.dart';
import 'package:mobile_customer/pages/auth/global_controller.dart';
import 'package:mobile_customer/pages/notif/notif_page.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:get/get.dart';

class BaseAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final bool showBackButton;
  final Widget trailing;
  final Widget leading;
  final Widget titleWidget;
  final ValueChanged<String> onSubmitSearch;
  final List<Widget> actions;
  final Color bgColor;
  final bool showNotif;

  const BaseAppBar({
    Key key,
    this.title,
    this.showBackButton = true,
    this.trailing,
    this.actions,
    this.onSubmitSearch,
    this.bgColor,
    this.showNotif = false,
    this.leading,
    this.titleWidget,
  }) : super(key: key);

  @override
  _BaseAppBarState createState() => _BaseAppBarState();

  @override
  Size get preferredSize {
    return Size.fromHeight(55);
  }
}

class _BaseAppBarState extends State<BaseAppBar> {
  final gstate = Get.put(GlobalController());
  bool searchButton = false;
  Color fontColor = Colors.black;
  TextEditingController searchController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      backgroundColor: widget.bgColor ?? Colors.transparent,
      elevation: 0,
      brightness: Brightness.light,
      iconTheme: IconThemeData(color: primaryColor),
      textTheme: TextTheme(
        headline6: TextStyle(color: Color(0XFF8B8B8B), fontSize: 18),
      ),
      leading: widget.leading,
      title: (searchButton)
          ? Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              height: 40,
              child: TextField(
                // autofocus:true,
                controller: searchController,
                style: new TextStyle(color: fontColor),
                decoration: new InputDecoration(
                  // prefixIcon: new Icon(Icons.search, color: Colors.black),
                  hintText: 'Type here...',
                  hintStyle: TextStyle(color: fontColor),
                  contentPadding: const EdgeInsets.symmetric(horizontal: 20.0),
                ),
                onSubmitted: (val) {
                  widget.onSubmitSearch(val);
                },
              ),
            )
          : (widget.titleWidget != null)
              ? widget.titleWidget
              : Text(
                  widget.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      color: fontColor),
                ),
      actions: [
        if (widget.onSubmitSearch != null)
          Container(
            padding: EdgeInsets.all(10),
            child: GestureDetector(
              child: Icon((searchButton) ? Icons.close : Icons.search),
              onTap: () {
                setState(() {
                  searchButton = !searchButton;
                });
                if (!searchButton) {
                  searchController.text = '';
                  widget.onSubmitSearch('');
                }
              },
            ),
          ),
        if (widget.trailing != null) widget.trailing,
        if (widget.showNotif)
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: InkWell(
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => NotifPage()),
              ),
              child: Obx(
                () => gstate.totalNotif.value == 0
                    ? Image(
                        width: 25,
                        image: AssetImage('assets/icons/iconnonotif.png'),
                      )
                    : Image(
                        width: 25,
                        image: AssetImage('assets/icons/iconnotif.png'),
                      ),
              ),
            ),
          )
      ],
    );
  }
}
