import 'package:flutter/material.dart';

class BaseChip extends StatelessWidget {
  final Color backgroundColor;
  final Widget label;
  final double radius;
  final String text;
  const BaseChip(
      {Key key, this.backgroundColor, this.label, this.radius = 20, this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: backgroundColor,
      elevation: 3.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: (label != null)
            ? label
            : Text(
                text,
                style: TextStyle(
                    fontSize: 10,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
      ),
    );
  }
}
