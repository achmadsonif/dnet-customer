import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../utils/constants.dart';
import '../../utils/webservice.dart';

import '../alertx.dart';
import '../list_viewx.dart';
import 'base_app_bar.dart';
import 'base_model.dart';

class BaseList extends StatefulWidget {
  final dynamic model;
  final Function onTap;
  final String apiBase;
  final String name;
  final Widget listWidget;
  BaseList(
      {Key key,
      this.onTap,
      this.model,
      this.apiBase,
      this.name,
      this.listWidget})
      : super(key: key);

  @override
  _BaseListState createState() => _BaseListState();
}

class _BaseListState extends State<BaseList> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);

  String _searchController = '';

  List<Object> _dataList = List<Object>();

  int page = 1;

  Future<void> _loadMore() async {
    Map<String, dynamic> param = {
      'page': page,
      'limit': LENGTH_DATA,
      'sort': 'id,DESC'
    };
    if (_searchController.isNotEmpty)
      param['s'] = jsonEncode({
        'name': {'\$contL': '$_searchController'}
      });

    try {
      Map response = await Webservice().get(widget.apiBase, data: param);
      Iterable iter = response['data'];
      List newDataList = iter.map((i) => widget.model.fromJson(i)).toList();
      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> _refresh() async {
    page = 0;
    _dataList.clear();
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Widget _buildItemsForListView(BuildContext context, int i) {
    dynamic item = _dataList[i];

    return InkWell(
        child: ListTile(
          title: Text(item.name),
        ),
        onTap: () async {
          // bool val = await Navigator.of(context).push(
          //   MaterialPageRoute(
          //       builder: (context) => ProductCategoryAddPage(item: item)),
          // );
          // if (val != null && val) _refresh();
        },
        onLongPress: () => delete(item));
  }

  Future<void> delete(var item) async {
    // bool status = await Alertx.confirmDialog(
    //     label: 'Apa anda yakin ingin menghapus ${item.name} ?');
    // if (status) {
    //   Webservice()
    //       .delete(widget.apiBase + '/' + item.id)
    //       .then((value) => _refresh());
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: widget.name,
        onSubmitSearch: (val) {
          print(val);
          _searchController = val;
          _refreshController.requestRefresh();
        },
      ),
      body: ListViewx(
          controller: _refreshController,
          onRefresh: _refresh,
          onLoading: _loadMore,
          child: ListView.builder(
            itemCount: _dataList.length,
            itemBuilder: _buildItemsForListView,
            itemExtent: 50.0,
          )

          // ListView.builder(
          //   itemBuilder:_buildItemsForListView,
          //   itemExtent: 100.0,
          //   itemCount: _dataList.length,
          // ),
          ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () async {
          // bool val = await Navigator.push(
          //   context,
          //   MaterialPageRoute(builder: (context) => ProductCategoryAddPage()),
          // );
          // if (val != null && val) _refresh();
        },
      ),
    );
  }
}
