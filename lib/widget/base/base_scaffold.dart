import 'package:flutter/material.dart';

class BaseScaffold extends StatelessWidget {
  final Widget body;
  final Widget appBar;
  final Widget floatingActionButton;
  final FloatingActionButtonLocation floatingActionButtonLocation;
  final int backgroudType;
  const BaseScaffold(
      {Key key,
      this.body,
      this.backgroudType = 2,
      this.appBar,
      this.floatingActionButton,
      this.floatingActionButtonLocation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget bgWidget = bg2(MediaQuery.of(context).size.height * 0.90);

    if (backgroudType == 1) bgWidget = Container();
    if (backgroudType == 2)
      bgWidget = bg2(MediaQuery.of(context).size.height * 0.90);
    if (backgroudType == 3) bgWidget = bg3();
    if (backgroudType == 4)
      bgWidget = bg4(MediaQuery.of(context).size.height * 0.30);
    if (backgroudType == 5)
      bgWidget = bg5(MediaQuery.of(context).size.height * 0.60);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: appBar,
      body: Stack(children: [
        SingleChildScrollView(child: bgWidget),
        SafeArea(child: body)
      ]),
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: floatingActionButtonLocation,
    );
  }

  Widget bg1() {
    return Container(
      width: double.infinity,
      child: Image.asset(
        'assets/images/bgBody.png',
        fit: BoxFit.fill,
      ),
    );
  }

  Widget bg2(double height) {
    return Container(
      padding: EdgeInsets.only(top: 90),
      height: height,
      child: Image.asset(
        'assets/images/bgBody2.png',
        fit: BoxFit.fill,
      ),
    );
  }

  Widget bg3() {
    return Positioned(
      bottom: 0,
      child: Image.asset(
        'assets/images/bgBody3.png',
      ),
    );
  }

  Widget bg4(double height) {
    return Container(
      height: height,
      width: double.infinity,
      child: Image.asset(
        'assets/images/bgBody4.png',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget bg5(double height) {
    return Container(
      height: height,
      width: double.infinity,
      child: Image.asset(
        'assets/images/bgBody5.png',
        fit: BoxFit.cover,
      ),
    );
  }
}
