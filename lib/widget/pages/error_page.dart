import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/error.png'),
            SizedBox(height: 40,),
            Text('Oppps !!! ',style: TextStyle(color: Colors.grey,fontSize: 20,fontWeight: FontWeight.w500),),
          ],
        ),
      ),
    );
  }
}