import 'package:flutter/material.dart';
import '../../utils/base_style.dart';
import '../../utils/constants.dart';
import '../button/button_rounded.dart';

class SuccessPage extends StatelessWidget {
  final String text;
  final String desc;
  const SuccessPage({Key key, this.text='Success', this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: InkWell(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 60,
                  child: Icon(Icons.check,size: 100,color: Colors.white,),
                  backgroundColor: primaryColor,
                ),
                SizedBox(height: 40,),
                Text(text,style: TextStyle(color: secondaryColor,fontSize: 24,fontWeight: FontWeight.w500),),
                if(desc != null )...[
                  SizedBox(height: 10,),
                  Text(desc,style: TextStyle(color: Colors.grey,fontSize: 18,fontWeight: FontWeight.w500),),
                ],

                Padding(
                  padding: const EdgeInsets.all(30),
                  child: ButtonRounded(label: 'OK',color: primaryColor,),
                )



              ],
            ),
          ),
          onTap: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }
}