import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/utils/constants.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Alertx {
  static var alertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: false,
  );
  static warning(String msg) {
    if (Get.isDialogOpen) Get.back();
    Get.snackbar('Error', msg,
        colorText: Colors.white, backgroundColor: Colors.amber);
  }

  static error(context, msg) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: "$msg",
      style: alertStyle,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.red,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
  }

  success(msg, dynamic page) {
    Color color = Color(0xff37D88E);
    Get.dialog(
      Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        child: Container(
          color: Colors.transparent,
          height: 350,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 200,
                child: Stack(
                  children: [
                    Center(
                        child: SvgPicture.asset(
                      'assets/icons/successicon.svg',
                      height: 100,
                      width: 100,
                    )),
                    Center(
                        child: SvgPicture.asset(
                      'assets/icons/successbg.svg',
                      height: 200,
                    )),
                  ],
                ),
              ),
              Text(
                "SUCCESS!",
                style: TextStyle(
                    color: color, fontSize: 22, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                msg,
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                child: Card(
                  color: color,
                  elevation: 3.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 25),
                    child: Text('DONE',
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                onTap: () => Get.offAll(page),
              )
            ],
          ),
        ),
      ),
    ).then((value) => Get.offAll(page));
  }

  static Future<bool> confirmDialog(
      {String title = '', String label = 'Apakah anda yakin ?'}) async {
    return showDialog(
      context: Get.context,
      builder: (context) => AlertDialog(
        // title: new Text('Are you sure?'),
        content: Text(label),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Tidak'),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text('Ya'),
          ),
        ],
      ),
    );
    // return Alert(
    //   context: Get.context,
    //   type: AlertType.warning,
    //   title: title,
    //   desc: label,
    //   buttons: [
    //     DialogButton(
    //       child: Text(
    //         "BATAL",
    //         style: TextStyle(color: Colors.white, fontSize: 20),
    //       ),
    //       onPressed: () => Get.back(result: false),
    //     ),
    //     DialogButton(
    //       child: Text(
    //         "YA",
    //         style: TextStyle(color: Colors.white, fontSize: 20),
    //       ),
    //       onPressed: () => Get.back(result: true),
    //       color: Color.fromRGBO(235, 52, 52, 1.0),
    //     )
    //   ],
    // ).show();
    // return Get.dialog(AlertDialog(
    //   content: Text(label),
    //   actions: <Widget>[
    //     FlatButton(
    //       child: const Text(
    //         'Batal',
    //         style: TextStyle(color: Colors.grey),
    //       ),
    //       onPressed: () {
    //         Get.back(result: false);
    //       },
    //     ),
    //     FlatButton(
    //       child: const Text('Ya'),
    //       onPressed: () {
    //         Get.back(result: true);
    //       },
    //     ),
    //   ],
    // ));
  }

  static Future<bool> confirmDialog2(
      {String title = 'Apakah anda yakin ?', String desc = ''}) async {
    return Get.dialog(
      Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        child: Container(
          color: Colors.transparent,
          height: 350,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.info_outline,
                color: Color(0xFFFFC700),
                size: 100,
              ),
              SizedBox(
                height: 25,
              ),
              Text(
                title,
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                desc,
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    child: Card(
                      color: Color(0xFFFE5152),
                      elevation: 3.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 25),
                        child: Text('Ya',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                    onTap: () => Get.back(result: true),
                  ),
                  InkWell(
                    child: Card(
                      color: Color(0xff595959),
                      elevation: 3.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 25),
                        child: Text('Tidak',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                    onTap: () => Get.back(result: false),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  static loading() {
    Get.dialog(AlertDialog(
      content: Container(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [CircularProgressIndicator(), Text('Loading..')],
        ),
      ),
    ));
  }
}
