import '../../utils/gps_utils.dart';
import '../../widget/button/button_rounded.dart';
import '../../widget/notif_warning.dart';
import 'package:flutter/material.dart';

import 'form_input.dart';

class FormGps extends StatefulWidget {

  @required final TextEditingController latController;
  @required final TextEditingController longController;

  FormGps({
    this.latController, 
    this.longController
  });

  @override
  _FormGpsState createState() => _FormGpsState();
}

class _FormGpsState extends State<FormGps> {

  @override
  void initState() { 
    super.initState();
    getLocation();
    // 
  }
  
  bool _loading = true;
  bool _isError = false;
  String _errorMessage= '';
  GpsLocation gpsLocation;

  Future<void> getLocation() async {
    setState(() { _loading = true;});
    await getGPS();
    setState(() { _loading = false;});
    
  }

  Future<void> getGPS() async {    

      try {
        gpsLocation = await GpsUtils().getLocation();
        setState(() {
          widget.latController.text= gpsLocation.latitude.toString();
          widget.longController.text= gpsLocation.longitude.toString();
        });
      } catch (e) {
        print('error gps $e');
      }
    
  }  

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: FormInput(
                title: 'latitude',
                controller: widget.latController,
                placeholder: 'Turn GPS ON',
                // validatorRequired: true,
                readOnly: true,
              ),
            ),
            Expanded(
              child: FormInput(
                title: 'longitude',
                controller: widget.longController,
                placeholder: 'Turn GPS ON',
                // validatorRequired: true,
                readOnly: true,
              ),
            ),
          ],
        ),
        (_isError) 
          ? Padding(
              padding: const EdgeInsets.all(8.0),
              child: NotifWarning(text: _errorMessage),
            )
          : (gpsLocation == null )
            ?(_loading) 
              ? LinearProgressIndicator()
              :ButtonRounded(
                label: (_loading)?'Processing...':'Get Location', 
                icon: Icons.gps_not_fixed,
                onPressed:(){
                  if(!_loading){
                    getLocation();
                  }
                } 
              )
            :SizedBox.shrink(),
      ],
    );
  }
}