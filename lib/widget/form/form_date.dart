import 'package:flutter/material.dart';
// import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'form_input.dart';

enum FormDateType { ios, android }

class FormDate extends StatelessWidget {
  final String title;
  final String placeholder;
  final Function validator;
  final bool validatorRequired;
  final ValueChanged<DateTime> onChanged;
  final TextEditingController controller;
  final String formatDate;
  final FormDateType formDateType;
  final InputDecoration decoration;

  // TextEditingController _controller = TextEditingController();
  // String formatViewDate = 'dd MMM y';

  const FormDate(
      {Key key,
      this.title,
      this.placeholder,
      this.validator,
      this.validatorRequired = false,
      this.onChanged,
      this.controller,
      this.formatDate = 'yyyy-MM-dd',
      this.formDateType,
      this.decoration})
      : super(key: key);

  // @override
  // void initState() {
  //   super.initState();
  //   if(widget.initialValue != null){
  //     _controller.text = DateFormat(formatViewDate).format(widget.initialValue);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FormInput(
          title: title,
          controller: controller,
          placeholder: placeholder,
          readOnly: true,
          validatorRequired: validatorRequired,
          decoration: decoration,
          onTap: () {
            if (formDateType == FormDateType.ios)
              showdateTypeIos();
            else
              showdateTypeAndroid(context);
          },
        ),
      ],
    );
  }

  void showdateTypeIos() {
    // DatePicker.showDatePicker(
    //   context,
    //   onMonthChangeStartWithFirstDate: true,
    //   dateFormat: 'yyyy-MMMM-dd',
    //   onConfirm: (dateTime, List<int> index) {
    //     setState(() {
    //       _controller.text = DateFormat(formatViewDate).format(dateTime);
    //     });
    //     widget.onChanged(dateTime);
    //   },
    // );
  }

  Future<void> showdateTypeAndroid(BuildContext context) async {
    final DateTime d = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
    );
    if (d != null) {
      controller.text = DateFormat(formatDate).format(d);
      if (onChanged != null) onChanged(d);
    }
  }
}
