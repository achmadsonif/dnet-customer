import 'package:flutter/material.dart';

class FormSwitch extends StatelessWidget {
  final bool value;
  final String title;
  final ValueChanged<bool> onChanged;
  const FormSwitch({Key key, this.value, this.onChanged, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title),
          Switch(
            value: value, 
            onChanged: onChanged,
          ),
        ],
      ),
    );
  }
}