import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/constants.dart';
import '../../utils/base_style.dart';

enum ValidationType {
  hp,
  email,
}

class FormInput extends StatefulWidget {
  final String title;
  final String placeholder;
  final TextEditingController controller;
  final bool readOnly;
  final bool obscureText;
  final TextInputType keyboardType;
  final Function validator;
  final List<ValidationType> validatioType;
  final Function onChanged;
  final Function onTap;
  final bool validatorRequired;
  final String initialValue;
  final int maxLine;
  final IconData icon;
  final Widget prefix;
  final InputDecoration decoration;

  FormInput({
    this.title,
    @required this.controller,
    this.placeholder,
    this.readOnly = false,
    this.keyboardType = TextInputType.text,
    this.validator,
    this.onChanged,
    this.onTap,
    this.validatorRequired = false,
    this.initialValue,
    this.maxLine = 1,
    this.icon,
    this.obscureText = false,
    this.prefix,
    this.decoration,
    this.validatioType = const [],
  });

  @override
  _FormInputState createState() => _FormInputState();
}

class _FormInputState extends State<FormInput> {
  FocusNode myFocusNode;
  bool _focused = false;
  bool obscureText = false;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    myFocusNode.addListener(_handleFocusChange);
    if (widget.obscureText) {
      obscureText = true;
    }
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  void _handleFocusChange() {
    setState(() {
      _focused = myFocusNode.hasFocus ? true : false;
    });

    // if (myFocusNode.hasFocus != _focused) {
    //   setState(() {
    //     _focused = myFocusNode.hasFocus;
    //   });
    // }
  }

  String checkValidation(value) {
    if (widget.validatorRequired && value.isEmpty)
      return (widget.title ?? '') + ' tidak boleh kosong';
    if (widget.validatioType.contains(ValidationType.hp)) {
      if (value.length > 19)
        return (widget.title ?? '') + ' max digit 19';
      else if (int.tryParse(value) == null)
        return (widget.title ?? '') + ' hanya angka saja';
    }
    if (widget.validator != null) {
      String res = widget.validator(value);
      if (res != null) return res;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          TextFormField(
            style: TextStyle(fontSize: 12),
            readOnly: widget.readOnly,
            keyboardType: (widget.maxLine > 1)
                ? TextInputType.multiline
                : widget.keyboardType,
            controller: widget.controller,
            onChanged: widget.onChanged,
            onTap: widget.onTap,
            maxLines: widget.maxLine,
            initialValue: widget.initialValue,
            obscureText: obscureText,
            validator: checkValidation,
            // validator: (widget.validatorRequired)
            //     ? ((value) {
            //         if (value.isEmpty) {
            //           return (widget.title ?? '') + ' tidak boleh kosong';
            //         }
            //         return null;
            //       })
            //     : widget.validator,
            decoration: (widget.decoration != null)
                ? widget.decoration
                : InputDecoration(
                    labelText: widget.title,
                    labelStyle: TextStyle(
                        color: _focused ? primaryColor : Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: primaryColor, width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    hintText: widget.placeholder,
                    prefixIcon: widget.prefix,
                    // If  you are using latest version of flutter then lable text and hint text shown like this
                    // if you r using flutter less then 1.20.* then maybe this is not working properly
                    floatingLabelBehavior: FloatingLabelBehavior.always,
                    suffixIcon: (widget.obscureText)
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                obscureText = !obscureText;
                              });
                            },
                            child: Icon(
                                (obscureText) ? Icons.lock : Icons.lock_open))
                        : (widget.icon != null)
                            ? Icon(
                                widget.icon,
                              )
                            : null),
          )
        ]));
  }
}
