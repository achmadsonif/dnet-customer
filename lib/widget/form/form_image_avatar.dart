import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class FormImageAvatar extends StatefulWidget {
  final String title;
  final bool cameraOnly;
  final String urlImage;
  final ValueChanged<File> onChanged;
  final double size;
  FormImageAvatar({
    Key key, 
    this.onChanged, 
    this.title='', 
    this.cameraOnly=false, this.urlImage,
    this.size = 200
  }) : super(key: key);

  @override
  _FormImageAvatarState createState() => _FormImageAvatarState();
}

class _FormImageAvatarState extends State<FormImageAvatar> {
  File foto;

  Future getImagefotoCamera() async {
    try {
      var image = await ImagePicker().getImage(source: ImageSource.camera,maxHeight: 450.0, maxWidth: 450.0);
      
      if (image != null)
      setState(() {
        updateImage(File(image.path));
      });
    } catch (e) {
      print(e);
    }
  }

  Future getImagefotoGallery() async {
    try {
      var image = await ImagePicker().getImage(source: ImageSource.gallery,maxHeight: 1024.0,maxWidth: 1024.0);

      if (image != null)
      setState(() {
        updateImage(File(image.path));
      });
    } catch (e) {
      print(e);
    }
  }

  void updateImage(File image) {
    foto = image;
    widget.onChanged(image);
  }

  void showCamera() {
    showDialog<bool>(
      context: context,
      barrierDismissible: false, 
      // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Image source'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Divider(),
              SizedBox(height: 20,),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    OutlineButton(
                      child: Icon(Icons.camera_alt,color: Colors.amber,),
                      padding: EdgeInsets.all(15),
                      shape: CircleBorder(),
                      borderSide: BorderSide(color: Colors.amber),
                      onPressed: (){
                        Navigator.of(context).pop();
                        getImagefotoCamera();
                      },
                    ),
                    OutlineButton(
                      child: Icon(Icons.image,color: Colors.amber,),
                      padding: EdgeInsets.all(15),
                      shape: CircleBorder(),
                      borderSide: BorderSide(color: Colors.amber),
                      onPressed: (){
                        Navigator.of(context).pop();
                        getImagefotoGallery();
                      },
                    ) ,
                  ]
                ),
            ],
          ),
        );
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: InkWell(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(200),
                  child: foto == null
                    ? Image.network(
                        widget.urlImage,
                        height: widget.size,
                        width: widget.size,
                        fit: BoxFit.cover,
                      )
                    : Image.file(
                        foto,
                        height: widget.size,
                        width: widget.size,
                        fit: BoxFit.cover,
                      )
                ),
                Positioned.fill(
                  child: Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    decoration: BoxDecoration(
                      color:Colors.amber,
                      shape: BoxShape.circle,
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Icon(Icons.camera_alt,color: Colors.white,),
                    ),
                  ),
                ))
              ],
            ),
            const SizedBox(height: 5.0),

          ],
        ),
        onTap: () {
          showCamera();
        },
      ),
    );
  }
}