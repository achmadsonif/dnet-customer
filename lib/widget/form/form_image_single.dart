import 'dart:io';

import '../../utils/constants.dart';

import '../../widget/button/button_rounded.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class FormImageSingle extends StatefulWidget {
  final String title;
  final bool cameraOnly;
  final ValueChanged<File> onChanged;
  final String folder;
  final String filename;
  FormImageSingle({
    Key key, 
    this.onChanged, 
    this.title='', 
    this.cameraOnly=false, 
    this.folder, 
    this.filename,
  }) : super(key: key);

  @override
  _FormImageSingleState createState() => _FormImageSingleState();
}

class _FormImageSingleState extends State<FormImageSingle> {
  File foto;
  String url;

  @override
  void initState() { 
    super.initState();
    if(widget.folder != null && widget.filename != null)
      url = URL+'image/${widget.folder}/${widget.filename}';
  }

  Future getImagefotoCamera() async {
    try {
      var image = await ImagePicker().getImage(source: ImageSource.camera,maxHeight: 450.0, maxWidth: 450.0);
      
      if (image != null)
      setState(() {
        updateImage(File(image.path));
      });
    } catch (e) {
      print(e);
    }
  }

  Future getImagefotoGallery() async {
    try {
      var image = await ImagePicker().getImage(source: ImageSource.gallery,maxHeight: 1024.0,maxWidth: 1024.0);

      if (image != null)
      setState(() {
        updateImage(File(image.path));
      });
    } catch (e) {
      print(e);
    }
  }

  void updateImage(File image) {
    foto = image;
    widget.onChanged(image);
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title),
          const SizedBox(height: 5.0),
          (widget.cameraOnly) 
            ? ButtonRounded(
                label: 'Camera',
                icon : Icons.camera, 
                onPressed: getImagefotoCamera,
              )
            : Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                OutlineButton(
                  child: Icon(Icons.camera_alt,color: Colors.amber,),
                  onPressed: getImagefotoCamera,
                  padding: EdgeInsets.all(15),
                  shape: CircleBorder(),
                  borderSide: BorderSide(color: Colors.amber),
                ),
                OutlineButton(
                  child: Icon(Icons.image,color: Colors.amber,),
                  onPressed: getImagefotoGallery,
                  padding: EdgeInsets.all(15),
                  shape: CircleBorder(),
                  borderSide: BorderSide(color: Colors.amber),
                ) ,
              ]
            ),
          SizedBox(height: 10.0),
          (foto != null) 
            ? Container(
              height: 200.0,
              width: 400,
              color: Colors.grey[300],
              padding: EdgeInsets.all(5.0),
              child: Image.file(foto),
            )
            :(url != null)
              ? Container(
                height: 200.0,
                width: 400,
                color: Colors.grey[300],
                padding: EdgeInsets.all(5.0),
                child: Image.network(url),
              )
              : SizedBox.shrink()
              ,
        ],
      ),
    );
  }
}