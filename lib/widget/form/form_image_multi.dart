import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../alertx.dart';
import '../image_with_icon.dart';

class FormImageMulti extends StatefulWidget {
  final String title;
  final ValueChanged<List> onChanged;
  FormImageMulti({
    Key key, 
    this.onChanged, 
    this.title='',
  }) : super(key: key);

  @override
  _FormImageMultiState createState() => _FormImageMultiState();
}

class _FormImageMultiState extends State<FormImageMulti> {
  List<File> _fotoList = [];

  Future getImagefotoCamera() async {
    try {
      var image = await ImagePicker().getImage(source: ImageSource.camera,maxHeight: 1024.0, maxWidth: 1024.0);
      
      if (image != null)
      setState(() {
        addImageList(File(image.path));
      });
    } catch (e) {
      print(e);
    }
  }

  Future getImagefotoGallery() async {
    try {
      var image = await ImagePicker().getImage(source: ImageSource.gallery,maxHeight: 1024.0,maxWidth: 1024.0);

      if (image != null)
      setState(() {
        addImageList(File(image.path));
      });
    } catch (e) {
      print(e);
    }
  }

  void addImageList(File image) {
    _fotoList.add(image);
    widget.onChanged(_fotoList);
  }

  void removeImageList(int index) {
    _fotoList.removeAt(index);
    widget.onChanged(_fotoList);
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.title),
          const SizedBox(height: 5.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              OutlineButton(
                child: Icon(Icons.camera_alt,color: Colors.amber,),
                onPressed: getImagefotoCamera,
                padding: EdgeInsets.all(15),
                shape: CircleBorder(),
                borderSide: BorderSide(color: Colors.amber),

              ),
              OutlineButton(
                child: Icon(Icons.image,color: Colors.amber,),
                onPressed: getImagefotoGallery,
                padding: EdgeInsets.all(15),
                shape: CircleBorder(),
                borderSide: BorderSide(color: Colors.amber),
              ),
            ]
          ),
          (_fotoList.length > 0)
            ? GridView.count(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              childAspectRatio: 1,
              crossAxisCount: 2,
              children: List.generate(_fotoList.length, (index) {
                File foto = _fotoList[index];
                return 
                    ImageWithIcon(
                      imageFile: foto,
                      ontapIcon: ()async {
                        bool confirm = await Alertx.confirmDialog() ;
                        if(confirm){
                          setState(() {
                            removeImageList(index);
                          });
                        }
                      },
                    // ),
                    // Container(
                    //   padding: EdgeInsets.all(10),
                    //   width: double.infinity,
                    //   child:RaisedButton(
                    //     child: Icon(Icons.edit),
                    //     textColor: Colors.white,
                    //     color: Colors.red,
                    //     onPressed: null
                    //   )
                    // ),
                  // ],
                );
              })
            )
            :SizedBox.shrink(),
        ],
      ),
    );
  }
}