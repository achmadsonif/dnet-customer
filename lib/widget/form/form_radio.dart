import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/constants.dart';

class FormRadio extends StatelessWidget {
  final String title;
  final RadioModel selected;
  final ValueChanged<RadioModel> onChanged;
  final List<RadioModel> listItem;
  final bool validatorRequired;
  final Function validator;

  const FormRadio({
    Key key,
    this.title = '',
    this.selected,
    @required this.onChanged,
    @required this.listItem,
    this.validatorRequired = false,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormField(
      builder: (FormFieldState<bool> state) {
        return Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputDecorator(
                  decoration: InputDecoration(
                      labelText: title,
                      labelStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(28),
                        borderSide: BorderSide(
                            color: state.hasError ? Colors.red : kTextColor),
                        gapPadding: 10,
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (var item in listItem)
                        Row(
                          children: [
                            Radio(
                              value: item,
                              groupValue: selected,
                              onChanged: onChanged,
                            ),
                            Text(item.name)
                          ],
                        ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                state.hasError
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 42),
                        child: Text(
                          state.errorText,
                          style: TextStyle(color: Colors.red),
                        ),
                      )
                    : Container(),
              ],
            ));
      },
      validator: (validatorRequired)
          ? ((value) {
              if (selected == null) {
                return (title ?? '') + ' tidak boleh kosong';
              }
              return null;
            })
          : validator,
    );
    // return Padding(
    //   padding: const EdgeInsets.all(10.0),
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.start,
    //     crossAxisAlignment: CrossAxisAlignment.start,
    //     children: [
    // Text(
    //   title,
    //   style: TextStyle(
    //     fontWeight: FontWeight.bold,
    //     color: Colors.black,
    //   ),
    // ),

    // for (var item in listItem)
    //   Row(
    //     children: [
    //       Radio(
    //         value: item,
    //         groupValue: selected,
    //         onChanged: onChanged,
    //       ),
    //       Text(item.name)
    //     ],
    //   ),
    //     ],
    //   ),
    // );
  }
}

class RadioModel {
  final String id;
  final String name;

  RadioModel(this.id, this.name);
}
