import 'dart:convert';

import '../../utils/constants.dart';
import '../../widget/base/base_app_bar.dart';

import '../../utils/webservice.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../list_viewx.dart';

class FormDropdownSearch extends StatefulWidget {
  final String title;
  final String hintText;
  final String errorText;
  final String textField;
  final String valueField;
  final ValueChanged<DropdownItem> onChanged;
  final Map<String, dynamic> paramPost;
  final Function validator;
  final String url;
  final bool validatorRequired;
  final DropdownItem dropdownItem;
  final TextEditingController controller;

  FormDropdownSearch({
    Key key,
    this.title,
    this.hintText = 'Select one option',
    this.errorText,
    this.onChanged,
    this.textField = 'name',
    this.valueField = 'id',
    this.url,
    this.paramPost,
    this.validator,
    this.validatorRequired = false,
    this.controller,
    this.dropdownItem,
  }) : super(key: key);

  @override
  FormDropdownSearchState createState() => FormDropdownSearchState();
}

class FormDropdownSearchState extends State<FormDropdownSearch> {
  TextEditingController _ddController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.dropdownItem != null)
      _ddController.text = widget.dropdownItem.name;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            (widget.title != null) ? Text(widget.title) : SizedBox.shrink(),
            (widget.title != null) ? SizedBox(height: 10) : SizedBox.shrink(),
            TextFormField(
                readOnly: true,
                controller: (widget.controller != null)
                    ? widget.controller
                    : _ddController,
                decoration: InputDecoration(
                  hintText: widget.hintText,
                  hintStyle: TextStyle(fontSize: 12, color: Color(0xFFD0D0D0)),
                  border: OutlineInputBorder(),
                  suffixIcon: Icon(
                    Icons.arrow_drop_down,
                    color: Colors.grey,
                  ),
                ), // Create an optional decoration for your TextFormField
                validator: (widget.validatorRequired)
                    ? ((value) {
                        if (value.isEmpty) {
                          return 'Please select';
                        }
                        return null;
                      })
                    : widget.validator,
                onTap: () async {
                  DropdownItem p = await Navigator.of(context)
                      .push(new MaterialPageRoute<DropdownItem>(
                    builder: (BuildContext context) {
                      return DropdownSearchFilter(
                        textField: widget.textField,
                        valueField: widget.valueField,
                        url: widget.url,
                        paramPost: widget.paramPost,
                      );
                    },
                    // fullscreenDialog: true
                  ));
                  if (p != null) {
                    setState(() {
                      (widget.controller != null)
                          ? widget.controller.text = p.name
                          : _ddController.text = p.name;
                    });
                    widget.onChanged(p);
                  }
                })
          ],
        ));
  }
}

class DropdownSearchFilter extends StatefulWidget {
  final String textField;
  final String valueField;
  final String url;
  final Map<String, dynamic> paramPost;

  const DropdownSearchFilter(
      {Key key, this.textField, this.valueField, this.url, this.paramPost})
      : super(key: key);
  @override
  _DropdownSearchFilterState createState() => new _DropdownSearchFilterState();
}

class _DropdownSearchFilterState extends State<DropdownSearchFilter> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);
  String _searchController = '';
  List _dataList = [];
  int page = 1;

  @override
  void initState() {
    super.initState();
  }

  Future<void> _loadMore() async {
    List newDataList = [];
    Map<String, dynamic> param = {'page': page, 'limit': 30, 'sort': 'id,DESC'};
    if (_searchController.isNotEmpty)
      param['s'] = jsonEncode({
        'name': {'\$contL': '$_searchController'}
      });
    if (widget.paramPost != null) param.addAll(widget.paramPost);
    try {
      var response = await Webservice().get(widget.url, data: param);
      newDataList = response['data'];

      if (newDataList.length > 0) {
        page += 1;
        setState(() {
          _dataList.addAll(newDataList);
        });
      }
      (newDataList.length < LENGTH_DATA || newDataList.length == 0)
          ? _refreshController.loadNoData()
          : _refreshController.loadComplete();
    } catch (e) {
      print(e);
      _refreshController.loadFailed();
    }
  }

  Future<void> refresh() async {
    page = 1;
    _dataList.clear();
    await _loadMore();
    _refreshController.refreshCompleted();
  }

  Column _buildItemsForListView(BuildContext context, int index) {
    // KondisiPasar item = _dataList[index];
    return Column(children: <Widget>[
      InkWell(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10),
                child: Text(_dataList[index][widget.textField],
                    style: TextStyle(fontSize: 14)),
              ),
              Divider()
            ],
          ),
          onTap: () {
            Navigator.of(context).pop(DropdownItem(
              id: _dataList[index][widget.valueField],
              name: _dataList[index][widget.textField],
              data: _dataList[index],
            ));
          }),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Pilih',
        onSubmitSearch: (val) {
          print(val);
          _searchController = val;
          _refreshController.requestRefresh();
        },
      ),
      body: ListViewx(
        controller: _refreshController,
        onRefresh: refresh,
        onLoading: _loadMore,
        child: ListView.builder(
          itemCount: _dataList.length,
          itemBuilder: _buildItemsForListView,
        ),
      ),
    );
  }
}

class DropdownItem {
  int id;
  String name;
  Map data;

  DropdownItem({this.id, this.name = '', this.data});
}
