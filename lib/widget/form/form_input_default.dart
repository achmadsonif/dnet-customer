import 'package:flutter/material.dart';

class FormInputDefault extends StatelessWidget {
  final String title;
  final String placeholder;
  final TextEditingController controller;
  final bool readOnly;
  final TextInputType keyboardType;
  final Function validator;
  final Function onChanged;
  final Function onTap;
  final bool validatorRequired;
  final String initialValue;
  final int maxLine;

  FormInputDefault({
    this.title, 
    @required this.controller, 
    this.placeholder,
    this.readOnly = false,
    this.keyboardType =TextInputType.text,
    this.validator,
    this.onChanged,
    this.onTap,
    this.validatorRequired = false, 
    this.initialValue, 
    this.maxLine=1,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title),
          const Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
          TextFormField(
            readOnly: readOnly,
            validator: (validatorRequired)
            ? ((value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
            }):validator,
            controller: controller,
            onChanged: onChanged,
            onTap: onTap,
            maxLines: maxLine,
            initialValue: initialValue,
            keyboardType: (maxLine > 1) ? TextInputType.multiline : keyboardType,
            decoration: InputDecoration(
              hintText: placeholder,
              hintStyle: TextStyle(
                // fontSize: 12,
                fontStyle: FontStyle.italic,
                color: Color(0xFFD0D0D0)
              ),
              border: OutlineInputBorder(),
            ),
            // keyboardType: TextInputType.number,
          ),
        ],
      ),
    );
  }
}