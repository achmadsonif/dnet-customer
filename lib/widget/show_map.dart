import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';


class ShowMap extends StatelessWidget {
  final double lat;
  final double long;
  final double zoom;
  const ShowMap({Key key, this.lat, this.long, this.zoom=13.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return 
      Container(
        height: 200,
        child: FlutterMap(
          options: new MapOptions(
            center: new LatLng(lat, long),
            zoom: zoom,
          ),
          layers: [
            new TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c'],
              tileProvider: NonCachingNetworkTileProvider(),
            ),
            new MarkerLayerOptions(
              markers: [
                new Marker(
                  // width: 40.0,
                  // height: 40.0,
                  point: new LatLng(lat, long),
                  builder: (ctx) =>
                  new Container(
                    child: CircleAvatar(child: Icon(Icons.person_pin,),),
                  ),
                ),
              ],
            ),
          ],
        )
      );
  }
}