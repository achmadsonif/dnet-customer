import 'package:flutter/material.dart';

class NotifWarning extends StatelessWidget {
  final text;
  const NotifWarning({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width:
          MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: hexToColor("#F9CEDA"),
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(
            15),
        child: Row(
          children: <Widget>[
            // SvgPicture.asset(
            //     'assets/ic_notif_warning.svg'),
            SizedBox(width: 10),
            Expanded(
              child: Text(
                text,
                style: TextStyle(
                    color: hexToColor("#C7043A"),
                    fontSize: 10,
                    fontWeight:
                        FontWeight.normal),
              ),
            )
          ],
        ),
      ),
    );
  }

  static Color hexToColor(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return new Color(int.parse(hexColor, radix: 16) + 0xFF000000);
  }
}