import 'package:flutter/material.dart';

class SquareContainer extends StatelessWidget {
  const SquareContainer({
    Key key,
    @required this.color,
    @required this.child,
    @required this.height,
  }) : super(key: key);

  final Color color;
  final Widget child;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height != null ? height : null,
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.only(bottom: 20),
      width: double.infinity,
      decoration: BoxDecoration(
        color: color,
      ),
      child: child,
    );
  }
}
