import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ListViewx extends StatelessWidget {
  final Widget child;
  final RefreshController controller;
  final Function onRefresh;
  final Function onLoading;
  final bool enablePullDown;
  final bool enablePullUp;
  final Axis scrollController;
  const ListViewx(
      {Key key,
      this.child,
      this.controller,
      this.onRefresh,
      this.onLoading,
      this.enablePullUp = true,
      this.enablePullDown = true,
      this.scrollController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        scrollDirection: scrollController,
        enablePullDown: enablePullDown,
        enablePullUp: enablePullUp,
        header: WaterDropMaterialHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CircularProgressIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("");
            }
            print(mode);
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: controller,
        onRefresh: onRefresh,
        onLoading: onLoading,
        child: child);
  }
}
