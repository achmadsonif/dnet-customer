import 'package:flutter/material.dart';

class ButtonRounded extends StatelessWidget {
  final String label;
  final Color color;
  final IconData icon;
  final Function onPressed;
  final bool loading;
  const ButtonRounded(
      {Key key,
      this.label = '',
      this.onPressed,
      this.color = Colors.amber,
      this.icon,
      this.loading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return (loading)
        ? Container(
            child: Center(child: CircularProgressIndicator()),
          )
        : Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(100)),
                color: color),
            child: FlatButton(
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                (icon != null)
                    ? Icon(
                        icon,
                        color: Colors.white,
                      )
                    : SizedBox.shrink(),
                SizedBox(width: 10),
                Text(
                  label,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 18),
                ),
              ]),
              onPressed: onPressed,
            ),
          );
  }
}
