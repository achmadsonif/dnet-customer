import 'package:flutter/material.dart';

class ButtonRoundedSquare extends StatelessWidget {
  final Color bgColor;
  final Color iconColor;
  final double size;
  final Function onPressed;
  final IconData icon;
  const ButtonRoundedSquare(
      {Key key,
      this.bgColor = Colors.blue,
      this.iconColor = Colors.white,
      this.size = 30,
      this.onPressed,
      @required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(size * 0.3), //or 15.0
        child: Container(
          height: size,
          width: size,
          color: bgColor,
          child: Icon(icon, color: iconColor, size: size * 0.6),
        ),
      ),
      onTap: onPressed,
    );
  }
}
