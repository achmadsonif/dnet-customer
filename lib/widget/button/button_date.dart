import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import '../../utils/constants.dart';
import '../../utils/base_utils.dart';
import 'package:dart_date/dart_date.dart';

class BaseButtonDate extends StatelessWidget {
  final Function(DateTime) onConfirm;
  final DateTime value;
  final DateTime minTime;
  final DateTime maxTime;
  final String placeholder;
  final bool iosModel;
  const BaseButtonDate({
    Key key,
    this.onConfirm,
    this.value,
    this.placeholder = 'Tanggal',
    this.iosModel = false,
    this.minTime,
    this.maxTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
        side: BorderSide(
          color: Colors.grey,
          // width: 2,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            FocusManager.instance.primaryFocus.unfocus();
            if (iosModel) {
              DatePicker.showDatePicker(
                context,
                showTitleActions: true,
                minTime: (minTime != null) ? minTime : DateTime(2020, 3, 5),
                // maxTime: DateTime(2019, 6, 7),
                onConfirm: onConfirm,
                currentTime: (value != null) ? value : DateTime.now(),
                locale: LocaleType.id,
              );
            } else {
              showDatePicker(
                context: context,
                initialDate: (value != null)
                    ? value
                    : (minTime != null)
                        ? minTime
                        : DateTime.now(),
                firstDate:
                    (minTime != null) ? minTime.startOfDay : DateTime(2010),
                lastDate:
                    (maxTime != null) ? maxTime.startOfDay : DateTime(2101),
              ).then((value) => onConfirm(value));
            }
          },
          child: Row(
            children: [
              Icon(
                Icons.date_range,
                color: primaryColor,
              ),
              SizedBox(
                width: 10,
              ),
              Text((value != null) ? value.toDate() : placeholder),
            ],
          ),
        ),
      ),
    );
  }
}
