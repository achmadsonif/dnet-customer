import 'package:flutter/material.dart';
import '../../utils/constants.dart';
import '../../utils/size_config.dart';
import '../../utils/base_style.dart';

class ButtonSave extends StatelessWidget {
  final String label;
  final IconData icon;
  final Function onPressed;
  final bool loading;
  const ButtonSave(
      {Key key,
      this.label = '',
      this.onPressed,
      this.icon,
      this.loading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return (loading)
        ? Container(
            child: Center(child: CircularProgressIndicator()),
          )
        : SizedBox(
            width: double.infinity,
            height: 40,
            child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: primaryColor,
              onPressed: onPressed,
              child: Text(
                label,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
            ),
          );
  }
}
