import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import '../../utils/constants.dart';
import '../../utils/base_utils.dart';

class BaseButtonTime extends StatelessWidget {
  final Function(DateTime) onConfirm;
  final DateTime value;
  const BaseButtonTime({
    Key key,
    this.onConfirm,
    this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
        side: BorderSide(
          color: Colors.grey,
          // width: 2,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () {
            FocusManager.instance.primaryFocus.unfocus();
            DatePicker.showTimePicker(
              context,
              currentTime: value,
              showTitleActions: true,
              showSecondsColumn: false,
              onConfirm: onConfirm,
              locale: LocaleType.id,
            );
          },
          child: Row(
            children: [
              Icon(
                Icons.access_time,
                color: primaryColor,
              ),
              SizedBox(
                width: 10,
              ),
              Text((value != null) ? value.toTime() : 'Jam'),
            ],
          ),
        ),
      ),
    );
  }
}
