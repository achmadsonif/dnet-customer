import 'dart:io';

import 'package:flutter/material.dart';

class ImageWithIcon extends StatelessWidget {
  final File imageFile;
  final Function ontapIcon;
  const ImageWithIcon({Key key, this.imageFile, this.ontapIcon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          child: Image.file(imageFile)
        ),
        Positioned(
          right: 0,
          child: new Container(
            padding: EdgeInsets.all(1),
            decoration: new BoxDecoration(
              color: Colors.red[300],
              borderRadius: BorderRadius.circular(20),
            ),
            child: InkWell(
              child: Icon(Icons.close,color: Colors.white,size: 20,),
              onTap: ontapIcon
            ),
          ),
        ),
      ],
    );
  }
}