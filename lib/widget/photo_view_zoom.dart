import 'package:flutter/material.dart';
import 'package:mobile_customer/widget/base/base_app_bar.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewZoom extends StatelessWidget {
  final String url;
  const PhotoViewZoom({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Foto',
      ),
      body: Container(
          color: Colors.white,
          child: PhotoView(
            backgroundDecoration: BoxDecoration(color: Colors.white),
            imageProvider: NetworkImage(url),
            minScale: PhotoViewComputedScale.contained * 0.8,
            maxScale: 4.0,
          )),
    );
  }
}
