//import 'package:cached_network_image/cached_network_image.dart';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../utils/constants.dart';

import 'photo_view_zoom.dart';

class LoadImage extends StatelessWidget {
  final String url;
  final String folder;
  final String filename;
  final double height;
  final double width;
  final BoxShape shape;
  final bool zoom;
  final Function onTap;
  const LoadImage({
    this.url, 
    this.height=80, 
    this.width=80, 
    this.zoom=false, 
    this.shape=BoxShape.rectangle, 
    this.onTap, 
    this.folder, 
    this.filename,
  });


  @override
  Widget build(BuildContext context) {
    Map<String, String> paramHeaders = {};
    
    // if( folder != null){
    //   String token = await WebAuth.getToken();
    //   paramHeaders = {
    //     'Authorization':"Bearer $token"
    //   };
    // }

    String urlFile = URL+'image/$folder/$filename';

    if(zoom)
      return InkWell(
        child: containerImage(urlFile,paramHeaders),
        onTap: () => Navigator.push(context,MaterialPageRoute(
              builder: (_) => PhotoViewZoom(
                url: urlFile,
              )))
      );
    else 
      return containerImage(urlFile,paramHeaders);
  }

  Widget containerImage(String urlFile,Map paramHeaders ) {
    return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            shape: shape,
            image: DecorationImage(
                fit: BoxFit.cover,
                image: CachedNetworkImageProvider(
                  urlFile,
                  headers: paramHeaders
                ),
              ),
        ),
      );
  }
}

class LoadImageLocal extends StatelessWidget {
  final File file;
  final double height;
  final double width;
  final BoxShape shape;
  const LoadImageLocal({Key key, this.file, this.height=80, this.width=80, this.shape=BoxShape.rectangle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          shape: shape,
          image: DecorationImage(
              fit: BoxFit.fill,
              image: Image.file(file).image,
            ),
      ),
    );
  }
}