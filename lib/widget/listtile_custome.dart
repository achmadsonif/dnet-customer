import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListTileDetailColumn extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget subtitleAlt;

  ListTileDetailColumn({this.title, this.subtitle, this.subtitleAlt});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: TextStyle(color: Colors.orange)),
          const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
          (subtitleAlt == null)
              ? Text(subtitle, style: TextStyle(fontSize: 14))
              : subtitleAlt,
        ],
      ),
    );
  }
}

class ListTileDetailRow extends StatelessWidget {
  final IconData icon;
  final Color color;
  final Widget icon1;
  final String title;
  final String subtitle;
  final bool bold;

  ListTileDetailRow(
      {this.title,
      this.color,
      this.subtitle,
      this.icon,
      this.icon1,
      this.bold});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Row(
            children: [
              if (icon != null) ...[
                Icon(
                  icon,
                  color: color == null ? Colors.black : color,
                  size: 20,
                ),
                SizedBox(
                  width: 5,
                ),
              ],
              Text(
                title,
                style: TextStyle(color: color == null ? Colors.black : color),
              ),
            ],
          ),
        ),
        Flexible(
            flex: 2,
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              if (icon1 != null) ...[
                icon1,
                SizedBox(
                  width: 5,
                ),
              ],
              Flexible(
                child: Text(
                  subtitle,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      color: color == null ? Colors.black : color,
                      fontWeight:
                          bold == true ? FontWeight.bold : FontWeight.normal),
                ),
              )
            ])),
      ],
    );
  }
}

class SubHeaderTile extends StatelessWidget {
  final String title;
  final Widget icon;

  SubHeaderTile({this.title, this.icon});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(20),
        child: Row(children: <Widget>[
          icon,
          SizedBox(
            width: 10,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
          )
        ]));
  }
}
