import 'package:flutter/material.dart';
import 'package:mobile_customer/utils/constants.dart';

class BaseOutlineCard extends StatelessWidget {
  final Widget child;
  const BaseOutlineCard({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: BorderSide(
            color: primaryColor,
            width: 2,
          ),
        ),
        child:Padding(
          padding: const EdgeInsets.all(13.0),
          child: child,
        ),
        );
  }
}
