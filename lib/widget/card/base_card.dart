import 'package:flutter/material.dart';

class BaseCard extends StatelessWidget {
  final Widget child;
  final Color color;
  final double radius;
  const BaseCard({Key key, this.child, this.color, this.radius = 15})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      elevation: 3.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius),
      ),
      child: child,
    );
  }
}
