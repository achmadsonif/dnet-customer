import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_customer/pages/auth/login_page.dart';
import 'package:mobile_customer/pages/chat/chat_page.dart';
// import 'package:mobile_customer/pages/home/home.dart';
import 'package:mobile_customer/pages/home/home_bottom_nav.dart';
import 'package:mobile_customer/widget/pages/loading_page.dart';

import 'pages/auth/global_controller.dart';
import 'pages/auth/prelogin_page.dart';
import 'pages/auth/user_model.dart';
import 'pages/splash.dart';
import 'utils/constants.dart';
import 'package:mobile_customer/utils/theme.dart';
import 'pages/auth/web_auth.dart';
import 'utils/notif_utils.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await NotifUtils().initMsg();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // User user;
  // // This widget is the root of your application.

  // Future<String> get isLoggedIn async {
  //   user = await WebAuth().getUser();
  //   if (user != null) {
  //     final gstate = Get.put(GlobalController());
  //     gstate.setUser(user);
  //     NotifUtils().getToken();

  //     return user.name;
  //   }
  //   return "";
  // }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        // debugShowCheckedModeBanner: false,
        title: 'Dnet',
        theme: theme(),
        routes: <String, WidgetBuilder>{
          '/home': (BuildContext context) => HomePage(),
        },
        home: SplashPage()

        // theme: ThemeData(
        //     // iconTheme: IconThemeData(color: Colors.white),
        //     scaffoldBackgroundColor: Colors.white,
        //     primaryIconTheme: IconThemeData(color: Colors.white)),
        // home: FutureBuilder(
        //     future: isLoggedIn,
        //     builder: (context, snapshot) {
        //       if (!snapshot.hasData) return LoadingPage();
        //       if (snapshot.data != "") {
        //         return HomePage();
        //       } else {
        //         return PreLoginPage();
        //       }
        //     }),
        );
  }
}
